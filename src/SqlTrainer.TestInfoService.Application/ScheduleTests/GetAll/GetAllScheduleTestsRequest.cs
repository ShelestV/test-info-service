﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll;

public sealed record GetAllScheduleTestsRequest(string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize)
{
}