﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll;

public interface IGetAllScheduleTestsRequestHandler : IRequestHandler<GetAllScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>
{
}