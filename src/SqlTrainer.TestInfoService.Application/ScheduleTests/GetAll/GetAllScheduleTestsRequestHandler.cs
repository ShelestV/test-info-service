﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll;

public sealed class GetAllScheduleTestsRequestHandler : RequestHandler<GetAllScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>, IGetAllScheduleTestsRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<GetAllScheduleTestsRequestHandler> logger;

    public GetAllScheduleTestsRequestHandler(
        IScheduleTestRepository scheduleTestRepository, 
        ILogger<GetAllScheduleTestsRequestHandler> logger)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<ScheduleTest>?> HandleRequestAsync(GetAllScheduleTestsRequest request, CancellationToken cancellationToken = default) =>
        await scheduleTestRepository.GetAllAsync(request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(
        GetAllScheduleTestsRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule tests has not been retrieved");
        return Task.CompletedTask;
    }
}