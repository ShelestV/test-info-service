﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser;

public sealed record GetByUserScheduleTestsRequest(Guid UserId, string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);