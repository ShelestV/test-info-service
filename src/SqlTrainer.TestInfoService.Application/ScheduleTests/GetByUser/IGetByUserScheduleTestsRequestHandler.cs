﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser;

public interface IGetByUserScheduleTestsRequestHandler : IRequestHandler<GetByUserScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>
{
}