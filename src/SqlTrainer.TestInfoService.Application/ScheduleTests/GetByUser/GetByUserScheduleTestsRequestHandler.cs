﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser;

public sealed class GetByUserScheduleTestsRequestHandler : RequestHandler<GetByUserScheduleTestsRequest, IReadOnlyCollection<ScheduleTest>>, IGetByUserScheduleTestsRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<GetByUserScheduleTestsRequestHandler> logger;

    public GetByUserScheduleTestsRequestHandler(
        IScheduleTestRepository scheduleTestRepository, 
        ILogger<GetByUserScheduleTestsRequestHandler> logger)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<ScheduleTest>?> HandleRequestAsync(GetByUserScheduleTestsRequest request, CancellationToken cancellationToken = default) =>
        await scheduleTestRepository.GetByUserAsync(request.UserId, request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(
        GetByUserScheduleTestsRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule tests with user id {UserId} have been not retrieved", request.UserId);
        return Task.CompletedTask;
    }
}