﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetById;

public sealed record GetByIdScheduleTestRequest(Guid Id) : IRequest;