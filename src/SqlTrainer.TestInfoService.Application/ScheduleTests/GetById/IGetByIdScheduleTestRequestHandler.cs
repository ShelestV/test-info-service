﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetById;

public interface IGetByIdScheduleTestRequestHandler : IRequestHandler<GetByIdScheduleTestRequest, ScheduleTest>
{
}