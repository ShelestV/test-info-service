﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.GetById;

public sealed class GetByIdScheduleTestRequestHandler : RequestHandler<GetByIdScheduleTestRequest, ScheduleTest>, IGetByIdScheduleTestRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<GetByIdScheduleTestRequestHandler> logger;

    public GetByIdScheduleTestRequestHandler(
        IScheduleTestRepository scheduleTestRepository, 
        ILogger<GetByIdScheduleTestRequestHandler> logger)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task<ScheduleTest?> HandleRequestAsync(GetByIdScheduleTestRequest request, CancellationToken cancellationToken = default) =>
        await scheduleTestRepository.GetByIdAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(
        GetByIdScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule test with id {Id} has not been retrieved", request.Id);
        return Task.CompletedTask;
    }
}