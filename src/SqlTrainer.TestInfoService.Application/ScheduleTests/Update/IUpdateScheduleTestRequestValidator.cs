﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Update;

public interface IUpdateScheduleTestRequestValidator : IRequestValidator<UpdateScheduleTestRequest>
{
}