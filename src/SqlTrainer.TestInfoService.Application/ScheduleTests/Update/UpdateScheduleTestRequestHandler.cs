﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Update;

public sealed class UpdateScheduleTestRequestHandler : RequestHandler<UpdateScheduleTestRequest>, IUpdateScheduleTestRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<UpdateScheduleTestRequestHandler> logger;

    public UpdateScheduleTestRequestHandler(
        IScheduleTestRepository scheduleTestRepository, 
        ILogger<UpdateScheduleTestRequestHandler> logger, 
        IUpdateScheduleTestRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(UpdateScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var scheduleTest = new ScheduleTest(request.Id)
        {
            TestId = request.TestId,
            StartAt = request.StartAt,
            FinishAt = request.FinishAt
        };
        scheduleTest.WithUserScheduleTests(request.UserIds
            .Select(userId => new UserScheduleTest(scheduleTest.Id, userId)).ToArray());
        
        await scheduleTestRepository.UpdateAsync(scheduleTest, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        UpdateScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule test with id {Id} has not been updated", request.Id);
        return Task.CompletedTask;
    }
}