﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Update;

public sealed record UpdateScheduleTestRequest(
    Guid Id,
    Guid TestId,
    DateTimeOffset StartAt,
    DateTimeOffset FinishAt,
    IEnumerable<Guid> UserIds) 
    : IRequest;