﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Update;

public interface IUpdateScheduleTestRequestHandler : IRequestHandler<UpdateScheduleTestRequest>
{
}