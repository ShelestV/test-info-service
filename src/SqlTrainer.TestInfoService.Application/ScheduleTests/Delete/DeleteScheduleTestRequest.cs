﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Delete;

public sealed record DeleteScheduleTestRequest(Guid Id) : IRequest;