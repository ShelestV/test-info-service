﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Delete;

public interface IDeleteScheduleTestRequestHandler : IRequestHandler<DeleteScheduleTestRequest>
{
}