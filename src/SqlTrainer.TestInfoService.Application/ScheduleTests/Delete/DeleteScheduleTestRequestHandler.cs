﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Delete;

public sealed class DeleteScheduleTestRequestHandler : RequestHandler<DeleteScheduleTestRequest>, IDeleteScheduleTestRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<DeleteScheduleTestRequestHandler> logger;

    public DeleteScheduleTestRequestHandler(
        IScheduleTestRepository scheduleTestRepository, 
        ILogger<DeleteScheduleTestRequestHandler> logger, 
        IUnitOfWork unitOfWork) 
        : base(unitOfWork: unitOfWork)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(DeleteScheduleTestRequest request, CancellationToken cancellationToken = default) => 
        await scheduleTestRepository.DeleteAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(
        DeleteScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule test with id {Id} has not been deleted", request.Id);
        return Task.CompletedTask;
    }
}