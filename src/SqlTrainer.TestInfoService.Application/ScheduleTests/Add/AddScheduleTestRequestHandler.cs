﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Add;

public sealed class AddScheduleTestRequestHandler : RequestHandler<AddScheduleTestRequest>, IAddScheduleTestRequestHandler
{
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly ILogger<AddScheduleTestRequestHandler> logger;

    public AddScheduleTestRequestHandler(
        IScheduleTestRepository scheduleTestRepository,
        ILogger<AddScheduleTestRequestHandler> logger,
        IAddScheduleTestRequestValidator validator,
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.scheduleTestRepository = scheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(AddScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        var scheduleTest = new ScheduleTest
        {
            TestId = request.TestId,
            StartAt = request.StartAt,
            FinishAt = request.FinishAt
        };
        scheduleTest.WithUserScheduleTests(request.UserIds
            .Select(userId => new UserScheduleTest(scheduleTest.Id, userId)).ToArray());
        
        await scheduleTestRepository.AddAsync(scheduleTest, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        AddScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Schedule test with test id {TestId} has not been added", request.TestId);
        return Task.CompletedTask;
    }
}