﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Add;

public sealed class AddScheduleTestRequestValidator : RequestValidator<AddScheduleTestRequest>, IAddScheduleTestRequestValidator
{
    private readonly ITestRepository testRepository;

    public AddScheduleTestRequestValidator(ITestRepository testRepository)
    {
        this.testRepository = testRepository;
    }

    protected override async Task<ValidationResult<AddScheduleTestRequest>> ValidateAsync(
        ValidationResult<AddScheduleTestRequest> validationResult, 
        AddScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.TestId == default)
            validationResult.FailValidation("TestId is required");
        
        var test = await testRepository.GetByIdAsync(request.TestId, cancellationToken);
        if (test is null)
            validationResult.FailValidation($"Test with id {request.TestId} does not exist");
        
        if (!request.UserIds.Any())
            validationResult.FailValidation("At least one user id is required");
        
        if (request.FinishAt <= DateTime.UtcNow)
            validationResult.FailValidation("FinishAt must be greater than current date");
        
        if (request.FinishAt <= request.StartAt)
            validationResult.FailValidation("FinishAt must be greater than StartAt");
        
        return validationResult;
    }
}