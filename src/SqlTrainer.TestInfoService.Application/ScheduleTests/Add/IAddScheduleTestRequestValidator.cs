﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Add;

public interface IAddScheduleTestRequestValidator : IRequestValidator<AddScheduleTestRequest>
{
}