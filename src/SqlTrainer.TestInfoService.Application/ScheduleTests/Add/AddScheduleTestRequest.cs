﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Add;

public sealed record AddScheduleTestRequest(
    Guid TestId,
    DateTimeOffset StartAt,
    DateTimeOffset FinishAt,
    IEnumerable<Guid> UserIds) : IRequest;