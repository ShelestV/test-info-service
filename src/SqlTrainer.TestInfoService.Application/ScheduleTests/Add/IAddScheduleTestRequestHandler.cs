﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.ScheduleTests.Add;

public interface IAddScheduleTestRequestHandler : IRequestHandler<AddScheduleTestRequest>
{
}