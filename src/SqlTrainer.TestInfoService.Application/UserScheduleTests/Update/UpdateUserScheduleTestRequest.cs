﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

public sealed record UpdateUserScheduleTestRequest(
    Guid ScheduleTestId,
    Guid UserId,
    DateTimeOffset? FinishedAt,
    bool? CheckedByTeacher) 
    : IRequest;