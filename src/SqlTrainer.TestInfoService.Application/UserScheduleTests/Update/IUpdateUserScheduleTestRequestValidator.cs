﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

public interface IUpdateUserScheduleTestRequestValidator : IRequestValidator<UpdateUserScheduleTestRequest>
{
}