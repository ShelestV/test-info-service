﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

public interface IUpdateUserScheduleTestRequestHandler : IRequestHandler<UpdateUserScheduleTestRequest>
{
}