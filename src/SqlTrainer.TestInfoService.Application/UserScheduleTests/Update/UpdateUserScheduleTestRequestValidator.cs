﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

public sealed class UpdateUserScheduleTestRequestValidator : RequestValidator<UpdateUserScheduleTestRequest>, IUpdateUserScheduleTestRequestValidator
{
    private readonly IScheduleTestRepository scheduleTestRepository;

    public UpdateUserScheduleTestRequestValidator(IScheduleTestRepository scheduleTestRepository)
    {
        this.scheduleTestRepository = scheduleTestRepository;
    }

    protected override async Task<ValidationResult<UpdateUserScheduleTestRequest>> ValidateAsync(
        ValidationResult<UpdateUserScheduleTestRequest> validationResult, 
        UpdateUserScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.UserId == default)
            validationResult.FailValidation("UserId is required");
        
        if (request.ScheduleTestId == default)
            validationResult.FailValidation("ScheduleTestId is required");

        var scheduleTest = await scheduleTestRepository.GetByIdAsync(request.ScheduleTestId, cancellationToken);
        if (scheduleTest is null)
            validationResult.FailValidation($"Schedule test with id {request.ScheduleTestId} does not exist");

        if (request.FinishedAt.HasValue)
        {
            if (request.FinishedAt.Value == default)
                validationResult.FailValidation("FinishedAt is required, when it is not null");
            
            if (DateTime.UtcNow < request.FinishedAt.Value)
                validationResult.FailValidation("FinishedAt must be less than UTC now");
        }

        return validationResult;
    }
}