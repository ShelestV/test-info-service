﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

public sealed class UpdateUserScheduleTestRequestHandler : RequestHandler<UpdateUserScheduleTestRequest>, IUpdateUserScheduleTestRequestHandler
{
    private readonly IUserScheduleTestRepository userScheduleTestRepository;
    private readonly ILogger<UpdateUserScheduleTestRequestHandler> logger;

    public UpdateUserScheduleTestRequestHandler(
        IUserScheduleTestRepository userScheduleTestRepository, 
        ILogger<UpdateUserScheduleTestRequestHandler> logger, 
        IUpdateUserScheduleTestRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.userScheduleTestRepository = userScheduleTestRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(UpdateUserScheduleTestRequest request, CancellationToken cancellationToken = default)
    {
        var userScheduleTest = new UserScheduleTest(request.ScheduleTestId, request.UserId)
        {
            FinishedAt = request.FinishedAt,
            CheckedByTeacher = request.CheckedByTeacher
        };
        await userScheduleTestRepository.UpdateAsync(userScheduleTest, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        UpdateUserScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "User schedule test with id {ScheduleTestId} for user {UserId} has not been updated", request.ScheduleTestId, request.UserId);
        return Task.CompletedTask;
    }
}