﻿using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface IUserAnswerRepository
{
    Task AddAsync(UserAnswer userAnswer, CancellationToken cancellationToken = default);
    Task UpdateAsync(UserAnswer userAnswer, CancellationToken cancellationToken = default);
    Task<IReadOnlyCollection<UserAnswer>> GetByScheduleTestAndUserAsync(Guid scheduleTestId, Guid userId, CancellationToken cancellationToken = default);
}