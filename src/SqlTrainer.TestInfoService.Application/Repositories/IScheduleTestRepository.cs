﻿using Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface IScheduleTestRepository : ICrudRepository<ScheduleTest>
{
    Task<IReadOnlyCollection<ScheduleTest>> GetByUserAsync(Guid userId, string searchTerm = "", string orderBy = "", SortDirection orderByDirection = SortDirection.Ascending, int page = 1, int pageSize = 1, CancellationToken cancellationToken = default);
}