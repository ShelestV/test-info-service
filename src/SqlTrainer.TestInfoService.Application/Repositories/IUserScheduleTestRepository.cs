﻿using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface IUserScheduleTestRepository
{
    Task UpdateAsync(UserScheduleTest userScheduleTest, CancellationToken cancellationToken = default);
}