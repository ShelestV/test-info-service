﻿using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface ITopicRepository : ICrudRepository<Domain.Models.Topic>
{
}