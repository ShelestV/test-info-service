﻿using Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface IQuestionRepository : ICrudRepository<Question>
{
    Task<IReadOnlyCollection<Question>> GetRangeAsync(IEnumerable<Guid> ids, CancellationToken cancellationToken = default);
    Task<IReadOnlyCollection<Question>> GetByTestAsync(Guid testId, string searchTerm = "", string orderBy = "", SortDirection orderByDirection = SortDirection.Ascending, int page = 1, int pageSize = 1, CancellationToken cancellationToken = default);
    Task<IReadOnlyCollection<Question>> GetByScheduleTestAsync(Guid scheduleTestId, CancellationToken cancellationToken = default);
}