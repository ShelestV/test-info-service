﻿using Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface ITestRepository : ICrudRepository<Test>
{
}