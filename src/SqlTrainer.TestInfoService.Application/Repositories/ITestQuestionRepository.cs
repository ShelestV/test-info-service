﻿using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface ITestQuestionRepository
{
    Task AddAsync(TestQuestion testQuestion, CancellationToken cancellationToken = default);
    Task AddRangeAsync(Guid testId, IEnumerable<TestQuestion> testQuestions, CancellationToken cancellationToken = default);
    Task DeleteAsync(Guid testId, IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default);
    Task DeleteByTestAsync(Guid testId, CancellationToken cancellationToken = default);
    Task UpdateAsync(TestQuestion testQuestion, CancellationToken cancellationToken = default);
    Task<TestQuestion?> GetByScheduleTestAndQuestionAsync(Guid scheduleTestId, Guid questionId, CancellationToken cancellationToken = default);
    Task<IReadOnlyCollection<TestQuestion>> GetByScheduleTestAndQuestionIdsAsync(Guid scheduleTestId, IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default);
}