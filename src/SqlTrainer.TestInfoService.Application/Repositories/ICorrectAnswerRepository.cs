﻿using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Repositories;

public interface ICorrectAnswerRepository
{
    Task AddAsync(CorrectAnswer correctAnswer, CancellationToken cancellationToken = default);
    Task UpdateAsync(CorrectAnswer correctAnswer, CancellationToken cancellationToken = default);
    Task<CorrectAnswer?> GetByQuestionAsync(Guid questionId, CancellationToken cancellationToken = default);
    Task<IReadOnlyCollection<CorrectAnswer>> GetByQuestionsAsync(IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default);
}