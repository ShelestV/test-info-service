﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Tests.Delete;

public interface IDeleteTestRequestHandler : IRequestHandler<DeleteTestRequest>
{
}