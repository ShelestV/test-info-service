﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Tests.Delete;

public sealed class DeleteTestRequestHandler : RequestHandler<DeleteTestRequest>, IDeleteTestRequestHandler
{
    private readonly ITestRepository testRepository;
    private readonly ILogger<DeleteTestRequestHandler> logger;

    public DeleteTestRequestHandler(
        ITestRepository testRepository, 
        IUnitOfWork unitOfWork,
        ILogger<DeleteTestRequestHandler> logger) 
        : base(unitOfWork: unitOfWork)
    {
        this.testRepository = testRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(DeleteTestRequest request, CancellationToken cancellationToken = default) => 
        await testRepository.DeleteAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(
        DeleteTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Test with id {Id} has not be deleted", request.Id);
        return Task.CompletedTask;
    }
}