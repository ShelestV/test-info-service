﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Tests.Delete;

public sealed record DeleteTestRequest(Guid Id) : IRequest;