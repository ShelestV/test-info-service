﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.GetById;

public sealed class GetByIdTestRequestHandler : RequestHandler<GetByIdTestRequest, Test>, IGetByIdTestRequestHandler
{
    private readonly ITestRepository testRepository;
    private readonly ILogger<GetByIdTestRequestHandler> logger;

    public GetByIdTestRequestHandler(
        ITestRepository testRepository, 
        ILogger<GetByIdTestRequestHandler> logger)
    {
        this.testRepository = testRepository;
        this.logger = logger;
    }

    protected override async Task<Test?> HandleRequestAsync(GetByIdTestRequest request, CancellationToken cancellationToken = default) =>
        await testRepository.GetByIdAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(
        GetByIdTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Test with id {Id} has not been retrieved", request.Id);
        return Task.CompletedTask;
    }
}