﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Tests.GetById;

public sealed record GetByIdTestRequest(Guid Id) : IRequest;