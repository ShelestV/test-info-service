﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.GetById;

public interface IGetByIdTestRequestHandler : IRequestHandler<GetByIdTestRequest, Test>
{
}