﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Tests.GetAll;

public sealed record GetAllTestsRequest(string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);