﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.GetAll;

public interface IGetAllTestsRequestHandler : IRequestHandler<GetAllTestsRequest, IReadOnlyCollection<Test>>
{
}