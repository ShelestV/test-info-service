﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.GetAll;

public sealed class GetAllTestsRequestHandler : RequestHandler<GetAllTestsRequest, IReadOnlyCollection<Test>>, IGetAllTestsRequestHandler
{
    private readonly ITestRepository testRepository;
    private readonly ILogger<GetAllTestsRequestHandler> logger;

    public GetAllTestsRequestHandler(
        ITestRepository testRepository, 
        ILogger<GetAllTestsRequestHandler> logger)
    {
        this.testRepository = testRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<Test>?> HandleRequestAsync(GetAllTestsRequest request, CancellationToken cancellationToken = default) =>
        await testRepository.GetAllAsync(request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(
        GetAllTestsRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Tests has not been retrieved");
        return Task.CompletedTask;
    }
}