﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Tests.Add;

public sealed record AddTestRequest(
    string Name, 
    DateTime CreatedAt, 
    IReadOnlyCollection<(Guid QuestionId, double MaxMark)> TestQuestionsData) 
    : IRequest;