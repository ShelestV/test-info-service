﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Tests.Add;

public interface IAddTestRequestValidator : IRequestValidator<AddTestRequest>
{
}