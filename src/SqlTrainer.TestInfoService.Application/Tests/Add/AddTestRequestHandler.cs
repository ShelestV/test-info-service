﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.Add;

public sealed class AddTestRequestHandler : RequestHandler<AddTestRequest>, IAddTestRequestHandler
{
    private readonly ITestRepository testRepository;
    private readonly ITestQuestionRepository testQuestionRepository;
    private readonly ILogger<AddTestRequestHandler> logger;

    public AddTestRequestHandler(
        ITestRepository testRepository,
        ITestQuestionRepository testQuestionRepository,
        IAddTestRequestValidator testRequestValidator,
        IUnitOfWork unitOfWork,
        ILogger<AddTestRequestHandler> logger) : base(testRequestValidator, unitOfWork)
    {
        this.testRepository = testRepository;
        this.testQuestionRepository = testQuestionRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(
        AddTestRequest request,
        CancellationToken cancellationToken = default)
    {
        var test = new Test { Name = request.Name, CreatedAt = request.CreatedAt };
        await testRepository.AddAsync(test, cancellationToken);
        foreach (var (questionId, maxMark) in request.TestQuestionsData)
        {
            var testQuestion = new TestQuestion(test.Id, questionId) { MaxMark = maxMark };
            await testQuestionRepository.AddAsync(testQuestion, cancellationToken);
        }
    }

    protected override Task HandleErrorAsync(
        AddTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Test has not been added. Test name: {Name}", request.Name);
        return Task.CompletedTask;
    }
}