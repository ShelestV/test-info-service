﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Tests.Add;

public interface IAddTestRequestHandler : IRequestHandler<AddTestRequest>
{
}