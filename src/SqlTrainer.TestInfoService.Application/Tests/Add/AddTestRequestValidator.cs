﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Tests.Add;

public sealed class AddTestRequestValidator : RequestValidator<AddTestRequest>, IAddTestRequestValidator
{
    private readonly IQuestionRepository questionRepository;

    public AddTestRequestValidator(IQuestionRepository questionRepository)
    {
        this.questionRepository = questionRepository;
    }

    protected override async Task<ValidationResult<AddTestRequest>> ValidateAsync(
        ValidationResult<AddTestRequest> validationResult, 
        AddTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required");

        if (DateTimeOffset.UtcNow < request.CreatedAt)
            validationResult.FailValidation("CreatedAt is required");

        if (!request.TestQuestionsData.Any())
            validationResult.FailValidation("TestQuestionsData is required");
        
        var questions = await questionRepository.GetRangeAsync(
            request.TestQuestionsData.Select(x => x.QuestionId), cancellationToken);
        if (questions.Count != request.TestQuestionsData.Count)
            validationResult.FailValidation("Some questions are not found");

        foreach (var (_, maxMark) in request.TestQuestionsData)
            if (maxMark <= 0.0)
                validationResult.FailValidation("MaxMark must be greater than 0");
        
        return validationResult;
    }
}