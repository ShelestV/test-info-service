﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Tests.Update;

public interface IUpdateTestRequestHandler : IRequestHandler<UpdateTestRequest>
{
}