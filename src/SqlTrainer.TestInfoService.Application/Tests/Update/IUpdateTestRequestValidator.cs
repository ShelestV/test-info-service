﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Tests.Update;

public interface IUpdateTestRequestValidator : IRequestValidator<UpdateTestRequest>
{
}