﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Tests.Update;

public sealed record UpdateTestRequest(
    Guid Id, 
    string Name, 
    IReadOnlyCollection<(Guid QuestionId, double MaxMark)> TestQuestionsData) 
    : IRequest;