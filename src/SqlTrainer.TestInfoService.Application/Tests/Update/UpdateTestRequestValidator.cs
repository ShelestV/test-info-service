﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Tests.Update;

public sealed class UpdateTestRequestValidator : RequestValidator<UpdateTestRequest>, IUpdateTestRequestValidator
{
    private readonly ITestRepository testRepository;
    private readonly IQuestionRepository questionRepository;

    public UpdateTestRequestValidator(ITestRepository testRepository, IQuestionRepository questionRepository)
    {
        this.testRepository = testRepository;
        this.questionRepository = questionRepository;
    }

    protected override async Task<ValidationResult<UpdateTestRequest>> ValidateAsync(
        ValidationResult<UpdateTestRequest> validationResult, 
        UpdateTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.Id == default)
            validationResult.FailValidation("Id is required");
        
        var test = await testRepository.GetByIdAsync(request.Id, cancellationToken);
        if (test is null)
            validationResult.FailValidation("Test with such id does not exist");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required");

        if (!request.TestQuestionsData.Any())
            validationResult.FailValidation("TestQuestionsData is required");
        
        var questions = await questionRepository.GetRangeAsync(
            request.TestQuestionsData.Select(x => x.QuestionId), cancellationToken);
        if (questions.Count != request.TestQuestionsData.Count)
            validationResult.FailValidation("Some questions are not found");

        foreach (var (_, maxMark) in request.TestQuestionsData)
            if (maxMark <= 0.0)
                validationResult.FailValidation("MaxMark must be greater than 0");
        
        return validationResult;
    }
}