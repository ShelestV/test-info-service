﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Tests.Update;

public sealed class UpdateTestRequestHandler : RequestHandler<UpdateTestRequest>, IUpdateTestRequestHandler
{
    private readonly ITestRepository testRepository;
    private readonly ITestQuestionRepository testQuestionRepository;
    private readonly ILogger<UpdateTestRequestHandler> logger;

    public UpdateTestRequestHandler(
        ITestRepository testRepository, 
        ITestQuestionRepository testQuestionRepository, 
        IUpdateTestRequestValidator validator, 
        IUnitOfWork unitOfWork,
        ILogger<UpdateTestRequestHandler> logger) 
        : base(validator, unitOfWork)
    {
        this.testRepository = testRepository;
        this.testQuestionRepository = testQuestionRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(
        UpdateTestRequest request, 
        CancellationToken cancellationToken = default)
    {
        var test = new Test(request.Id)
        {
            Name = request.Name, 
            CreatedAt = default,
        };
        await testRepository.UpdateAsync(test, cancellationToken);
        await testQuestionRepository.DeleteByTestAsync(request.Id, cancellationToken);
        foreach (var (questionId, maxMark) in request.TestQuestionsData)
        {
            var testQuestion = new TestQuestion(test.Id, questionId) { MaxMark = maxMark };
            await testQuestionRepository.AddAsync(testQuestion, cancellationToken);
        }
    }

    protected override Task HandleErrorAsync(
        UpdateTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Test with id {Id} has not been updated", request.Id);
        return Task.CompletedTask;
    }
}