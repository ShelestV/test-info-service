﻿namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public record QuestionBody(Guid QuestionId, string? Body);