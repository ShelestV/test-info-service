﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public interface IAddUserAnswersRequestHandler : IRequestHandler<AddUserAnswersRequest>
{
}