﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public sealed record AddUserAnswersRequest(
    Guid ScheduleTestId,
    Guid UserId,
    DateTimeOffset AnsweredAt, 
    IReadOnlyCollection<QuestionBody> QuestionBodies)
    : IRequest;