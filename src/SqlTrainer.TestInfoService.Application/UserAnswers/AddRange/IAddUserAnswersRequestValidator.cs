﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public interface IAddUserAnswersRequestValidator : IRequestValidator<AddUserAnswersRequest>
{
}