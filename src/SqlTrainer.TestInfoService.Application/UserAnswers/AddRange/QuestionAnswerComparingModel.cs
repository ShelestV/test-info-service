﻿using DatabaseHelper.ScriptResults;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public sealed record QuestionAnswerComparingModel(Guid QuestionId, ScriptResult Actual, ScriptResult Expected, double MaxMark);