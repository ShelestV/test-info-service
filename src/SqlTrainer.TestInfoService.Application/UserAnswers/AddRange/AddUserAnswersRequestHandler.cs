﻿using Application.Mediator.Handlers;
using Application.Repositories;
using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public sealed class AddUserAnswersRequestHandler : RequestHandler<AddUserAnswersRequest>, IAddUserAnswersRequestHandler
{
    private readonly IUserAnswerRepository userAnswerRepository;
    private readonly IQuestionRepository questionRepository;
    private readonly ICorrectAnswerRepository correctAnswerRepository;
    private readonly ITestQuestionRepository testQuestionRepository;
    private readonly IScriptService scriptService;
    private readonly IScriptFormattingService scriptFormattingService;
    private readonly IScriptResultsCompareService scriptResultsCompareService;
    private readonly ILogger<AddUserAnswersRequestHandler> logger;

    public AddUserAnswersRequestHandler(
        IUserAnswerRepository userAnswerRepository, 
        IQuestionRepository questionRepository, 
        ICorrectAnswerRepository correctAnswerRepository,
        ITestQuestionRepository testQuestionRepository,
        IScriptService scriptService,
        IScriptFormattingService scriptFormattingService,
        IScriptResultsCompareService scriptResultsCompareService,
        ILogger<AddUserAnswersRequestHandler> logger, 
        IAddUserAnswersRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.userAnswerRepository = userAnswerRepository;
        this.questionRepository = questionRepository;
        this.correctAnswerRepository = correctAnswerRepository;
        this.testQuestionRepository = testQuestionRepository;
        this.scriptService = scriptService;
        this.scriptFormattingService = scriptFormattingService;
        this.scriptResultsCompareService = scriptResultsCompareService;
        this.logger = logger;
    }

    // ToDo: refactor adding of user answers
    protected override async Task HandleRequestAsync(AddUserAnswersRequest request, CancellationToken cancellationToken = default)
    {
        var questions = await questionRepository.GetRangeAsync(request.QuestionBodies.Select(qb => qb.QuestionId), cancellationToken);
        if (questions.Count != request.QuestionBodies.Count)
            throw new ArgumentException("Not all questions exist");

        var correctAnswers = await correctAnswerRepository.GetByQuestionsAsync(questions.Select(q => q.Id), cancellationToken);
        if (correctAnswers.Count != questions.Count)
            throw new ArgumentException("Not all questions have correct answers");
        
        var testQuestions = await testQuestionRepository.GetByScheduleTestAndQuestionIdsAsync(request.ScheduleTestId, questions.Select(q => q.Id), cancellationToken);
        if (testQuestions.Count != questions.Count)
            throw new ArgumentException("Not all questions are in the test");

        var userAnswersToSave = new List<UserAnswer>();

        // Convert QuestionBody to DatabaseQuestionBody
        var initialUserAnswerBodies = request.QuestionBodies
            .Select(qb => new DatabaseQuestionBody(questions.First(q => q.Id == qb.QuestionId).DatabaseId, qb.QuestionId, scriptFormattingService.Clear(qb.Body)))
            .ToArray();
        
        // Filter answers that could not be executed and format them
        var userAnswerBodiesToSave = initialUserAnswerBodies.Where(qb => string.IsNullOrWhiteSpace(qb.Body));
        userAnswersToSave.AddRange(userAnswerBodiesToSave.Select(uab =>
            new UserAnswer(request.ScheduleTestId, uab.QuestionId, request.UserId)
            {
                Body = uab.Body ?? string.Empty,
                ProgramMark = 0.0,
                AnsweredAt = request.AnsweredAt
            }));
        
        // Get empty answers to save immediately
        var userAnswerBodies = initialUserAnswerBodies
            .Where(qb => !string.IsNullOrWhiteSpace(qb.Body))
            .OrderBy(qb => qb.QuestionId)
            .ToList();
        
        // Filter correct answers according to user answers and format them
        var correctAnswerBodies = correctAnswers
            .Select(ca => new DatabaseQuestionBody(ca.Question!.DatabaseId, ca.QuestionId, scriptFormattingService.Clear(ca.Body)))
            .Where(ca => userAnswerBodies.Select(uab => uab.QuestionId).Contains(ca.QuestionId) && !string.IsNullOrWhiteSpace(ca.Body))
            .OrderBy(qb => qb.QuestionId)
            .ToList();
        
        if (correctAnswerBodies.Count != correctAnswers.Count)
        {
            logger.LogWarning("Not all correct answers could be executed");
            userAnswersToSave.AddRange(userAnswerBodies.Select(uab => 
                new UserAnswer(request.ScheduleTestId, uab.QuestionId, request.UserId)
                {
                    Body = uab.Body ?? string.Empty,
                    ProgramMark = 0.0,
                    AnsweredAt = request.AnsweredAt
                }));
            await SaveUserAnswersAsync(userAnswersToSave, cancellationToken);
            return;
        }
        
        for (var i = 0; i < userAnswerBodies.Count; i++)
        {
            var userAnswerBody = userAnswerBodies[i];
            var correctAnswerBody = correctAnswerBodies[i];
            
            if (!userAnswerBody.Body!.Equals(correctAnswerBody.Body))
                continue;
            
            // Remove all correct answers to not execute them
            var testQuestion = testQuestions.First(tq => tq.QuestionId == userAnswerBody.QuestionId);
            userAnswersToSave.Add(new UserAnswer(request.ScheduleTestId, userAnswerBody.QuestionId, request.UserId)
            {
                Body = userAnswerBody.Body,
                ProgramMark = testQuestion.MaxMark,
                AnsweredAt = request.AnsweredAt
            });
            
            userAnswerBodies.Remove(userAnswerBody);
            correctAnswerBodies.Remove(correctAnswerBody);
            i--;
        }
        
        var allAnswerBodies = userAnswerBodies.Concat(correctAnswerBodies).ToList();
        var comparingModels = new List<QuestionAnswerComparingModel>();
        foreach (var answersByDatabase in allAnswerBodies.GroupBy(answer => answer.DatabaseId))
        {
            var executeResult = await scriptService.ExecuteScriptsAsync(answersByDatabase
                .Select(answer => answer.Body!), answersByDatabase.Key, cancellationToken);

            var results = new List<ScriptResult>();
            if (!executeResult.IsSuccess)
            {
                // ToDo: think about bad scenario
                continue;
            }
            
            results.AddRange(executeResult.Value);

            var correctAnswersByDatabase = correctAnswerBodies.Where(dqb => dqb.DatabaseId == answersByDatabase.Key).Select(dqb => dqb.Body).ToList();
            var userAnswersByDatabase = userAnswerBodies.Where(dqb => dqb.DatabaseId == answersByDatabase.Key).Select(dqb => dqb.Body).ToList();
            foreach (var answersByQuestion in answersByDatabase.GroupBy(dqb => dqb.QuestionId))
            {
                var testQuestion = testQuestions.First(tq => tq.QuestionId == answersByQuestion.Key);
                
                var correctAnswerBody = answersByQuestion.First(dqb => correctAnswersByDatabase.Contains(dqb.Body)).Body;
                var userAnswerBody = answersByQuestion.First(dqb => userAnswersByDatabase.Contains(dqb.Body)).Body;
                
                var correctAnswer = results.First(res => res.Script.Equals(correctAnswerBody));
                var userAnswer = results.First(res => res.Script.Equals(userAnswerBody));
                
                comparingModels.Add(new QuestionAnswerComparingModel(answersByQuestion.Key, userAnswer, correctAnswer, testQuestion.MaxMark));
            }
        }

        userAnswersToSave.AddRange(comparingModels.Select(comparingModel => 
            new UserAnswer(request.ScheduleTestId, comparingModel.QuestionId, request.UserId)
            {
                Body = request.QuestionBodies.First(qb => qb.QuestionId == comparingModel.QuestionId).Body!, 
                ProgramMark = scriptResultsCompareService.Compare(comparingModel.Actual, comparingModel.Expected, comparingModel.MaxMark), 
                AnsweredAt = request.AnsweredAt
            }));
        
        await SaveUserAnswersAsync(userAnswersToSave, cancellationToken);
    }

    private async Task SaveUserAnswersAsync(IEnumerable<UserAnswer> userAnswers, CancellationToken cancellationToken = default)
    {
        foreach (var userAnswer in userAnswers)
            await userAnswerRepository.AddAsync(userAnswer, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        AddUserAnswersRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "User answers for user {UserId} for schedule test {ScheduleTestId} has not been added", request.UserId, request.ScheduleTestId);
        return Task.CompletedTask;
    }
}