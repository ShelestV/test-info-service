﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public sealed class AddUserAnswersRequestValidator : RequestValidator<AddUserAnswersRequest>, IAddUserAnswersRequestValidator
{
    private readonly IUserAnswerRepository userAnswerRepository;
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly IQuestionRepository questionRepository;
    
    public AddUserAnswersRequestValidator(
        IUserAnswerRepository userAnswerRepository, 
        IScheduleTestRepository scheduleTestRepository, 
        IQuestionRepository questionRepository)
    {
        this.userAnswerRepository = userAnswerRepository;
        this.scheduleTestRepository = scheduleTestRepository;
        this.questionRepository = questionRepository;
    }

    protected override async Task<ValidationResult<AddUserAnswersRequest>> ValidateAsync(
        ValidationResult<AddUserAnswersRequest> validationResult, 
        AddUserAnswersRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.ScheduleTestId == default)
            validationResult.FailValidation("ScheduleTestId is required");
        
        var scheduleTest = await scheduleTestRepository.GetByIdAsync(request.ScheduleTestId, cancellationToken);
        if (scheduleTest is null)
            validationResult.FailValidation("Schedule test does not exist");
        
        if (request.UserId == default)
            validationResult.FailValidation("UserId is required");
        
        if (request.AnsweredAt == default)
            validationResult.FailValidation("AnsweredAt is required");
        
        if (DateTime.UtcNow < request.AnsweredAt)
            validationResult.FailValidation("AnsweredAt cannot be in the future");

        if (scheduleTest is not null)
        {
            if (request.AnsweredAt < scheduleTest.StartAt)
                validationResult.FailValidation("AnsweredAt cannot be before test start");
            
            if (scheduleTest.FinishAt < request.AnsweredAt)
                validationResult.FailValidation("AnsweredAt cannot be after test finish");
        }

        var questions = await questionRepository.GetRangeAsync(
            request.QuestionBodies.Select(qb => qb.QuestionId), cancellationToken);
        
        if (questions.Count != request.QuestionBodies.Count)
            validationResult.FailValidation("Not all questions exist");

        return validationResult;
    }
}