﻿namespace SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;

public record DatabaseQuestionBody(Guid DatabaseId, Guid QuestionId, string? Body) : QuestionBody(QuestionId, Body);