﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.Update;

public interface IUpdateUserAnswerRequestHandler : IRequestHandler<UpdateUserAnswerRequest>
{
}