﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.Update;

public sealed class UpdateUserAnswerRequestValidator : RequestValidator<UpdateUserAnswerRequest>, IUpdateUserAnswerRequestValidator
{
    private readonly ITestQuestionRepository testQuestionRepository;
    private readonly IScheduleTestRepository scheduleTestRepository;
    private readonly IQuestionRepository questionRepository;

    public UpdateUserAnswerRequestValidator(
        ITestQuestionRepository testQuestionRepository, 
        IScheduleTestRepository scheduleTestRepository, 
        IQuestionRepository questionRepository)
    {
        this.testQuestionRepository = testQuestionRepository;
        this.scheduleTestRepository = scheduleTestRepository;
        this.questionRepository = questionRepository;
    }

    protected override async Task<ValidationResult<UpdateUserAnswerRequest>> ValidateAsync(
        ValidationResult<UpdateUserAnswerRequest> validationResult, 
        UpdateUserAnswerRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.ScheduleTestId == default)
        {
            validationResult.FailValidation("ScheduleTestId is required");
        }
        else
        {
            var scheduleTest = await scheduleTestRepository.GetByIdAsync(request.ScheduleTestId, cancellationToken);
            if (scheduleTest is null)
                validationResult.FailValidation("ScheduleTest does not exist");
        }

        if (request.UserId == default)
            validationResult.FailValidation("UserId is required");

        if (request.QuestionId == default)
        {
            validationResult.FailValidation("QuestionId is required");
        }
        else
        {
            var question = await questionRepository.GetByIdAsync(request.QuestionId, cancellationToken);
            if (question is null)
                validationResult.FailValidation("Question does not exist");
        }

        if (request.TeacherMark < 0)
            validationResult.FailValidation("Mark must be greater than or equal to 0");
        
        var testQuestion = await testQuestionRepository.GetByScheduleTestAndQuestionAsync(
            request.ScheduleTestId, request.QuestionId, cancellationToken);

        if (testQuestion is null)
            validationResult.FailValidation("TestQuestion does not exist");
        else if (testQuestion.MaxMark < request.TeacherMark)
            validationResult.FailValidation("Mark must be less than or equal to MaxMark");

        return validationResult;
    }
}