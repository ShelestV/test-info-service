﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.Update;

public sealed record UpdateUserAnswerRequest(Guid ScheduleTestId, Guid UserId, Guid QuestionId, double TeacherMark) : IRequest;