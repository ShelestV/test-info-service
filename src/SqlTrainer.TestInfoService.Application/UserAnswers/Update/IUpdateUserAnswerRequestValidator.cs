﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.Update;

public interface IUpdateUserAnswerRequestValidator : IRequestValidator<UpdateUserAnswerRequest>
{
}