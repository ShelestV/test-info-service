﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.Update;

public sealed class UpdateUserAnswerRequestHandler : RequestHandler<UpdateUserAnswerRequest>, IUpdateUserAnswerRequestHandler
{
    private readonly IUserAnswerRepository userAnswerRepository;
    private readonly ILogger<UpdateUserAnswerRequestHandler> logger;

    public UpdateUserAnswerRequestHandler(
        IUserAnswerRepository userAnswerRepository, 
        ILogger<UpdateUserAnswerRequestHandler> logger, 
        IUpdateUserAnswerRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.userAnswerRepository = userAnswerRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(UpdateUserAnswerRequest request, CancellationToken cancellationToken = default) =>
        await userAnswerRepository.UpdateAsync(new UserAnswer(request.ScheduleTestId, request.QuestionId, request.UserId)
        {
            Body = string.Empty,
            AnsweredAt = DateTime.UtcNow,
            ProgramMark = 0.0,
            TeacherMark = request.TeacherMark
        }, cancellationToken);

    protected override Task HandleErrorAsync(
        UpdateUserAnswerRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception,
            "User answer for user with id {UserId} for schedule test with id {ScheduleTestId} and question id {QuestionId} has not be updated", 
            request.UserId, request.ScheduleTestId, request.QuestionId);
        return Task.CompletedTask;
    }
}