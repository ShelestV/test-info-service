﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.GetByScheduleTestAndUser;

public sealed record GetByScheduleTestAndUserAnswersRequest(Guid ScheduleTestId, Guid UserId) : IRequest;