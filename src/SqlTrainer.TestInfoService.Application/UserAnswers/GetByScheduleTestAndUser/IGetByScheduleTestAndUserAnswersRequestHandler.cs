﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.GetByScheduleTestAndUser;

public interface IGetByScheduleTestAndUserAnswersRequestHandler : IRequestHandler<GetByScheduleTestAndUserAnswersRequest, IReadOnlyCollection<UserAnswer>>
{
}