﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.UserAnswers.GetByScheduleTestAndUser;

public sealed class GetByScheduleTestAndUserAnswersRequestHandler : RequestHandler<GetByScheduleTestAndUserAnswersRequest, IReadOnlyCollection<UserAnswer>>, IGetByScheduleTestAndUserAnswersRequestHandler
{
    private readonly IUserAnswerRepository userAnswerRepository;
    private readonly ILogger<GetByScheduleTestAndUserAnswersRequestHandler> logger;

    public GetByScheduleTestAndUserAnswersRequestHandler(
        IUserAnswerRepository userAnswerRepository, 
        ILogger<GetByScheduleTestAndUserAnswersRequestHandler> logger)
    {
        this.userAnswerRepository = userAnswerRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<UserAnswer>?> HandleRequestAsync(GetByScheduleTestAndUserAnswersRequest request, CancellationToken cancellationToken = default) =>
        await userAnswerRepository.GetByScheduleTestAndUserAsync(request.ScheduleTestId, request.UserId, cancellationToken);

    protected override Task HandleErrorAsync(
        GetByScheduleTestAndUserAnswersRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, 
            "User answers have not been retrieved by schedule test with id {ScheduleTestId} and user with id {UserId}",
            request.ScheduleTestId, request.UserId);
        return Task.CompletedTask;
    }
}