﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByTest;

public sealed class GetQuestionsByTestRequestHandler : RequestHandler<GetQuestionsByTestRequest, IReadOnlyCollection<Question>>, IGetQuestionsByTestRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ILogger<GetQuestionsByTestRequestHandler> logger;

    public GetQuestionsByTestRequestHandler(
        IQuestionRepository questionRepository,
        ILogger<GetQuestionsByTestRequestHandler> logger)
    {
        this.questionRepository = questionRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<Question>?> HandleRequestAsync(GetQuestionsByTestRequest request, CancellationToken cancellationToken = default) =>
        await questionRepository.GetByTestAsync(request.TestId, request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(
        GetQuestionsByTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Questions with test id {TestId} has not been retrieved", request.TestId);
        return Task.CompletedTask;
    }
}