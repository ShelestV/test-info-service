﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByTest;

public sealed record GetQuestionsByTestRequest(Guid TestId, string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);