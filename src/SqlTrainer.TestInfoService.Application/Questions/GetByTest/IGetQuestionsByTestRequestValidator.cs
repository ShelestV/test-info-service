﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByTest;

public interface IGetQuestionsByTestRequestValidator : IRequestValidator<GetQuestionsByTestRequest>
{
}