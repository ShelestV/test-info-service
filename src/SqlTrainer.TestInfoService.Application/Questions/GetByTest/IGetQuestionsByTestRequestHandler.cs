﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByTest;

public interface IGetQuestionsByTestRequestHandler : IRequestHandler<GetQuestionsByTestRequest, IReadOnlyCollection<Question>>
{
}