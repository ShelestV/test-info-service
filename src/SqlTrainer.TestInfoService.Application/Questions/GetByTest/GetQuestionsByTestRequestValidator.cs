﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByTest;

public sealed class GetQuestionsByTestRequestValidator : RequestValidator<GetQuestionsByTestRequest>, IGetQuestionsByTestRequestValidator
{
    private readonly ITestRepository testRepository;

    public GetQuestionsByTestRequestValidator(ITestRepository testRepository)
    {
        this.testRepository = testRepository;
    }

    protected override async Task<ValidationResult<GetQuestionsByTestRequest>> ValidateAsync(
        ValidationResult<GetQuestionsByTestRequest> validationResult, 
        GetQuestionsByTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.TestId == default)
            validationResult.FailValidation("TestId is required");
        
        var test = await testRepository.GetByIdAsync(request.TestId, cancellationToken);
        if (test == null)
            validationResult.FailValidation($"Test with id {request.TestId} does not exist");

        return validationResult;
    }
}