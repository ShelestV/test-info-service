﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;

public interface IGetQuestionsByScheduleTestRequestHandler : IRequestHandler<GetQuestionsByScheduleTestRequest, IReadOnlyCollection<Question>>
{
}