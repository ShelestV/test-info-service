﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;

public sealed record GetQuestionsByScheduleTestRequest(Guid ScheduleTestId) : IRequest;