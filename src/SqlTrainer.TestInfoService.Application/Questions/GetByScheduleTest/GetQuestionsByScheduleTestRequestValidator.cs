﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;

public sealed class GetQuestionsByScheduleTestRequestValidator : RequestValidator<GetQuestionsByScheduleTestRequest>, IGetQuestionsByScheduleTestRequestValidator
{
    private readonly IScheduleTestRepository scheduleTestRepository;

    public GetQuestionsByScheduleTestRequestValidator(IScheduleTestRepository scheduleTestRepository)
    {
        this.scheduleTestRepository = scheduleTestRepository;
    }

    protected override async Task<ValidationResult<GetQuestionsByScheduleTestRequest>> ValidateAsync(
        ValidationResult<GetQuestionsByScheduleTestRequest> validationResult, 
        GetQuestionsByScheduleTestRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.ScheduleTestId == default)
            validationResult.FailValidation("ScheduleTestId is required");
        
        var test = await scheduleTestRepository.GetByIdAsync(request.ScheduleTestId, cancellationToken);
        if (test == null)
            validationResult.FailValidation($"Schedule test with id {request.ScheduleTestId} does not exist");

        return validationResult;
    }
}