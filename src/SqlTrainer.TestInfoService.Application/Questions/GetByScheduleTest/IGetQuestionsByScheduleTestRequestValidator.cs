﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;

public interface IGetQuestionsByScheduleTestRequestValidator : IRequestValidator<GetQuestionsByScheduleTestRequest>
{
}