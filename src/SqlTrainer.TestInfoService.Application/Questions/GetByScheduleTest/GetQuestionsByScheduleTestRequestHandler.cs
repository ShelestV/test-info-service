﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;

public sealed class GetQuestionsByScheduleTestRequestHandler : RequestHandler<GetQuestionsByScheduleTestRequest, IReadOnlyCollection<Question>>, IGetQuestionsByScheduleTestRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ILogger<GetQuestionsByScheduleTestRequestHandler> logger;

    public GetQuestionsByScheduleTestRequestHandler(
        IQuestionRepository questionRepository,
        ILogger<GetQuestionsByScheduleTestRequestHandler> logger)
    {
        this.questionRepository = questionRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<Question>?> HandleRequestAsync(GetQuestionsByScheduleTestRequest request, CancellationToken cancellationToken = default) =>
        await questionRepository.GetByScheduleTestAsync(request.ScheduleTestId, cancellationToken);

    protected override Task HandleErrorAsync(
        GetQuestionsByScheduleTestRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Questions with schedule test id {ScheduleTestId} has not been retrieved", request.ScheduleTestId);
        return Task.CompletedTask;
    }
}