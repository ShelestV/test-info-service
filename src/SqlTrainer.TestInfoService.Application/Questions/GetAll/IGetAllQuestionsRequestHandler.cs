﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetAll;

public interface IGetAllQuestionsRequestHandler : IRequestHandler<GetAllQuestionsRequest, IReadOnlyCollection<Question>>
{
}