﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.GetAll;

public sealed class GetAllQuestionsRequestHandler : RequestHandler<GetAllQuestionsRequest, IReadOnlyCollection<Question>>, IGetAllQuestionsRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ILogger<GetAllQuestionsRequestHandler> logger;

    public GetAllQuestionsRequestHandler(
        IQuestionRepository questionRepository, 
        ILogger<GetAllQuestionsRequestHandler> logger)
    {
        this.questionRepository = questionRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<Question>?> HandleRequestAsync(GetAllQuestionsRequest request, CancellationToken cancellationToken = default) =>
        await questionRepository.GetAllAsync(request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(
        GetAllQuestionsRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Questions has not been retrieved");
        return Task.CompletedTask;
    }
}