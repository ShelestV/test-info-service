﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.GetAll;

public sealed record GetAllQuestionsRequest(string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize) 
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);