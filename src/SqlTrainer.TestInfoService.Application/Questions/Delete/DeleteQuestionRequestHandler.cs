﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.Delete;

public sealed class DeleteQuestionRequestHandler : RequestHandler<DeleteQuestionRequest>, IDeleteQuestionRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ILogger<DeleteQuestionRequestHandler> logger;

    public DeleteQuestionRequestHandler(
        IQuestionRepository questionRepository, 
        ILogger<DeleteQuestionRequestHandler> logger, 
        IUnitOfWork unitOfWork) 
        : base(unitOfWork: unitOfWork)
    {
        this.questionRepository = questionRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(DeleteQuestionRequest request, CancellationToken cancellationToken = default) =>
        await questionRepository.DeleteAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(
        DeleteQuestionRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Question with id {Id} has not been deleted", request.Id);
        return Task.CompletedTask;
    }
}