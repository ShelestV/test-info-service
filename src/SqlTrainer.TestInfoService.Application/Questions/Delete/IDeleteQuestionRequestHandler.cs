﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Questions.Delete;

public interface IDeleteQuestionRequestHandler : IRequestHandler<DeleteQuestionRequest>
{
}