﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Questions.Delete;

public sealed record DeleteQuestionRequest(Guid Id) : IRequest;