﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.Add;

public sealed class AddQuestionRequestHandler : RequestHandler<AddQuestionRequest>, IAddQuestionRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ICorrectAnswerRepository correctAnswerRepository;
    private readonly ILogger<AddQuestionRequestHandler> logger;

    public AddQuestionRequestHandler(
        IQuestionRepository questionRepository, 
        ICorrectAnswerRepository correctAnswerRepository, 
        ILogger<AddQuestionRequestHandler> logger, 
        IAddQuestionRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.questionRepository = questionRepository;
        this.correctAnswerRepository = correctAnswerRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(AddQuestionRequest request, CancellationToken cancellationToken = default)
    {
        var question = new Question
        {
            Body = request.Body,
            Complexity = request.Complexity,
            TopicId = request.TopicId,
            DatabaseId = request.DatabaseId
        };
        await questionRepository.AddAsync(question, cancellationToken);

        var correctAnswer = new CorrectAnswer
        {
            Body = request.CorrectAnswerBody,
            QuestionId = question.Id
        };
        await correctAnswerRepository.AddAsync(correctAnswer, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        AddQuestionRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Question with body {Body} has not been added", request.Body);
        return Task.CompletedTask;
    }
}