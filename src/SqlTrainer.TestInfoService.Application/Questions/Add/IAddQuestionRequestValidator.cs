﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Questions.Add;

public interface IAddQuestionRequestValidator : IRequestValidator<AddQuestionRequest>
{
}