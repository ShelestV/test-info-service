﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.Add;

public sealed class AddQuestionRequestValidator : RequestValidator<AddQuestionRequest>, IAddQuestionRequestValidator
{
    private readonly ITopicRepository topicRepository;

    public AddQuestionRequestValidator(ITopicRepository topicRepository)
    {
        this.topicRepository = topicRepository;
    }

    protected override async Task<ValidationResult<AddQuestionRequest>> ValidateAsync(
        ValidationResult<AddQuestionRequest> validationResult, 
        AddQuestionRequest request,
        CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Body))
            validationResult.FailValidation("Body is required");
        
        if (string.IsNullOrWhiteSpace(request.CorrectAnswerBody))
            validationResult.FailValidation("Correct answer body is required");
        
        if (request.Complexity < 1)
            validationResult.FailValidation("Complexity must be greater than 0");
        
        if (request.TopicId == default)
            validationResult.FailValidation("Topic id is required");
        
        if (request.DatabaseId == default)
            validationResult.FailValidation("Database id is required");
        
        var topic = await topicRepository.GetByIdAsync(request.TopicId, cancellationToken);
        if (topic == null)
            validationResult.FailValidation($"Topic with id {request.TopicId} does not exist");

        return validationResult;
    }
}