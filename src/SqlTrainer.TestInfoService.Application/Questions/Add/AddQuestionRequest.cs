﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Questions.Add;

public sealed record AddQuestionRequest(
    string Body,
    int Complexity,
    Guid TopicId,
    Guid DatabaseId,
    string CorrectAnswerBody) : IRequest;