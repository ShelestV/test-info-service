﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Questions.Add;

public interface IAddQuestionRequestHandler : IRequestHandler<AddQuestionRequest>
{
}