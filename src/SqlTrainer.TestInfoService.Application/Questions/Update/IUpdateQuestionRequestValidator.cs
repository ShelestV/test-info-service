﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Questions.Update;

public interface IUpdateQuestionRequestValidator : IRequestValidator<UpdateQuestionRequest>
{
}