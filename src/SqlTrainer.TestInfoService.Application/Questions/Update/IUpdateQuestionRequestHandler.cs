﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Questions.Update;

public interface IUpdateQuestionRequestHandler : IRequestHandler<UpdateQuestionRequest>
{
}