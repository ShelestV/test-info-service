﻿using Application.Mediator.Validators;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Questions.Update;

public sealed class UpdateQuestionRequestValidator : RequestValidator<UpdateQuestionRequest>, IUpdateQuestionRequestValidator
{
    private readonly ITopicRepository topicRepository;

    public UpdateQuestionRequestValidator(ITopicRepository topicRepository)
    {
        this.topicRepository = topicRepository;
    }

    protected override async Task<ValidationResult<UpdateQuestionRequest>> ValidateAsync(
        ValidationResult<UpdateQuestionRequest> validationResult, 
        UpdateQuestionRequest request,
        CancellationToken cancellationToken = default)
    {
        if (request.Id == default)
            validationResult.FailValidation("Id is required");
        
        if (string.IsNullOrWhiteSpace(request.Body))
            validationResult.FailValidation("Body is required");
        
        if (request.Complexity < 1)
            validationResult.FailValidation("Complexity must be greater than 0");
        
        if (string.IsNullOrWhiteSpace(request.CorrectAnswerBody))
            validationResult.FailValidation("CorrectAnswerBody is required");
        
        if (request.TopicId == default)
            validationResult.FailValidation("TopicId is required");
        
        if (request.DatabaseId == default)
            validationResult.FailValidation("DatabaseId is required");
        
        var topic = await topicRepository.GetByIdAsync(request.TopicId, cancellationToken);
        if (topic == null)
            validationResult.FailValidation($"Topic with id {request.TopicId} does not exist");
        
        return validationResult;
    }
}