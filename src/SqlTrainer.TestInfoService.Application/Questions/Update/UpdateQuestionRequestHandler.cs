﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Questions.Update;

public sealed class UpdateQuestionRequestHandler : RequestHandler<UpdateQuestionRequest>, IUpdateQuestionRequestHandler
{
    private readonly IQuestionRepository questionRepository;
    private readonly ICorrectAnswerRepository correctAnswerRepository;
    private readonly ILogger<UpdateQuestionRequestHandler> logger;

    public UpdateQuestionRequestHandler(
        IQuestionRepository questionRepository, 
        ICorrectAnswerRepository correctAnswerRepository, 
        ILogger<UpdateQuestionRequestHandler> logger, 
        IUpdateQuestionRequestValidator validator, 
        IUnitOfWork unitOfWork) 
        : base(validator, unitOfWork)
    {
        this.questionRepository = questionRepository;
        this.correctAnswerRepository = correctAnswerRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(
        UpdateQuestionRequest request, 
        CancellationToken cancellationToken = default)
    {
        var question = new Question(request.Id)
        {
            Body = request.Body,
            Complexity = request.Complexity,
            TopicId = request.TopicId,
            DatabaseId = request.DatabaseId
        };
        await questionRepository.UpdateAsync(question, cancellationToken);
        
        var correctAnswer = new CorrectAnswer(request.CorrectAnswerId)
        {
            Body = request.CorrectAnswerBody,
            QuestionId = request.Id
        };
        await correctAnswerRepository.UpdateAsync(correctAnswer, cancellationToken);
    }

    protected override Task HandleErrorAsync(
        UpdateQuestionRequest request, 
        Exception exception,
        CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Question with id {Id} has not been updated", request.Id);
        return Task.CompletedTask;
    }
}