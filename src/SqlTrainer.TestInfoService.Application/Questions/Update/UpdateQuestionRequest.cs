﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Questions.Update;

public sealed record UpdateQuestionRequest(
    Guid Id,
    string Body,
    int Complexity,
    Guid TopicId,
    Guid DatabaseId,
    Guid CorrectAnswerId,
    string CorrectAnswerBody) 
    : IRequest;