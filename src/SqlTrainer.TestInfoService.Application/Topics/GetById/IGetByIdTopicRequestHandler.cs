﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.GetById;

public interface IGetByIdTopicRequestHandler : IRequestHandler<GetByIdTopicRequest, Topic>
{
}