﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.GetById;

public sealed class GetByIdTopicRequestHandler : RequestHandler<GetByIdTopicRequest, Topic>, IGetByIdTopicRequestHandler
{
    private readonly ITopicRepository topicRepository;
    private readonly ILogger<GetByIdTopicRequestHandler> logger;

    public GetByIdTopicRequestHandler(ITopicRepository topicRepository, ILogger<GetByIdTopicRequestHandler> logger)
    {
        this.topicRepository = topicRepository;
        this.logger = logger;
    }

    protected override async Task<Topic?> HandleRequestAsync(GetByIdTopicRequest request, CancellationToken cancellationToken = default) => 
        await topicRepository.GetByIdAsync(request.Id, cancellationToken);

    protected override Task HandleErrorAsync(GetByIdTopicRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Topic with id {Id} has not been retrieved", request.Id);
        return Task.CompletedTask;
    }
}