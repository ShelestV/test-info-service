﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Topics.GetById;

public sealed record GetByIdTopicRequest(Guid Id) : IRequest;