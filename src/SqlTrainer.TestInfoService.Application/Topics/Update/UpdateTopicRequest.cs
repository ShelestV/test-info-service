﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Topics.Update;

public sealed record UpdateTopicRequest(Guid Id, string Name) : IRequest;