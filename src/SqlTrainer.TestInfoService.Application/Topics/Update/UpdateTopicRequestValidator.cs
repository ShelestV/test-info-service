﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Topics.Update;

public sealed class UpdateTopicRequestValidator : RequestValidator<UpdateTopicRequest>, IUpdateTopicValidator
{
    protected override Task<ValidationResult<UpdateTopicRequest>> ValidateAsync(
        ValidationResult<UpdateTopicRequest> validationResult, 
        UpdateTopicRequest request,
        CancellationToken cancellationToken = default)
    {
        
        if (request.Id == Guid.Empty)
            validationResult.FailValidation("Id is required.");
        
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required.");
        
        return Task.FromResult(validationResult);
    }
}