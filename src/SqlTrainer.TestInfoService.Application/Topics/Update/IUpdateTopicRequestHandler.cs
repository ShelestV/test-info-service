﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Topics.Update;

public interface IUpdateTopicRequestHandler : IRequestHandler<UpdateTopicRequest>
{
}