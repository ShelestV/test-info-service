﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.Update;

public sealed class UpdateTopicRequestHandler : RequestHandler<UpdateTopicRequest>, IUpdateTopicRequestHandler
{
    private readonly ITopicRepository topicRepository;
    private readonly ILogger<UpdateTopicRequestHandler> logger;

    public UpdateTopicRequestHandler(
        IUpdateTopicValidator validator,
        ITopicRepository topicRepository, 
        IUnitOfWork unitOfWork,
        ILogger<UpdateTopicRequestHandler> logger)
        : base(validator, unitOfWork)
    {
        this.topicRepository = topicRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(UpdateTopicRequest request, CancellationToken cancellationToken = default)
    {
        var topic = new Topic(request.Id) { Name = request.Name };
        await topicRepository.UpdateAsync(topic, cancellationToken);
    }

    protected override Task HandleErrorAsync(UpdateTopicRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Topic has not been updated. Id of topic: {Id}", request.Id);
        return Task.CompletedTask;
    }
}