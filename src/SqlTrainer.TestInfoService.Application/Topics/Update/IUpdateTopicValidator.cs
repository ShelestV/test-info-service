﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Topics.Update;

public interface IUpdateTopicValidator : IRequestValidator<UpdateTopicRequest>
{
}