﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Topics.Add;

public sealed record AddTopicRequest(string Name) : IRequest;