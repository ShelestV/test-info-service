﻿using Application.Mediator.Handlers;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.Add;

public sealed class AddTopicRequestHandler : RequestHandler<AddTopicRequest>, IAddTopicRequestHandler
{
    private readonly ITopicRepository topicRepository;
    private readonly ILogger<AddTopicRequestHandler> logger;
    
    public AddTopicRequestHandler(
        IAddTopicRequestValidator validator,
        ITopicRepository topicRepository, 
        IUnitOfWork unitOfWork,
        ILogger<AddTopicRequestHandler> logger)
        : base(validator, unitOfWork)
    {
        this.topicRepository = topicRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(AddTopicRequest request, CancellationToken cancellationToken = default)
    {
        var topic = new Topic { Name = request.Name };
        await topicRepository.AddAsync(topic, cancellationToken);
    }

    protected override Task HandleErrorAsync(AddTopicRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Topic has not been added. Name of topic: {Name}", request.Name);
        return Task.CompletedTask;
    }
}