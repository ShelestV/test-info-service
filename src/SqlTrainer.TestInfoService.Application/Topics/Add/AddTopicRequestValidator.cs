﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Topics.Add;

public sealed class AddTopicRequestValidator : RequestValidator<AddTopicRequest>, IAddTopicRequestValidator
{
    protected override Task<ValidationResult<AddTopicRequest>> ValidateAsync(
        ValidationResult<AddTopicRequest> validationResult, 
        AddTopicRequest request,
        CancellationToken cancellationToken = default)
    {
        if (string.IsNullOrWhiteSpace(request.Name))
            validationResult.FailValidation("Name is required.");

        return Task.FromResult(validationResult);
    }
}