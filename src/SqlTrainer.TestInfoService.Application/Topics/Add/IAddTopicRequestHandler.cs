﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Topics.Add;

public interface IAddTopicRequestHandler : IRequestHandler<AddTopicRequest>
{
}