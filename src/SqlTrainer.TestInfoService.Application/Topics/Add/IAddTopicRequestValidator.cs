﻿using Application.Mediator.Validators;

namespace SqlTrainer.TestInfoService.Application.Topics.Add;

public interface IAddTopicRequestValidator : IRequestValidator<AddTopicRequest>
{
}