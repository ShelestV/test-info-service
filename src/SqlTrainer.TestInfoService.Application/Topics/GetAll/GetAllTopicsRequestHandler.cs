﻿using Application.Mediator.Handlers;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.GetAll;

public sealed class GetAllTopicsRequestHandler : RequestHandler<GetAllTopicsRequest, IReadOnlyCollection<Topic>>, IGetAllTopicsRequestHandler
{
    private readonly ITopicRepository topicRepository;
    private readonly ILogger<GetAllTopicsRequestHandler> logger;

    public GetAllTopicsRequestHandler(ITopicRepository topicRepository, ILogger<GetAllTopicsRequestHandler> logger)
    {
        this.topicRepository = topicRepository;
        this.logger = logger;
    }

    protected override async Task<IReadOnlyCollection<Topic>?> HandleRequestAsync(GetAllTopicsRequest request, CancellationToken cancellationToken = default) => 
        await topicRepository.GetAllAsync(request.SearchTerm, request.OrderBy, request.OrderByDirection, request.Page, request.PageSize, cancellationToken);

    protected override Task HandleErrorAsync(GetAllTopicsRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Topics have not been retrieved");
        return Task.CompletedTask;
    }
}