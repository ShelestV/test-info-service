﻿using Application.Mediator.Requests;
using Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Topics.GetAll;

public sealed record GetAllTopicsRequest(string SearchTerm, string OrderBy, SortDirection OrderByDirection, int Page, int PageSize)
    : GetManyRequest(SearchTerm, OrderBy, OrderByDirection, Page, PageSize);