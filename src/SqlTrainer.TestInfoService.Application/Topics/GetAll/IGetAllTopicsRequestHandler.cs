﻿using Application.Mediator.Handlers;
using SqlTrainer.TestInfoService.Domain.Models;

namespace SqlTrainer.TestInfoService.Application.Topics.GetAll;

public interface IGetAllTopicsRequestHandler : IRequestHandler<GetAllTopicsRequest, IReadOnlyCollection<Topic>>
{
}