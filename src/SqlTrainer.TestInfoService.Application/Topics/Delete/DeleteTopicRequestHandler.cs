﻿using Application.Mediator.Handlers;
using Application.Mediator.Validators;
using Application.Repositories;
using Microsoft.Extensions.Logging;
using SqlTrainer.TestInfoService.Application.Repositories;

namespace SqlTrainer.TestInfoService.Application.Topics.Delete;

public sealed class DeleteTopicRequestHandler : RequestHandler<DeleteTopicRequest>, IDeleteTopicRequestHandler
{
    private readonly ITopicRepository topicRepository;
    private readonly ILogger<DeleteTopicRequestHandler> logger;

    public DeleteTopicRequestHandler(
        ITopicRepository topicRepository, 
        IUnitOfWork unitOfWork,
        ILogger<DeleteTopicRequestHandler> logger)
        : base(unitOfWork: unitOfWork)
    {
        this.topicRepository = topicRepository;
        this.logger = logger;
    }

    protected override async Task HandleRequestAsync(DeleteTopicRequest request, CancellationToken cancellationToken = default)
    {
        await topicRepository.DeleteAsync(request.Id, cancellationToken);
    }

    protected override Task HandleErrorAsync(DeleteTopicRequest request, Exception exception, CancellationToken cancellationToken = default)
    {
        logger.LogError(exception, "Topic has not been deleted. Id of topic: {Id}", request.Id);
        return Task.CompletedTask;
    }
}