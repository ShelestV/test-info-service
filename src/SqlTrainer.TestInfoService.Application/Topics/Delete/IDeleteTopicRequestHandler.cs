﻿using Application.Mediator.Handlers;

namespace SqlTrainer.TestInfoService.Application.Topics.Delete;

public interface IDeleteTopicRequestHandler : IRequestHandler<DeleteTopicRequest>
{
}