﻿using Application.Mediator.Requests;

namespace SqlTrainer.TestInfoService.Application.Topics.Delete;

public sealed record DeleteTopicRequest(Guid Id) : IRequest;