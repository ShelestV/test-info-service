﻿using DatabaseHelper.ScriptResults;
using Results;

namespace SqlTrainer.TestInfoService.Application.Services;

public interface IScriptService
{
    Task<Result<ScriptResult>> ExecuteScriptAsync(string script, Guid databaseId, CancellationToken cancellationToken = default);
    Task<Result<IReadOnlyCollection<ScriptResult>>> ExecuteScriptsAsync(IEnumerable<string> scripts, Guid databaseId, CancellationToken cancellationToken = default);
}