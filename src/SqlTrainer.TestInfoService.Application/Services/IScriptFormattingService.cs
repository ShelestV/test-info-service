﻿namespace SqlTrainer.TestInfoService.Application.Services;

public interface IScriptFormattingService
{
    string? Clear(string? query);
}