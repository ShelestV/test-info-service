﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetQuestionDto : IDatabaseGetDto
{
    public required Guid Id { get; set; }
    public required string Body { get; set; }
    public required int Complexity { get; set; }
    public required Guid CorrectAnswerId { get; set; }
    public required string CorrectAnswerBody { get; set; }
    public required Guid TopicId { get; set; }
    public required Guid DatabaseId { get; set; }
}