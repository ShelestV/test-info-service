﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetCorrectAnswerDto : IDatabaseGetDto
{
    public required Guid Id { get; set; }
    public required string Body { get; set; }
    public required Guid QuestionId { get; set; }
    public required string QuestionBody { get; set; }
    public required int QuestionComplexity { get; set; }
    public required Guid TopicId { get; set; }
    public required Guid DatabaseId { get; set; }
}