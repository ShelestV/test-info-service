﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetTestDto : IDatabaseGetDto
{
    public required Guid Id { get; set; }
    public required string Name { get; set; }
    public required DateTimeOffset CreatedAt { get; set; }
    public required string Questions { get; set; }
}