﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetScheduleTestDto : IDatabaseGetDto
{
    public required Guid Id { get; set; }
    public required DateTimeOffset StartAt { get; set; }
    public required DateTimeOffset FinishAt { get; set; }
    public required Guid TestId { get; set; }
    public required string TestName { get; set; }
    public required DateTimeOffset TestCreatedAt { get; set; }
    public required string UserScheduleTests { get; set; }
}