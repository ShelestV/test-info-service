﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetTopicDto : IDatabaseGetDto
{
    public required Guid Id { get; set; }
    public required string Name { get; set; }
}