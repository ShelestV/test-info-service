﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetUserAnswerDto : IDatabaseGetDto
{
    public required Guid UserId { get; set; }
    public required Guid ScheduleTestId { get; set; }
    public required Guid QuestionId { get; set; }
    public required string Body { get; set; }
    public required double ProgramMark { get; set; }
    public required double TeacherMark { get; set; }
    public required double QuestionMaxMark { get; set; }
    public required DateTimeOffset AnsweredAt { get; set; }
    public required DateTimeOffset ScheduleTestStartAt { get; set; }
    public required DateTimeOffset ScheduleTestFinishAt { get; set; }
    public required string QuestionBody { get; set; }
    public required int QuestionComplexity { get; set; }
    public required Guid QuestionTopicId { get; set; }
    public required Guid QuestionDatabaseId { get; set; }
    public required Guid CorrectAnswerId { get; set; }
    public required string CorrectAnswerBody { get; set; }
    public required Guid TestId { get; set; }
    public required string TestName { get; set; }
    public required DateTimeOffset CreatedAt { get; set; }
}