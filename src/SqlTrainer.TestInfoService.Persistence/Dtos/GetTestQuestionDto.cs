﻿using Persistence.Dtos;

namespace SqlTrainer.TestInfoService.Persistence.Dtos;

public sealed class GetTestQuestionDto : IDatabaseGetDto
{
    public required Guid TestId { get; set; }
    public required Guid QuestionId { get; set; }
    public required double MaxMark { get; set; }
}