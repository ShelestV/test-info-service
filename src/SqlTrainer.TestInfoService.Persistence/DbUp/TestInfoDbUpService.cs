﻿using System.Reflection;
using Persistence.Configurations;
using PostgresPersistence.DbUp;

namespace SqlTrainer.TestInfoService.Persistence.DbUp;

public sealed class TestInfoDbUpService : DbUpService
{
    public TestInfoDbUpService(IDatabaseConfiguration configuration) : base(configuration)
    {
        this.MigrationAssembly = Assembly.GetExecutingAssembly();
    }

    protected override Assembly MigrationAssembly { get; }
}