﻿using Application.Repositories;

namespace SqlTrainer.TestInfoService.Persistence.Helpers;

public static class SortDirectionExtensions
{
    public static string ToSqlString(this SortDirection sortDirection) => sortDirection switch
    {
        SortDirection.Ascending => "asc",
        SortDirection.Descending => "desc",
        _ => throw new ArgumentOutOfRangeException(nameof(sortDirection), sortDirection, "Invalid sort direction")
    };

    public static SortDirection FromSqlString(this string sortDirection) => sortDirection switch
    {
        "desc" => SortDirection.Descending,
        _ => SortDirection.Ascending
    };
}