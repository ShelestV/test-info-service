﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Test;

public sealed record AddTestInputParameters(Guid Id, string Name, DateTimeOffset CreatedAt) : IDatabaseInputParameters;