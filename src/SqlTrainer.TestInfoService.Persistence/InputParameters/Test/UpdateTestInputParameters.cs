﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Test;

public sealed record UpdateTestInputParameters(Guid Id, string Name) : IDatabaseInputParameters;