﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record AddRangeTestQuestionsInputParameters(
    Guid TestId, 
    TestQuestionParameter[] QuestionMaxMarks) 
    : IDatabaseInputParameters;

public sealed record TestQuestionParameter(Guid QuestionId, double MaxMark);