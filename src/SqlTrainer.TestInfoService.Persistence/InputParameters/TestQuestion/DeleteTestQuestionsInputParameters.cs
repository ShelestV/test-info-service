﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record DeleteTestQuestionsInputParameters(Guid TestId, Guid[] QuestionIds) : IDatabaseInputParameters;