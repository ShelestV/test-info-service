﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record DeleteTestQuestionsByTestInputParameters(Guid TestId) : IDatabaseInputParameters;