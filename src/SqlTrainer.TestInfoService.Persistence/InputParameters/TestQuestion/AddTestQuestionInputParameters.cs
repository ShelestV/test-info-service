﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record AddTestQuestionInputParameters(Guid TestId, Guid QuestionId, double MaxMark) : IDatabaseInputParameters;