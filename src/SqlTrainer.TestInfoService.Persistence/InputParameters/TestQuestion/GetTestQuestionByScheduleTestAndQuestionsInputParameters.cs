﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record GetTestQuestionByScheduleTestAndQuestionsInputParameters(
        Guid ScheduleTestId,
        IEnumerable<Guid> QuestionIds) 
    : IDatabaseInputParameters;