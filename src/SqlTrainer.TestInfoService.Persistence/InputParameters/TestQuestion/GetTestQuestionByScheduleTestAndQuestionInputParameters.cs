﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

public sealed record GetTestQuestionByScheduleTestAndQuestionInputParameters(Guid ScheduleTestId, Guid QuestionId) 
    : IDatabaseInputParameters;