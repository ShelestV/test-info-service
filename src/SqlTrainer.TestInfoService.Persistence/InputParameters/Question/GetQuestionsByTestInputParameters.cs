﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Question;

public sealed record GetQuestionsByTestInputParameters(
    Guid TestId,
    string SearchTerm,
    string OrderBy,
    string OrderByDirection,
    int Limit,
    int Offset) 
    : IDatabaseInputParameters;