﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Question;

public sealed record GetQuestionsByIdsInputParameters(Guid[] QuestionIds) : IDatabaseInputParameters;