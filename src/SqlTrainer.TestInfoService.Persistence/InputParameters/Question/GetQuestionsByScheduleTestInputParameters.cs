﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Question;

public sealed record GetQuestionsByScheduleTestInputParameters(Guid ScheduleTestId) : IDatabaseInputParameters;