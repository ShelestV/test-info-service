﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Question;

public sealed record AddQuestionInputParameters(Guid Id, string Body, int Complexity, Guid TopicId, Guid DatabaseId) 
    : IDatabaseInputParameters;