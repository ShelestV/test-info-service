﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.UserAnswers;

public sealed record GetByScheduleTestAndUserIdInputParameters(
        Guid ScheduleTestId,
        Guid UserId) 
    : IDatabaseInputParameters;