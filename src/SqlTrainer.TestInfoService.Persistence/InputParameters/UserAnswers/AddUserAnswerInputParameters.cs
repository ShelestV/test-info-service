﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.UserAnswers;

public sealed record AddUserAnswerInputParameters(
    Guid UserId,
    Guid ScheduleTestId,
    Guid QuestionId,
    string Body,
    double ProgramMark,
    DateTimeOffset AnsweredAt) 
    : IDatabaseInputParameters;