﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.UserAnswers;

public sealed record UpdateUserAnswerInputParameters(
        Guid UserId,
        Guid ScheduleTestId,
        Guid QuestionId,
        string Body,
        double TeacherMark,
        DateTimeOffset AnsweredAt) 
    : IDatabaseInputParameters;