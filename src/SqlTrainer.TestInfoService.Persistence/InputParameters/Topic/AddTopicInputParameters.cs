﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Topic;

public sealed record AddTopicInputParameters(Guid Id, string Name) : IDatabaseInputParameters;