﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.Topic;

public sealed record UpdateTopicInputParameters(Guid Id, string Name) : IDatabaseInputParameters;