﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.UserScheduleTest;

public sealed record UpdateUserScheduleTestInputParameters(
    Guid ScheduleTestId,
    Guid UserId,
    DateTimeOffset? FinishedAt,
    bool? CheckedByTeacher) 
    : IDatabaseInputParameters;