﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.CorrectAnswer;

public sealed record UpdateCorrectAnswerInputParameters(
    Guid Id,
    string Body) : IDatabaseInputParameters;