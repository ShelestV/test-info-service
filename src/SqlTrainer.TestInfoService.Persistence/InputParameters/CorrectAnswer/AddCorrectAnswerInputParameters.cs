﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.CorrectAnswer;

public sealed record AddCorrectAnswerInputParameters(Guid Id, string Body, Guid QuestionId) : IDatabaseInputParameters;