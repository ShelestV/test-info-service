﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.CorrectAnswer;

public sealed record GetCorrectAnswerByQuestionInputParameters(Guid QuestionId) : IDatabaseInputParameters;