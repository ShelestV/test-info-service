﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.CorrectAnswer;

public record GetCorrectAnswerByQuestionsInputParameters(Guid[] InQuestionIds) : IDatabaseInputParameters;