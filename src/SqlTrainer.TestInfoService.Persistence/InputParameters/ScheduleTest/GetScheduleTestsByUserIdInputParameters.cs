﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.ScheduleTest;

public sealed record GetScheduleTestsByUserIdInputParameters(
    Guid UserId,
    string SearchTerm,
    string OrderBy,
    string OrderByDirection,
    int Limit,
    int Offset) 
    : IDatabaseInputParameters;