﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.ScheduleTest;

public sealed record UpdateScheduleTestInputParameters(
        Guid Id,
        Guid TestId,
        Guid[] UserIds,
        DateTimeOffset StartAt,
        DateTimeOffset FinishAt)
    : IDatabaseInputParameters;