﻿using Persistence.InputParameters;

namespace SqlTrainer.TestInfoService.Persistence.InputParameters.ScheduleTest;

public sealed record AddScheduleTestInputParameters(
    Guid Id, 
    Guid TestId, 
    Guid[] UserIds, 
    DateTimeOffset StartAt, 
    DateTimeOffset FinishAt) 
    : IDatabaseInputParameters;