﻿create table public."topics"
(
    "id"   UUID
        constraint "topics_id_pk" primary key,
    "name" varchar(500)
        constraint "topics_name_notnull" not null
);

create table public."tests"
(
    "id"         UUID
        constraint "tests_id_pk" primary key,
    "name"       varchar(500)
        constraint "tests_name_notnull" not null,
    "created_at" timestamp with time zone
        constraint "tests_createdat_notnull" not null
);

create table public."questions"
(
    "id"          UUID
        constraint "questions_id_pk" primary key,
    "body"        VARCHAR(5000)
        constraint "questions_body_notnull" not null,
    "complexity"  int
        constraint "questions_maxmark_notnull" not null,
    "topic_id"    UUID
        constraint "topic_id_notnull" not null,
    "database_id" uuid
        constraint "database_id_notnull" not null,
    constraint "questions_topicid_topic_fk" foreign key ("topic_id") references "topics" ("id")
);

create table public."correct_answers"
(
    "id"          UUID
        constraint "correctanswers_id_pk" primary key,
    "body"        VARCHAR(5000)
        constraint "correctanswers_body_notnull" not null,
    "question_id" UUID
        constraint "correctanswers_questionid_notnull" not null,
    constraint "correctanswers_questionid_question_fk" foreign key ("question_id") references "questions" ("id")
);

create table public."test_questions"
(
    "test_id"     UUID
        constraint "testquestions_testid_notnull" not null,
    "question_id" UUID
        constraint "testquestions_questionid_notnull" not null,
    "max_mark"    double precision
        constraint "testquestions_maxmark_notnull" not null,
    constraint "testquestions_testid_questionid_pk" primary key ("test_id", "question_id"),
    constraint "testquestions_testid_tests_fk" foreign key ("test_id") references "tests" ("id"),
    constraint "testquestions_questionid_questions_fk" foreign key ("question_id") references "questions" ("id")
);

create table public."schedule_tests"
(
    "id"        UUID
        constraint "scheduletests_id_pk" primary key,
    "test_id"   UUID
        constraint "scheduletests_testid_notnull" not null,
    "start_at"  timestamp with time zone
        constraint "scheduletests_startat_notnull" not null,
    "finish_at" timestamp with time zone
        constraint "scheduletests_finishat_notnull" not null,
    constraint "scheduletests_testid_tests_fk" foreign key ("test_id") references "tests" ("id")
);

create table user_schedule_tests
(
    "schedule_test_id"   uuid
        CONSTRAINT "userscheduletests_scheduletestid_notnull" not null,
    "user_id"            uuid
        constraint "userscheduletests_userid_notnull" not null,
    "finished_at"        timestamp with time zone,
    "checked_by_teacher" boolean,
    constraint "userscheduletests_scheduletestid_scheduletests_fk" foreign key ("schedule_test_id") references "schedule_tests" ("id"),
    constraint "userscheduletests_scheduletestid_userid_pk" primary key (schedule_test_id, user_id)
);

create table public."user_answers"
(
    "user_id"          UUID
        constraint "useranswers_userid_notnull" not null,
    "schedule_test_id" UUID
        constraint "useranswers_scheduletestid_notnull" not null,
    "question_id"      UUID
        constraint "useranswers_questionid_notnull" not null,
    "body"             VARCHAR(5000)
        constraint "useranswers_body_notnull" not null,
    "program_mark"     double precision
        constraint "useranswers_programmark_notnull" not null,
    "teacher_mark"     double precision,
    "answered_at"      timestamp with time zone
        constraint "useranswers_answeredat_notnull" not null,
    constraint "useranswers_scheduletestid_userid_scheduletests_fk" foreign key ("schedule_test_id", "user_id") references "user_schedule_tests" ("schedule_test_id", "user_id"),
    constraint "useranswers_questionid_questions_fk" foreign key ("question_id") references "questions" ("id"),
    constraint "useranswers_userid_scheduletestid_questiondid_pk" primary key ("user_id", "schedule_test_id", "question_id")
);

create or replace procedure public."insert_test"(
    in "Id" uuid,
    in "Name" varchar(500),
    in "CreatedAt" timestamp with time zone
)
    language 'sql'
as
$$
insert into public.tests("id", "name", "created_at")
values ("Id", "Name", "CreatedAt");
$$;

create type public."question_max_mark" as
(
    "question_id" uuid,
    "max_mark"    double precision
);

create or replace procedure public."insert_test_questions"(
    in "TestId" uuid,
    in "QuestionMaxMarks" public."question_max_mark"[]
)
    language 'plpgsql'
as
$$
declare
    questionMaxMark public."question_max_mark";
begin
    foreach questionMaxMark in array "QuestionMaxMarks"
        loop
            insert into public.test_questions("test_id", "question_id", "max_mark")
            values ("TestId", questionMaxMark."question_id", questionMaxMark."max_mark");
        end loop;
end;
$$;

create or replace procedure public."insert_test_question"(
    in "TestId" uuid,
    in "QuestionId" uuid,
    in "MaxMark" double precision
)
    language 'sql'
as
$$
insert into public.test_questions("test_id", "question_id", "max_mark")
values ("TestId", "QuestionId", "MaxMark");
$$;

create or replace procedure public."insert_question"(
    in "Id" uuid,
    in "Body" varchar(5000),
    in "Complexity" int,
    in "TopicId" uuid,
    in "DatabaseId" uuid
)
    language 'sql'
as
$$
insert into public."questions" ("id", "body", "complexity", "topic_id", "database_id")
values ("Id", "Body", "Complexity", "TopicId", "DatabaseId");
$$;

create or replace procedure public."insert_correct_answer"(
    in "Id" uuid,
    in "Body" varchar(5000),
    in "QuestionId" uuid
)
    language 'sql'
as
$$
insert into public."correct_answers" ("id", "body", "question_id")
values ("Id", "Body", "QuestionId");
$$;

create or replace procedure public."update_correct_answer"(
    in "Id" uuid,
    in "Body" varchar(5000)
)
    language 'sql'
as
$$
update public."correct_answers"
set "body" = "Body"
where "id" = "Id";
$$;

create or replace function public."get_correct_answer_by_question_id"(
    in "InQuestionId" uuid
)
    returns table
            (
                "Id"                 uuid,
                "Body"               varchar(5000),
                "QuestionId"         uuid,
                "QuestionBody"       varchar(5000),
                "QuestionComplexity" int,
                "TopicId"            uuid,
                "DatabaseId"         uuid
            )
    language 'plpgsql'
as
$$
begin
    return query select ca."id", ca."body", ca."question_id", q."body", q."complexity", q."topic_id", q."database_id"
                 from correct_answers ca
                          inner join questions q on ca.question_id = q.id
                 where ca.question_id = "InQuestionId";
end;
$$;

create or replace function public."get_correct_answer_by_question_ids"(
    in "InQuestionIds" uuid[]
)
    returns table
            (
                "Id"                 uuid,
                "Body"               varchar(5000),
                "QuestionId"         uuid,
                "QuestionBody"       varchar(5000),
                "QuestionComplexity" int,
                "TopicId"            uuid,
                "DatabaseId"         uuid
            )
    language 'plpgsql'
as
$$
begin
    return query select ca."id", ca."body", ca."question_id", q."body", q."complexity", q."topic_id", q."database_id"
                 from correct_answers ca
                          inner join questions q on ca.question_id = q.id
                 where ca.question_id = any ("InQuestionIds");
end;
$$;

create or replace function public."get_all_tests"(
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderDirection" varchar(5)
)
    returns table
            (
                "Id"        uuid,
                "Name"      varchar(500),
                "CreatedAt" timestamp with time zone,
                "Questions" json
            )
    language 'plpgsql'
as
$$
begin
    return query select t."id",
                        t."name",
                        t."created_at",
                        array_to_json(array_agg(qca))
                 from tests t
                          left join (select tq.test_id    "TestId",
                                            tq.max_mark   "MaxMark",
                                            q.id          "Id",
                                            q.body        "Body",
                                            q.complexity  "Complexity",
                                            q.topic_id    "TopicId",
                                            q.database_id "DatabaseId",
                                            ca.id         "CorrectAnswerId",
                                            ca.body       "CorrectAnswerBody"
                                     from questions q
                                              inner join correct_answers ca on ca.question_id = q.id
                                              left join test_questions tq on tq.question_id = q.id) as qca
                                    on t.id = qca."TestId"
                 where t."name" like '%' || "SearchTerm" || '%'
                 group by t."id", t."name", t."created_at"
                 order by (case when "OrderBy" = 'name' and "OrderDirection" = 'asc' then t."name" end),
                          (case when "OrderBy" = 'name' and "OrderDirection" = 'desc' then t."name" end) desc,
                          (case when "OrderBy" = 'created_at' and "OrderDirection" = 'asc' then t."created_at" end),
                          (case
                               when "OrderBy" = 'created_at' and "OrderDirection" = 'desc'
                                   then t."created_at" end) desc,
                          (case when "OrderDirection" = 'asc' then t."id" end),
                          (case when "OrderDirection" = 'desc' then t."id" end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace function public."get_all_questions"(
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderByDirection" varchar(5)
)
    returns table
            (
                "Id"                uuid,
                "Body"              varchar(5000),
                "Complexity"        int,
                "TopicId"           uuid,
                "DatabaseId"        uuid,
                "CorrectAnswerId"   uuid,
                "CorrectAnswerBody" varchar(5000)
            )
    language 'plpgsql'
as
$$
begin
    return query select q.id, q.body, q.complexity, q.topic_id, q.database_id, ca.id, ca.body
                 from questions q
                          inner join correct_answers ca on q.id = ca.question_id
                 where q."body" like '%' || "SearchTerm" || '%'
                 order by (case when "OrderBy" = 'body' and "OrderByDirection" = 'asc' then q."body" end),
                          (case when "OrderBy" = 'body' and "OrderByDirection" = 'desc' then q."body" end) desc,
                          (case when "OrderBy" = 'complexity' and "OrderByDirection" = 'asc' then q."complexity" end),
                          (case
                               when "OrderBy" = 'complexity' and "OrderByDirection" = 'desc'
                                   then q."complexity" end) desc,
                          (case when "OrderByDirection" = 'asc' then q."id" end),
                          (case when "OrderByDirection" = 'desc' then q."id" end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace procedure public."delete_question"(
    in "Id" uuid
)
    language 'sql'
as
$$
delete
from public.user_answers
where question_id = "Id";

delete
from public.test_questions
where question_id = "Id";

delete
from public.correct_answers
where question_id = "Id";

delete
from public.questions
where id = "Id";
$$;

create or replace procedure public."delete_test"(
    in "Id" uuid
)
    language 'sql'
as
$$
delete
from public.user_answers
where schedule_test_id in (select st.id from public.schedule_tests st where st.test_id = "Id");

delete
from public.user_schedule_tests
where schedule_test_id in (select st.id from public.schedule_tests st where st.test_id = "Id");

delete
from public.schedule_tests
where test_id = "Id";

delete
from public.test_questions
where test_id = "Id";

delete
from public.tests
where id = "Id"
$$;

create or replace procedure public."update_question"(
    in "Id" uuid,
    in "Body" varchar(5000),
    in "Complexity" double precision,
    in "TopicId" uuid,
    in "DatabaseId" uuid
)
    language 'sql'
as
$$
update questions
set body        = "Body",
    complexity  = "Complexity",
    topic_id    = "TopicId",
    database_id = "DatabaseId"
where id = "Id"
$$;

create or replace procedure public."update_test"(
    in "Id" uuid,
    in "Name" varchar(500)
)
    language 'sql'
as
$$
update tests
set name = "Name"
where id = "Id"
$$;

create or replace procedure public."insert_topic"(
    in "Id" uuid,
    in "Name" varchar(500)
)
    language 'sql'
as
$$
insert into public."topics" ("id", "name")
values ("Id", "Name");
$$;

create or replace procedure public."update_topic"(
    in "Id" uuid,
    in "Name" varchar(500)
)
    language 'sql'
as
$$
update topics
set name = "Name"
where id = "Id";
$$;

create or replace procedure public."delete_topic"(
    in "Id" uuid
)
    language 'sql'
as
$$
delete
from public.user_answers
where question_id in (select id from public.questions q where q.topic_id = "Id");

delete
from public.test_questions
where question_id in (select id from public.questions q where q.topic_id = "Id");

delete
from public.correct_answers
where question_id in (select id from public.questions q where q.topic_id = "Id");
    
delete
from public.questions
where topic_id = "Id";

delete
from public.topics
where id = "Id";
$$;

create or replace function public."get_all_topics"(
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderByDirection" varchar(5)
)
    returns table
            (
                "Id"   uuid,
                "Name" varchar(500)
            )
    language 'plpgsql'
as
$$
begin
    return query select t.id, t.name
                 from topics t
                 where t."name" like '%' || "SearchTerm" || '%'
                 order by (case when "OrderBy" = 'name' and "OrderByDirection" = 'asc' then t."name" end),
                          (case when "OrderBy" = 'name' and "OrderByDirection" = 'desc' then t."name" end) desc,
                          (case when "OrderByDirection" = 'asc' then t."id" end),
                          (case when "OrderByDirection" = 'desc' then t."id" end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace function public."get_topic_by_id"(
    in "TopicId" uuid
)
    returns table
            (
                "Id"   uuid,
                "Name" varchar(500)
            )
    language 'plpgsql'
as
$$
begin
    return query select t.id, t.name
                 from topics t
                 where t."id" = "TopicId";
end;
$$;

create or replace function public."get_questions_by_test_id"(
    in "TestId" uuid,
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderByDirection" varchar(5)
)
    returns table
            (
                "Id"                uuid,
                "Body"              varchar(5000),
                "Complexity"        int,
                "TopicId"           uuid,
                "DatabaseId"        uuid,
                "CorrectAnswerId"   uuid,
                "CorrectAnswerBody" varchar(5000)
            )
    language 'plpgsql'
as
$$
begin
    return query select q.id, q.body, q.complexity, q.topic_id, q.database_id, ca.id, ca.body
                 from questions q
                          inner join correct_answers ca on q.id = ca.question_id
                 where q.id in (select "question_id" from test_questions where "test_id" = "TestId")
                   and q."body" like '%' || "SearchTerm" || '%'
                 order by (case when "OrderBy" = 'body' and "OrderByDirection" = 'asc' then q."body" end),
                          (case when "OrderBy" = 'body' and "OrderByDirection" = 'desc' then q."body" end) desc,
                          (case when "OrderBy" = 'complexity' and "OrderByDirection" = 'asc' then q."complexity" end),
                          (case
                               when "OrderBy" = 'complexity' and "OrderByDirection" = 'desc'
                                   then q."complexity" end) desc,
                          (case when "OrderByDirection" = 'asc' then q."id" end),
                          (case when "OrderByDirection" = 'desc' then q."id" end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace function public."get_question_by_id"(
    in "QuestionId" uuid
)
    returns table
            (
                "Id"                uuid,
                "Body"              varchar(5000),
                "Complexity"        int,
                "TopicId"           uuid,
                "DatabaseId"        uuid,
                "CorrectAnswerId"   uuid,
                "CorrectAnswerBody" varchar(5000)
            )
    language 'plpgsql'
as
$$
begin
    return query select q.id, q.body, q.complexity, q.topic_id, q.database_id, ca.id, ca.body
                 from questions q
                          inner join correct_answers ca on q.id = ca.question_id
                 where q.id = "QuestionId";
end;
$$;

create or replace function public."get_test_by_id"(
    in "InTestId" uuid
)
    returns table
            (
                "Id"        uuid,
                "Name"      varchar(500),
                "CreatedAt" timestamp with time zone,
                "Questions" json
            )
    language 'plpgsql'
as
$$
begin
    return query select t."id",
                        t."name",
                        t."created_at",
                        array_to_json(array_agg(qca))
                 from tests t
                          left join (select tq.test_id    "TestId",
                                            tq.max_mark   "MaxMark",
                                            q.id          "Id",
                                            q.body        "Body",
                                            q.complexity  "Complexity",
                                            q.topic_id    "TopicId",
                                            q.database_id "DatabaseId",
                                            ca.id         "CorrectAnswerId",
                                            ca.body       "CorrectAnswerBody"
                                     from questions q
                                              inner join correct_answers ca on ca.question_id = q.id
                                              left join test_questions tq ON tq.question_id = q.id) as qca
                                    on t.id = qca."TestId"
                 where t."id" = "InTestId"
                 group by t."id", t."name", t."created_at";
end;
$$;

create or replace procedure public."delete_test_questions"(
    in "TestId" uuid,
    in "QuestionIds" uuid[]
)
    language 'plpgsql'
as
$$
declare
    questionId uuid;
begin
    foreach questionId in array "QuestionIds"
        loop
            delete
            from public.user_answers
            where "schedule_test_id" in (select t.id from schedule_tests t where t.test_id = "TestId")
              and "question_id" = questionId;

            delete
            from public.test_questions
            where "test_id" = "TestId"
              and "question_id" = questionId;
        end loop;
end;
$$;

create or replace procedure public."delete_test_questions_by_test"(
    in "TestId" uuid
)
    language 'sql'
as
$$
delete
from public.test_questions
where "test_id" = "TestId";
$$;

create or replace procedure update_test_question(
    in "TestId" uuid,
    in "QuestionId" uuid,
    in "MaxMark" double precision
)
    language 'sql'
as
$$
update test_questions
set max_mark = "MaxMark"
where test_id = "TestId"
  and question_id = "QuestionId";
$$;

create or replace function public."get_all_schedule_tests"(
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderByDirection" varchar(5)
)
    returns table
            (
                "Id"                uuid,
                "TestId"            uuid,
                "TestName"          varchar(500),
                "TestCreatedAt"     timestamp with time zone,
                "StartAt"           timestamp with time zone,
                "FinishAt"          timestamp with time zone,
                "UserScheduleTests" json
            )
    language 'plpgsql'
as
$$
begin
    return query select st.id,
                        st.test_id,
                        t.name,
                        t.created_at,
                        st.start_at,
                        st.finish_at,
                        array_to_json(array_agg(ust))
                 from schedule_tests st
                          inner join tests t on st.test_id = t.id
                          inner join (select iust.schedule_test_id                         "ScheduleTestId",
                                             iust.user_id                                  "UserId",
                                             iust.finished_at                              "FinishedAt",
                                             iust.checked_by_teacher                       "CheckedByTeacher",
                                             sum(iua.program_mark)                         "ProgramMark",
                                             sum(iua.teacher_mark)                         "TeacherMark",
                                             sum((select tq.max_mark
                                                  from test_questions tq
                                                  where tq.test_id in (select ist.test_id
                                                                       from schedule_tests ist
                                                                       where ist.id = iust.schedule_test_id)
                                                    and tq.question_id = iua.question_id)) "QuestionMaxMark"
                                      from user_schedule_tests iust
                                               left join user_answers iua
                                                         on iust.schedule_test_id = iua.schedule_test_id and
                                                            iust.user_id = iua.user_id
                                      group by iust.schedule_test_id, iust.user_id, iust.finished_at,
                                               iust.checked_by_teacher
                                      order by iust.checked_by_teacher desc,
                                               case iust.checked_by_teacher
                                                   when true then sum(iua.teacher_mark)
                                                   when false then sum(iua.program_mark)
                                                   end desc, iust.finished_at) ust on st.id = ust."ScheduleTestId"
                 where t.name like '%' || "SearchTerm" || '%'
                 group by st.id, st.test_id, t.name, t.created_at, st.start_at, st.finish_at
                 order by (case when "OrderBy" = 'test_name' and "OrderByDirection" = 'asc' then t.name end),
                          (case when "OrderBy" = 'test_name' and "OrderByDirection" = 'desc' then t.name end) desc,
                          (case when "OrderBy" = 'start_at' and "OrderByDirection" = 'asc' then st.start_at end),
                          (case when "OrderBy" = 'start_at' and "OrderByDirection" = 'desc' then st.start_at end) desc,
                          (case when "OrderBy" = 'finish_at' and "OrderByDirection" = 'asc' then st.finish_at end),
                          (case
                               when "OrderBy" = 'finish_at' and "OrderByDirection" = 'desc' then st.finish_at end) desc,
                          (case when "OrderByDirection" = 'asc' then st.id end),
                          (case when "OrderByDirection" = 'desc' then st.id end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace function public."get_schedule_tests_by_user_id"(
    in "InUserId" uuid,
    in "SearchTerm" varchar(500),
    in "Limit" int,
    in "Offset" int,
    in "OrderBy" varchar(50),
    in "OrderByDirection" varchar(5)
)
    returns table
            (
                "Id"                uuid,
                "TestId"            uuid,
                "TestName"          varchar(500),
                "TestCreatedAt"     timestamp with time zone,
                "StartAt"           timestamp with time zone,
                "FinishAt"          timestamp with time zone,
                "UserScheduleTests" json
            )
    language 'plpgsql'
as
$$
begin
    return query select st.id,
                        st.test_id,
                        t.name,
                        t.created_at,
                        st.start_at,
                        st.finish_at,
                        array_to_json(array_agg(ust))
                 from schedule_tests st
                          inner join tests t on st.test_id = t.id
                          inner join (select iust.schedule_test_id                         "ScheduleTestId",
                                             iust.user_id                                  "UserId",
                                             iust.finished_at                              "FinishedAt",
                                             iust.checked_by_teacher                       "CheckedByTeacher",
                                             sum(iua.program_mark)                         "ProgramMark",
                                             sum(iua.teacher_mark)                         "TeacherMark",
                                             sum((select tq.max_mark
                                                  from test_questions tq
                                                  where tq.test_id in (select ist.test_id
                                                                       from schedule_tests ist
                                                                       where ist.id = iust.schedule_test_id)
                                                    and tq.question_id = iua.question_id)) "QuestionMaxMark"
                                      from user_schedule_tests iust
                                               left join user_answers iua
                                                         on iust.schedule_test_id = iua.schedule_test_id and
                                                            iust.user_id = iua.user_id
                                      group by iust.schedule_test_id, iust.user_id, iust.finished_at,
                                               iust.checked_by_teacher
                                      order by iust.checked_by_teacher desc,
                                               case iust.checked_by_teacher
                                                   when true then sum(iua.teacher_mark)
                                                   when false then sum(iua.program_mark)
                                                   end desc, iust.finished_at) ust on st.id = ust."ScheduleTestId"
                 where "InUserId" = ust."UserId"
                   and t.name like '%' || "SearchTerm" || '%'
                 group by st.id, st.test_id, t.name, t.created_at, st.start_at, st.finish_at
                 order by (case when "OrderBy" = 'test_name' and "OrderByDirection" = 'asc' then t.name end),
                          (case when "OrderBy" = 'test_name' and "OrderByDirection" = 'desc' then t.name end) desc,
                          (case when "OrderBy" = 'start_at' and "OrderByDirection" = 'asc' then st.start_at end),
                          (case when "OrderBy" = 'start_at' and "OrderByDirection" = 'desc' then st.start_at end) desc,
                          (case when "OrderBy" = 'finish_at' and "OrderByDirection" = 'asc' then st.finish_at end),
                          (case
                               when "OrderBy" = 'finish_at' and "OrderByDirection" = 'desc' then st.finish_at end) desc,
                          (case when "OrderByDirection" = 'asc' then st.id end),
                          (case when "OrderByDirection" = 'desc' then st.id end) desc
                 limit "Limit" offset "Offset";
end;
$$;

create or replace procedure public."insert_schedule_test"(
    in "Id" uuid,
    in "TestId" uuid,
    in "UserIds" uuid[],
    in "StartAt" timestamp with time zone,
    in "FinishAt" timestamp with time zone
)
    language 'plpgsql'
as
$$
declare
    userId uuid;
begin
    insert into schedule_tests ("id", "test_id", "start_at", "finish_at")
    values ("Id", "TestId", "StartAt", "FinishAt");
    foreach userId in array "UserIds"
        loop
            insert into user_schedule_tests (user_id, schedule_test_id)
            values (userId, "Id");
        end loop;
end;
$$;

create or replace procedure public."update_schedule_test"(
    in "Id" uuid,
    in "TestId" uuid,
    in "UserIds" uuid[],
    in "StartAt" timestamp with time zone,
    in "FinishAt" timestamp with time zone
)
    language 'plpgsql'
as
$$
declare
    userId uuid;
begin
    update schedule_tests
    set "test_id"   = "TestId",
        "start_at"  = "StartAt",
        "finish_at" = "FinishAt"
    where "id" = "Id";

    delete
    from user_schedule_tests
    where schedule_test_id = "Id";

    foreach userId in array "UserIds"
        loop
            insert into user_schedule_tests (user_id, schedule_test_id)
            values (userId, "Id");
        end loop;
end;
$$;

create or replace procedure public."delete_schedule_test"(
    in "Id" uuid
)
    language 'sql'
as
$$
delete
from user_answers
where schedule_test_id = "Id";
    
delete
from user_schedule_tests
where schedule_test_id = "Id";

delete
from schedule_tests
where "id" = "Id";
$$;

create or replace procedure "insert_user_answer"(
    in "UserId" uuid,
    in "ScheduleTestId" uuid,
    in "QuestionId" uuid,
    in "Body" varchar(5000),
    in "ProgramMark" double precision,
    in "AnsweredAt" timestamp with time zone
)
    language 'sql'
as
$$
insert into public."user_answers" ("user_id", "question_id", "schedule_test_id", "body", "program_mark", "teacher_mark",
                                   "answered_at")
values ("UserId", "QuestionId", "ScheduleTestId", "Body", "ProgramMark", 0.0, "AnsweredAt");
$$;

create or replace function "get_questions_by_ids"(
    in "QuestionIds" uuid[]
)
    returns table
            (
                "Id"                uuid,
                "Body"              varchar(5000),
                "Complexity"        int,
                "TopicId"           uuid,
                "DatabaseId"        uuid,
                "CorrectAnswerId"   uuid,
                "CorrectAnswerBody" varchar(5000)
            )
    language 'plpgsql'
as
$$
begin
    return query select q.id, q.body, q.complexity, q.topic_id, q.database_id, ca.id, ca.body
                 from questions q
                          inner join correct_answers ca on q.id = ca.question_id
                 where q.id = any ("QuestionIds");
end;
$$;

create or replace function "get_test_question_by_schedule_test_id_and_question_id"(
    in "InScheduleTestId" uuid,
    in "InQuestionId" uuid
)
    returns table
            (
                "TestId"     uuid,
                "QuestionId" uuid,
                "MaxMark"    double precision
            )
    language 'plpgsql'
as
$$
begin
    return query select tq.test_id, tq.question_id, tq.max_mark
                 from test_questions tq
                          inner join tests t on tq.test_id = t.id
                          inner join schedule_tests st on t.id = st.test_id
                 where tq.question_id = "InQuestionId"
                   and st.id = "InScheduleTestId";
end;
$$;

create or replace function "get_test_question_by_schedule_test_id_and_question_ids"(
    in "InScheduleTestId" uuid,
    in "InQuestionIds" uuid[]
)
    returns table
            (
                "TestId"     uuid,
                "QuestionId" uuid,
                "MaxMark"    double precision
            )
    language 'plpgsql'
as
$$
begin
    return query select tq.test_id, tq.question_id, tq.max_mark
                 from test_questions tq
                          inner join tests t on tq.test_id = t.id
                          inner join schedule_tests st on t.id = st.test_id
                 where tq.question_id = any ("InQuestionIds")
                   and st.id = "InScheduleTestId";
end;
$$;

create or replace function "get_schedule_test_by_id"(
    in "InScheduleTestId" uuid
)
    returns table
            (
                "Id"                uuid,
                "StartAt"           timestamp with time zone,
                "FinishAt"          timestamp with time zone,
                "TestId"            uuid,
                "TestName"          varchar(5000),
                "CreatedAt"         timestamp with time zone,
                "UserScheduleTests" json
            )
    language 'plpgsql'
as
$$
begin
    return query select st.id,
                        st.start_at,
                        st.finish_at,
                        st.test_id,
                        t.name,
                        t.created_at,
                        array_to_json(array_agg(ust))
                 from schedule_tests st
                          inner join tests t on st.test_id = t.id
                          inner join (select iust.schedule_test_id                         "ScheduleTestId",
                                             iust.user_id                                  "UserId",
                                             iust.finished_at                              "FinishedAt",
                                             iust.checked_by_teacher                       "CheckedByTeacher",
                                             sum(iua.program_mark)                         "ProgramMark",
                                             sum(iua.teacher_mark)                         "TeacherMark",
                                             sum((select tq.max_mark
                                                  from test_questions tq
                                                  where tq.test_id in (select ist.test_id
                                                                       from schedule_tests ist
                                                                       where ist.id = iust.schedule_test_id)
                                                    and tq.question_id = iua.question_id)) "QuestionMaxMark"
                                      from user_schedule_tests iust
                                               left join user_answers iua
                                                         on iust.schedule_test_id = iua.schedule_test_id and
                                                            iust.user_id = iua.user_id
                                      group by iust.schedule_test_id, iust.user_id, iust.finished_at,
                                               iust.checked_by_teacher
                                      order by iust.checked_by_teacher desc,
                                               case iust.checked_by_teacher
                                                   when true then sum(iua.teacher_mark)
                                                   when false then sum(iua.program_mark)
                                                   end desc, iust.finished_at) ust on st.id = ust."ScheduleTestId"
                 where st.id = "InScheduleTestId"
                 group by st.id, st.start_at, st.finish_at, st.test_id, t.name, t.created_at;
end;
$$;

create or replace function "get_questions_by_schedule_test_id"(
    in "ScheduleTestId" uuid
)
    returns table
            (
                "Id"                uuid,
                "Body"              varchar(5000),
                "Complexity"        int,
                "TopicId"           uuid,
                "DatabaseId"        uuid,
                "CorrectAnswerId"   uuid,
                "CorrectAnswerBody" varchar(5000)
            )
    language 'plpgsql'
as
$$
begin
    return query select q.id, q.body, q.complexity, q.topic_id, q.database_id, ca.id, ca.body
                 from questions q
                          inner join public.correct_answers ca on q.id = ca.question_id
                 where q.id in (select "question_id"
                                from test_questions tq
                                         inner join schedule_tests st on tq.test_id = st.test_id
                                where st.id = "ScheduleTestId");
end;
$$;

create or replace function "get_user_answers_by_schedule_test_id_and_user_id"(
    in "InScheduleTestId" uuid,
    in "InUserId" uuid
)
    returns table
            (
                "ScheduleTestId"       uuid,
                "UserId"               uuid,
                "QuestionId"           uuid,
                "Body"                 varchar(5000),
                "ProgramMark"          double precision,
                "TeacherMark"          double precision,
                "AnsweredAt"           timestamp with time zone,
                "ScheduleTestStartAt"  timestamp with time zone,
                "ScheduleTestFinishAt" timestamp with time zone,
                "QuestionBody"         varchar(5000),
                "QuestionComplexity"   int,
                "QuestionTopicId"      uuid,
                "QuestionDatabaseId"   uuid,
                "CorrectAnswerId"      uuid,
                "CorrectAnswerBody"    varchar(5000),
                "TestId"               uuid,
                "TestName"             varchar(5000),
                "TestCreatedAt"        timestamp with time zone
            )
    language 'plpgsql'
as
$$
begin
    return query select "InScheduleTestId",
                        "InUserId",
                        q.id,
                        coalesce(ua.body, ''),
                        coalesce(ua.program_mark, 0),
                        coalesce(ua.teacher_mark, 0),
                        coalesce(ua.answered_at, st.finish_at),
                        st.start_at,
                        st.finish_at,
                        q.body,
                        q.complexity,
                        q.topic_id,
                        q.database_id,
                        ca.id,
                        ca.body,
                        t.id,
                        t.name,
                        t.created_at
                 from schedule_tests st
                          inner join tests t on st.test_id = t.id
                          inner join test_questions tq on st.test_id = tq.test_id
                          inner join questions q on tq.question_id = q.id
                          inner join correct_answers ca on q.id = ca.question_id
                          left join user_answers ua on tq.question_id = ua.question_id and st.id = ua.schedule_test_id
                 where st.id = "InScheduleTestId"
                   and (ua.user_id = "InUserId" or ua.user_id is null);
end;
$$;

create or replace procedure "update_user_schedule_test"(
    in "ScheduleTestId" uuid,
    in "UserId" uuid,
    in "FinishedAt" timestamp with time zone,
    in "CheckedByTeacher" boolean
)
    language 'sql'
as
$$
update "user_schedule_tests"
set "finished_at"        = "FinishedAt",
    "checked_by_teacher" = "CheckedByTeacher"
where "user_id" = "UserId"
  and "schedule_test_id" = "ScheduleTestId"
$$;

create or replace procedure "update_user_answer"(
    in "UserId" uuid,
    in "ScheduleTestId" uuid,
    in "QuestionId" uuid,
    in "TeacherMark" double precision
)
    language 'sql'
as
$$
update public."user_answers"
set teacher_mark = "TeacherMark"
where user_id = "UserId"
  and schedule_test_id = "ScheduleTestId"
  and question_id = "QuestionId";
$$;
