﻿using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.InputParameters.TestQuestion;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class TestQuestionRepository : Repository, ITestQuestionRepository
{
    public TestQuestionRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    public async Task AddAsync(TestQuestion testQuestion, CancellationToken cancellationToken = default) => 
        await this.Context.CallAsync("call insert_test_question (@TestId, @QuestionId, @MaxMark)", 
            ModelToAddParameters(testQuestion), cancellationToken);
    
    private static AddTestQuestionInputParameters ModelToAddParameters(TestQuestion model) => new(model.TestId, model.QuestionId, model.MaxMark);

    public async Task AddRangeAsync(Guid testId, IEnumerable<TestQuestion> testQuestions, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync("call insert_test_questions (@TestId, @QuestionMaxMarks)", 
            ModelToAddRangeParameters(testId, testQuestions), cancellationToken);
    
    private static AddRangeTestQuestionsInputParameters ModelToAddRangeParameters(Guid testId, IEnumerable<TestQuestion> models) => 
        new(testId, models.Select(m => new TestQuestionParameter(m.QuestionId, m.MaxMark)).ToArray());
    
    public async Task DeleteAsync(Guid testId, IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync("call delete_test_questions (@TestId, @QuestionIds)", 
            new DeleteTestQuestionsInputParameters(testId, questionIds.ToArray()), cancellationToken);

    public async Task DeleteByTestAsync(Guid testId, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync("call delete_test_questions_by_test (@TestId)", 
            new DeleteTestQuestionsByTestInputParameters(testId), cancellationToken);
    
    public async Task UpdateAsync(TestQuestion testQuestion, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync("call update_test_question (@TestId, @QuestionId, @MaxMark)", 
            ModelToUpdateParameters(testQuestion), cancellationToken);

    private static UpdateTestQuestionInputParameters ModelToUpdateParameters(TestQuestion model) => new(model.TestId, model.QuestionId, model.MaxMark);
    
    public async Task<TestQuestion?> GetByScheduleTestAndQuestionAsync(Guid scheduleTestId, Guid questionId, CancellationToken cancellationToken = default) =>
        await this.Context.FirstOrDefaultAsync<TestQuestion, GetTestQuestionByScheduleTestAndQuestionInputParameters, GetTestQuestionDto>(
                "select * from get_test_question_by_schedule_test_id_and_question_id (@ScheduleTestId, @QuestionId)",
                new GetTestQuestionByScheduleTestAndQuestionInputParameters(scheduleTestId, questionId),
                DatabaseDtoToModel, cancellationToken);

    public async Task<IReadOnlyCollection<TestQuestion>> GetByScheduleTestAndQuestionIdsAsync(Guid scheduleTestId, IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<TestQuestion, GetTestQuestionByScheduleTestAndQuestionsInputParameters, GetTestQuestionDto>(
                "select * from get_test_question_by_schedule_test_id_and_question_ids(@ScheduleTestId, @QuestionIds)",
                new GetTestQuestionByScheduleTestAndQuestionsInputParameters(scheduleTestId, questionIds.ToArray()),
                DatabaseDtoToModel, cancellationToken)
        ?? Array.Empty<TestQuestion>();
    

    private static TestQuestion DatabaseDtoToModel(GetTestQuestionDto dto) => new(dto.TestId, dto.QuestionId) { MaxMark = dto.MaxMark };
}