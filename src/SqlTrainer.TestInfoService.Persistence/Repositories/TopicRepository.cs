﻿using Application.Repositories;
using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.Helpers;
using SqlTrainer.TestInfoService.Persistence.InputParameters.Topic;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class TopicRepository : 
    CrudRepository<Topic, AddTopicInputParameters, UpdateTopicInputParameters, GetTopicDto>, 
    ITopicRepository
{
    protected override string AddQuery => "call insert_topic (@Id, @Name)";
    protected override string UpdateQuery => "call update_topic (@Id, @Name)";
    protected override string DeleteQuery => "call delete_topic (@Id)";
    protected override string GetAllQuery => "select * from get_all_topics (@SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)";
    protected override string GetByIdQuery => "select * from get_topic_by_id (@Id)";
    
    public TopicRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    protected override AddTopicInputParameters ModelToAddParameters(Topic model) => new(model.Id, model.Name);

    protected override UpdateTopicInputParameters ModelToUpdateParameters(Topic model) => new(model.Id, model.Name);

    protected override Topic DatabaseDtoToModel(GetTopicDto dto) => new(dto.Id) { Name = dto.Name };
    
    protected override string OverrideOrderBy(string orderBy) => orderBy;

    protected override string OverrideOrderByDirection(SortDirection orderByDirection) => orderByDirection.ToSqlString();
}