﻿using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.InputParameters.CorrectAnswer;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class CorrectAnswerRepository : Repository, ICorrectAnswerRepository
{
    public CorrectAnswerRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    public async Task AddAsync(CorrectAnswer correctAnswer, CancellationToken cancellationToken = default) =>
        await Context.CallAsync("call insert_correct_answer (@Id, @Body, @QuestionId)",
            new AddCorrectAnswerInputParameters(correctAnswer.Id, correctAnswer.Body, correctAnswer.QuestionId),
            cancellationToken);

    public async Task UpdateAsync(CorrectAnswer correctAnswer, CancellationToken cancellationToken = default) =>
        await Context.CallAsync("call update_correct_answer (@Id, @Body)",
            new UpdateCorrectAnswerInputParameters(correctAnswer.Id, correctAnswer.Body),
            cancellationToken);
    
    public async Task<CorrectAnswer?> GetByQuestionAsync(Guid questionId, CancellationToken cancellationToken = default) =>
        await Context.FirstOrDefaultAsync<CorrectAnswer, GetCorrectAnswerByQuestionInputParameters, GetCorrectAnswerDto>(
            "select * from get_correct_answer_by_question_id(@InQuestionId)",
            new GetCorrectAnswerByQuestionInputParameters(questionId), DatabaseDtoToModel,
            cancellationToken);

    public async Task<IReadOnlyCollection<CorrectAnswer>> GetByQuestionsAsync(IEnumerable<Guid> questionIds, CancellationToken cancellationToken = default) =>
        await Context.SelectAsync<CorrectAnswer, GetCorrectAnswerByQuestionsInputParameters, GetCorrectAnswerDto>(
            "select * from get_correct_answer_by_question_ids(@InQuestionIds)",
            new GetCorrectAnswerByQuestionsInputParameters(questionIds.ToArray()), DatabaseDtoToModel,
            cancellationToken)
        ?? Array.Empty<CorrectAnswer>();

    private static CorrectAnswer DatabaseDtoToModel(GetCorrectAnswerDto dto)
    {
        var correctAnswer = new CorrectAnswer(dto.Id)
        {
            Body = dto.Body, 
            QuestionId = dto.QuestionId
        };

        var question = new Question(dto.QuestionId)
        {
            Body = dto.QuestionBody,
            Complexity = dto.QuestionComplexity,
            TopicId = dto.TopicId,
            DatabaseId = dto.DatabaseId
        };

        return correctAnswer.WithQuestion(question);
    }
}