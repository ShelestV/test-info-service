﻿using System.Text.Json;
using Application.Repositories;
using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.Helpers;
using SqlTrainer.TestInfoService.Persistence.InputParameters.ScheduleTest;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class ScheduleTestRepository :
    CrudRepository<ScheduleTest, AddScheduleTestInputParameters, UpdateScheduleTestInputParameters, GetScheduleTestDto>,
    IScheduleTestRepository
{
    protected override string AddQuery => "call insert_schedule_test (@Id, @TestId, @UserIds, @StartAt, @FinishAt)";
    protected override string UpdateQuery => "call update_schedule_test (@Id, @TestId, @UserIds, @StartAt, @FinishAt)";
    protected override string DeleteQuery => "call delete_schedule_test (@Id)";
    protected override string GetAllQuery => "select * from get_all_schedule_tests (@SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)";
    protected override string GetByIdQuery => "select * from get_schedule_test_by_id (@Id)";
    
    public ScheduleTestRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    public async Task<IReadOnlyCollection<ScheduleTest>> GetByUserAsync(Guid userId, string searchTerm = "", string orderBy = "", SortDirection orderByDirection = SortDirection.Ascending, int page = 1, int pageSize = 1, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<ScheduleTest, GetScheduleTestsByUserIdInputParameters, GetScheduleTestDto>(
            "select * from get_schedule_tests_by_user_id (@UserId, @SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)",
            new(userId, searchTerm, OverrideOrderBy(orderBy), orderByDirection.ToSqlString(), pageSize, (page - 1) * pageSize), DatabaseDtoToModel, cancellationToken)
        ?? Array.Empty<ScheduleTest>();

    protected override AddScheduleTestInputParameters ModelToAddParameters(ScheduleTest model) =>
        new(model.Id, 
            model.TestId, 
            model.UserScheduleTests!.Select(ust => ust.UserId).ToArray(), 
            model.StartAt, 
            model.FinishAt);

    protected override UpdateScheduleTestInputParameters ModelToUpdateParameters(ScheduleTest model) =>
        new(model.Id, 
            model.TestId, 
            model.UserScheduleTests!.Select(ust => ust.UserId).ToArray(), 
            model.StartAt, 
            model.FinishAt);

    protected override ScheduleTest DatabaseDtoToModel(GetScheduleTestDto dto)
    {
        var scheduleTest = new ScheduleTest(dto.Id)
        {
            TestId = dto.TestId,
            StartAt = dto.StartAt.ToUniversalTime(),
            FinishAt = dto.FinishAt.ToUniversalTime()
        };

        var test = new Test(dto.TestId)
        {
            Name = dto.TestName,
            CreatedAt = dto.TestCreatedAt.ToUniversalTime()
        };

        try
        {
            var userScheduleTests = JsonSerializer.Deserialize<UserScheduleTest[]>(dto.UserScheduleTests)
                                    ?? Array.Empty<UserScheduleTest>();
            scheduleTest.WithUserScheduleTests(userScheduleTests.Select(ust => 
                new UserScheduleTest(ust.ScheduleTestId, ust.UserId)
                {
                    CheckedByTeacher = ust.CheckedByTeacher,
                    FinishedAt = ust.FinishedAt?.ToUniversalTime() ?? null,
                    ProgramMark = ust.ProgramMark,
                    QuestionMaxMark = ust.QuestionMaxMark,
                    TeacherMark = ust.TeacherMark
                }).ToArray());
        }
        catch (ArgumentException)
        {
            scheduleTest.WithUserScheduleTests(Array.Empty<UserScheduleTest>());
        }

        return scheduleTest.WithTest(test);
    }

    protected override string OverrideOrderBy(string orderBy) => orderBy.ToLower() switch
    {
        "testname" => "test_name",
        "startat" => "start_at",
        "finishat" => "finish_at",
        _ => orderBy
    };

    protected override string OverrideOrderByDirection(SortDirection orderByDirection) => orderByDirection.ToSqlString();
}