﻿using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.InputParameters.UserAnswers;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class UserAnswerRepository : Repository, IUserAnswerRepository
{
    public UserAnswerRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    public async Task AddAsync(UserAnswer userAnswer, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync<AddUserAnswerInputParameters>(
            "call insert_user_answer (@UserId, @ScheduleTestId, @QuestionId, @Body, @ProgramMark, @AnsweredAt)",
            new(userAnswer.UserId, userAnswer.ScheduleTestId, userAnswer.QuestionId, userAnswer.Body, userAnswer.ProgramMark, userAnswer.AnsweredAt),
            cancellationToken);

    public async Task UpdateAsync(UserAnswer userAnswer, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync<UpdateUserAnswerInputParameters>(
            "call update_user_answer (@UserId, @ScheduleTestId, @QuestionId, @TeacherMark)",
            new(userAnswer.UserId, userAnswer.ScheduleTestId, userAnswer.QuestionId, userAnswer.Body, userAnswer.TeacherMark!.Value, userAnswer.AnsweredAt),
            cancellationToken);

    public async Task<IReadOnlyCollection<UserAnswer>> GetByScheduleTestAndUserAsync(Guid scheduleTestId, Guid userId, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<UserAnswer, GetByScheduleTestAndUserIdInputParameters, GetUserAnswerDto>(
            "select * from get_user_answers_by_schedule_test_id_and_user_id (@ScheduleTestId, @UserId)",
            new GetByScheduleTestAndUserIdInputParameters(scheduleTestId, userId), DatabaseDtoToModel, cancellationToken)
        ?? Array.Empty<UserAnswer>();

    private static UserAnswer DatabaseDtoToModel(GetUserAnswerDto dto)
    {
        var userAnswer = new UserAnswer(dto.ScheduleTestId, dto.QuestionId, dto.UserId)
        {
            Body = dto.Body,
            ProgramMark = dto.ProgramMark,
            TeacherMark = dto.TeacherMark,
            QuestionMaxMark = dto.QuestionMaxMark,
            AnsweredAt = dto.AnsweredAt.ToUniversalTime()
        };

        var scheduleTest = new ScheduleTest(dto.ScheduleTestId)
        {
            TestId = dto.TestId,
            StartAt = dto.ScheduleTestStartAt,
            FinishAt = dto.ScheduleTestFinishAt
        };

        var test = new Test(dto.TestId)
        {
            Name = dto.TestName,
            CreatedAt = dto.CreatedAt
        };
        
        var question = new Question(dto.QuestionId)
        {
            Body = dto.QuestionBody,
            Complexity = dto.QuestionComplexity,
            DatabaseId = dto.QuestionDatabaseId,
            TopicId = dto.QuestionTopicId
        };
        
        var correctAnswer = new CorrectAnswer(dto.CorrectAnswerId)
        {
            Body = dto.CorrectAnswerBody,
            QuestionId = dto.QuestionId
        };

        scheduleTest.WithTest(test);
        question.WithCorrectAnswer(correctAnswer);
        
        return userAnswer.WithQuestion(question).WithScheduleTest(scheduleTest);
    }
}