﻿using Application.Repositories;
using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.Helpers;
using SqlTrainer.TestInfoService.Persistence.InputParameters.Question;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class QuestionRepository :
    CrudRepository<Question, AddQuestionInputParameters, UpdateQuestionInputParameters, GetQuestionDto>,
    IQuestionRepository
{
    protected override string AddQuery => "call insert_question (@Id, @Body, @Complexity, @TopicId, @DatabaseId)";
    protected override string UpdateQuery => "call update_question (@Id, @Body, @Complexity, @TopicId, @DatabaseId)";
    protected override string DeleteQuery => "call delete_question (@Id)";
    protected override string GetAllQuery => "select * from get_all_questions (@SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)";
    protected override string GetByIdQuery => "select * from get_question_by_id (@Id)";
    
    public QuestionRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }
    
    public async Task<IReadOnlyCollection<Question>> GetRangeAsync(IEnumerable<Guid> ids, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<Question, GetQuestionsByIdsInputParameters, GetQuestionDto>("select * from get_questions_by_ids (@QuestionIds)", 
            new GetQuestionsByIdsInputParameters(ids.ToArray()), DatabaseDtoToModel, cancellationToken) 
        ?? Array.Empty<Question>();

    public async Task<IReadOnlyCollection<Question>> GetByTestAsync(Guid testId, string searchTerm = "", string orderBy = "", SortDirection orderByDirection = SortDirection.Ascending, int page = 1, int pageSize = 1, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<Question, GetQuestionsByTestInputParameters, GetQuestionDto>(
            "select * from get_questions_by_test_id (@TestId, @SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)",
            new(testId, searchTerm, OverrideOrderBy(orderBy), orderByDirection.ToSqlString(), pageSize, (page - 1) * pageSize), DatabaseDtoToModel, cancellationToken)
        ?? Array.Empty<Question>();

    public async Task<IReadOnlyCollection<Question>> GetByScheduleTestAsync(Guid scheduleTestId, CancellationToken cancellationToken = default) =>
        await this.Context.SelectAsync<Question, GetQuestionsByScheduleTestInputParameters, GetQuestionDto>(
            "select * from get_questions_by_schedule_test_id (@ScheduleTestId)",
            new(scheduleTestId), DatabaseDtoToModel, cancellationToken)
        ?? Array.Empty<Question>();

    protected override AddQuestionInputParameters ModelToAddParameters(Question model) =>
        new(model.Id, model.Body, model.Complexity, model.TopicId, model.DatabaseId);

    protected override UpdateQuestionInputParameters ModelToUpdateParameters(Question model) =>
        new(model.Id, model.Body, model.Complexity, model.TopicId, model.DatabaseId);

    protected override Question DatabaseDtoToModel(GetQuestionDto dto)
    {
        var question = new Question(dto.Id)
        {
            Body = dto.Body,
            Complexity = dto.Complexity,
            DatabaseId = dto.DatabaseId,
            TopicId = dto.TopicId
        };

        var correctAnswer = new CorrectAnswer(dto.CorrectAnswerId)
        {
            Body = dto.CorrectAnswerBody,
            QuestionId = dto.Id
        };

        return question.WithCorrectAnswer(correctAnswer);
    }

    protected override string OverrideOrderBy(string orderBy) => orderBy;

    protected override string OverrideOrderByDirection(SortDirection orderByDirection) => orderByDirection.ToSqlString();
}