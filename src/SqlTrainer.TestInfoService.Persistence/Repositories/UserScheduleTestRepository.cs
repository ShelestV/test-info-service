﻿using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.InputParameters.UserScheduleTest;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class UserScheduleTestRepository : Repository, IUserScheduleTestRepository
{
    public UserScheduleTestRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    public async Task UpdateAsync(UserScheduleTest userScheduleTest, CancellationToken cancellationToken = default) =>
        await this.Context.CallAsync<UpdateUserScheduleTestInputParameters>(
            "call update_user_schedule_test (@ScheduleTestId, @UserId, @FinishedAt, @CheckedByTeacher)",
            new(userScheduleTest.ScheduleTestId, userScheduleTest.UserId, userScheduleTest.FinishedAt, userScheduleTest.CheckedByTeacher),
            cancellationToken);
}