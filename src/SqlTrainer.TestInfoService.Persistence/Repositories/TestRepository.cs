﻿using System.Text.Json;
using Application.Repositories;
using Persistence.Contexts;
using Persistence.Repositories;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.Persistence.Dtos;
using SqlTrainer.TestInfoService.Persistence.Helpers;
using SqlTrainer.TestInfoService.Persistence.InputParameters.Test;

namespace SqlTrainer.TestInfoService.Persistence.Repositories;

public sealed class TestRepository : 
    CrudRepository<Test, AddTestInputParameters, UpdateTestInputParameters, GetTestDto>,
    ITestRepository
{
    protected override string AddQuery => "call insert_test (@Id, @Name, @CreatedAt);";
    protected override string UpdateQuery => "call update_test (@Id, @Name)";
    protected override string DeleteQuery => "call delete_test (@Id)";
    protected override string GetAllQuery => "select * from get_all_tests (@SearchTerm, @Limit, @Offset, @OrderBy, @OrderByDirection)";
    protected override string GetByIdQuery => "select * from get_test_by_id (@Id)";
    
    public TestRepository(DatabaseContext databaseContext) : base(databaseContext)
    {
    }

    protected override AddTestInputParameters ModelToAddParameters(Test model) => new(model.Id, model.Name, model.CreatedAt);

    protected override UpdateTestInputParameters ModelToUpdateParameters(Test model) => new(model.Id, model.Name);

    protected override Test DatabaseDtoToModel(GetTestDto dto)
    {
        var test = new Test(dto.Id)
        {
            Name = dto.Name,
            CreatedAt = dto.CreatedAt.ToUniversalTime()
        };

        try
        {
            var testQuestions = JsonSerializer
                .Deserialize<QuestionDto[]>(dto.Questions)?
                .Select(questionDto =>
                {
                    var testQuestion = new TestQuestion(questionDto.TestId, questionDto.Id)
                    {
                        MaxMark = questionDto.MaxMark
                    };
                    
                    var question = new Question(questionDto.Id)
                    {
                        Body = questionDto.Body,
                        Complexity = questionDto.Complexity,
                        DatabaseId = questionDto.DatabaseId,
                        TopicId = questionDto.TopicId
                    };

                    var correctAnswer = new CorrectAnswer(questionDto.CorrectAnswerId)
                    {
                        Body = questionDto.CorrectAnswerBody,
                        QuestionId = questionDto.Id
                    };
                    
                    question.WithCorrectAnswer(correctAnswer);
                    return testQuestion.WithQuestion(question);
                }) ?? Array.Empty<TestQuestion>();

            test.WithTestQuestions(testQuestions.ToArray());
        }
        catch (ArgumentNullException)
        {
            test.WithTestQuestions(Array.Empty<TestQuestion>());
        }
        
        return test;
    }

    protected override string OverrideOrderBy(string orderBy)
    {
        if (string.IsNullOrWhiteSpace(orderBy))
            return orderBy;

        return orderBy.ToLower() switch
        {
            "createdat" => "created_at",
            _ => orderBy
        };
    }

    protected override string OverrideOrderByDirection(SortDirection orderByDirection) => orderByDirection.ToSqlString();

    private sealed class QuestionDto
    {
        public required Guid Id { get; set; }
        public required Guid TestId { get; set; }
        public required double MaxMark { get; set; }
        public required string Body { get; set; }
        public required int Complexity { get; set; }
        public required Guid CorrectAnswerId { get; set; }
        public required string CorrectAnswerBody { get; set; }
        public required Guid TopicId { get; set; }
        public required Guid DatabaseId { get; set; }
    }
}