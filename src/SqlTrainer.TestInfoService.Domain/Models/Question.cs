﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class Question : IdModel, IEquatable<Question>
{
    public required string Body { get; init; }
    public required int Complexity { get; init; }
    public required Guid TopicId { get; init; }
    public required Guid DatabaseId { get; init; }
    public CorrectAnswer? CorrectAnswer { get; private set; }
    
    public Question(Guid id = default) : base(id) {}
    
    public Question WithCorrectAnswer(CorrectAnswer correctAnswer)
    {
        CorrectAnswer = correctAnswer;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is Question question && this.Equals(question);
    public bool Equals(Question? other) => other is not null && other.Id == this.Id;
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(Question? left, Question? right) => left is not null && left.Equals(right);
    public static bool operator !=(Question? left, Question? right) => !(left == right);
    
    public override string ToString() => Body;
}