﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class UserScheduleTest : Model, IEquatable<UserScheduleTest>
{
    public Guid ScheduleTestId { get; }
    public Guid UserId { get; }
    public DateTimeOffset? FinishedAt { get; init; }
    public bool? CheckedByTeacher { get; init; }
    public double? ProgramMark { get; init; }
    public double? TeacherMark { get; init; }
    public double? QuestionMaxMark { get; init; }
    public ScheduleTest? ScheduleTest { get; private set; }

    public UserScheduleTest(Guid scheduleTestId, Guid userId)
    {
        ScheduleTestId = scheduleTestId;
        UserId = userId;
    }
    
    public UserScheduleTest WithScheduleTest(ScheduleTest scheduleTest)
    {
        ScheduleTest = scheduleTest;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is UserScheduleTest userScheduleTest && this.Equals(userScheduleTest);
    public bool Equals(UserScheduleTest? other) => other is not null && other.ScheduleTestId == this.ScheduleTestId && other.UserId == this.UserId;
    public override int GetHashCode() => HashCode.Combine(ScheduleTestId, UserId);
    
    public static bool operator ==(UserScheduleTest? left, UserScheduleTest? right) => left is not null && left.Equals(right);
    public static bool operator !=(UserScheduleTest? left, UserScheduleTest? right) => !(left == right);
    
    public override string ToString() => $"User: {UserId}, Schedule test: {ScheduleTestId}";
}