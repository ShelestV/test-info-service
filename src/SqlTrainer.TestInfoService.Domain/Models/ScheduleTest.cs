﻿using Domain.Models;

namespace SqlTrainer.TestInfoService.Domain.Models;

public sealed class ScheduleTest : IdModel, IEquatable<ScheduleTest>
{
    public required Guid TestId { get; init; }
    public required DateTimeOffset StartAt { get; init; }
    public required DateTimeOffset FinishAt { get; init; }
    public Test? Test { get; private set; }
    public IReadOnlyCollection<UserScheduleTest>? UserScheduleTests { get; private set; }
    
    public ScheduleTest(Guid id = default) : base(id) {}
    
    public ScheduleTest WithTest(Test test)
    {
        Test = test;
        return this;
    }
    
    public ScheduleTest WithUserScheduleTests(IReadOnlyCollection<UserScheduleTest> userScheduleTests)
    {
        UserScheduleTests = userScheduleTests;
        return this;
    }
    
    public override bool Equals(object? obj) => obj is ScheduleTest scheduleTest && this.Equals(scheduleTest);
    public bool Equals(ScheduleTest? other) => other is not null && (other.Id == this.Id || other.TestId == this.TestId && other.StartAt == this.StartAt && other.FinishAt == this.FinishAt);
    public override int GetHashCode() => base.GetHashCode();
    
    public static bool operator ==(ScheduleTest? left, ScheduleTest? right) => left is not null && left.Equals(right);
    public static bool operator !=(ScheduleTest? left, ScheduleTest? right) => !(left == right);
    
    public override string ToString() => $"{Test?.Name ?? TestId.ToString()} ({StartAt} - {FinishAt})";
}