﻿using System.Text;
using SqlTrainer.TestInfoService.Application.Services;

namespace SqlTrainer.TestInfoService.DatabasesInfrastructure.Services;

public sealed class ScriptFormattingService : IScriptFormattingService
{
    public string? Clear(string? query)
    {
        if (string.IsNullOrWhiteSpace(query))
            return query;
        
        query = query.Replace('\t', ' ').Replace('\r', ' ');
        
        // Remove multiline comments
        while (query.Contains("/*"))
        {
            var leftIndex = query.IndexOf("/*", StringComparison.Ordinal);
            var rightIndex = query.IndexOf("*/", StringComparison.Ordinal);
            
            if (leftIndex == -1 || rightIndex == -1)
                return null; // Could not be executed
            
            query = query.Remove(leftIndex, rightIndex - leftIndex + 2);
        }
        
        // Remove single line comments
        var lines = query.Split("\n", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
        var queryBuilder = new StringBuilder();
        foreach (var line in lines)
        {
            var index = line.IndexOf("--", StringComparison.Ordinal);
            if (index == -1)
            {
                queryBuilder.Append(line).Append(' ');
                continue;
            }

            queryBuilder.Append(line.Remove(index)).Append(' ');
        }
        
        var updatedQuery = queryBuilder.ToString().Trim();
        
        // Remove multiple spaces
        while (updatedQuery.Contains("  "))
            updatedQuery = updatedQuery.Replace("  ", " ");
        
        return updatedQuery;
    }
}