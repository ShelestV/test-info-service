using PostgresPersistence;
using PostgresPersistence.Migrations;
using GrpcHelper.DependencyInjection.Extensions;
using PostgresPersistence.DbUp;
using SqlTrainer.TestInfoService.Application.Extensions;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Extensions;
using SqlTrainer.TestInfoService.DatabasesInfrastructure.Services;
using SqlTrainer.TestInfoService.Services;
using SqlTrainer.TestInfoService.Extensions;
using SqlTrainer.TestInfoService.Persistence.DbUp;

var builder = WebApplication.CreateBuilder(args);

if (args.Contains("-local"))
{
    var httpPort = builder.Configuration.GetValue<int>("HTTP_PORT");
    var httpsPort = builder.Configuration.GetValue<int>("HTTPS_PORT");
    builder.ConfigureKestrelForHttps((httpPort, httpsPort));
}
else
{
    builder.ConfigureKestrelForHttps();
}

var logger = LoggerFactory.Create(loggingBuilder => loggingBuilder
        .SetMinimumLevel(LogLevel.Error)
        .AddConsole())
    .CreateLogger<Program>();

var server = Environment.GetEnvironmentVariable("POSTGRES_SERVER");
var port = Environment.GetEnvironmentVariable("POSTGRES_PORT");
var user = Environment.GetEnvironmentVariable("POSTGRES_USER");
var password = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
var database = Environment.GetEnvironmentVariable("POSTGRES_TESTS_DB");

var connectionString = PostgresConnectionStringBuilder.BuildConnectionString(server, port, user, password, database, 
    error => logger.LogError("{}", error));

builder.Services.AddDatabaseServices(builder.Configuration);

builder.Services
    .InjectRepositories(connectionString)
    .AddTransient<IScriptFormattingService, ScriptFormattingService>()
    .AddTransient<IScriptResultsCompareService, ScriptResultCompareService>()
    .AddTransient<IDbUpService, TestInfoDbUpService>()
    .AddApplication();

builder.Services.AddGrpc();
builder.Services.AddGrpcReflection();

var app = builder.Build();

app.MapGrpcService<TopicService>();
app.MapGrpcService<QuestionService>();
app.MapGrpcService<TestService>();
app.MapGrpcService<ScheduleTestService>();
app.MapGrpcService<UserScheduleTestService>();
app.MapGrpcService<UserAnswerService>();

if (app.Environment.IsDevelopment())
    app.MapGrpcReflectionService();

app.MigrateDatabase<Program>().Run();