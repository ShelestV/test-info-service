﻿using PostgresPersistence.DependencyInjection;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Persistence.Repositories;

namespace SqlTrainer.TestInfoService.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection InjectRepositories(this IServiceCollection services, string connectionString)
    {
        services.InjectPostgresContext(connectionString);

        services.AddTransient<ICorrectAnswerRepository, CorrectAnswerRepository>();
        services.AddTransient<ITopicRepository, TopicRepository>();
        services.AddTransient<IQuestionRepository, QuestionRepository>();
        services.AddTransient<ITestRepository, TestRepository>();
        services.AddTransient<ITestQuestionRepository, TestQuestionRepository>();
        services.AddTransient<IScheduleTestRepository, ScheduleTestRepository>();
        services.AddTransient<IUserScheduleTestRepository, UserScheduleTestRepository>();
        services.AddTransient<IUserAnswerRepository, UserAnswerRepository>();
        
        return services;
    }
}