﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;
using SqlTrainer.TestInfoService.Application.UserAnswers.GetByScheduleTestAndUser;
using SqlTrainer.TestInfoService.Application.UserAnswers.Update;

namespace SqlTrainer.TestInfoService.Services;

using MediatorQuestionBody = SqlTrainer.TestInfoService.Application.UserAnswers.AddRange.QuestionBody;
using MediatorAddUserAnswersRequest = SqlTrainer.TestInfoService.Application.UserAnswers.AddRange.AddUserAnswersRequest;
using MediatorUpdateUserAnswerRequest = SqlTrainer.TestInfoService.Application.UserAnswers.Update.UpdateUserAnswerRequest;
using MediatorGetByScheduleTestAndUserAnswersRequest = GetByScheduleTestAndUserAnswersRequest;

public sealed class UserAnswerService : TestInfoService.UserAnswerService.UserAnswerServiceBase
{
    private readonly IAddUserAnswersRequestHandler addUserAnswersRequestHandler;
    private readonly IUpdateUserAnswerRequestHandler updateUserAnswerRequestHandler;
    private readonly IGetByScheduleTestAndUserAnswersRequestHandler getByScheduleTestAndUserAnswersRequestHandler;
    private readonly ILogger<UserAnswerService> logger;
    
    public UserAnswerService(
        IAddUserAnswersRequestHandler addUserAnswersRequestHandler,
        IUpdateUserAnswerRequestHandler updateUserAnswerRequestHandler,
        IGetByScheduleTestAndUserAnswersRequestHandler getByScheduleTestAndUserAnswersRequestHandler,
        ILogger<UserAnswerService> logger)
    {
        this.addUserAnswersRequestHandler = addUserAnswersRequestHandler;
        this.updateUserAnswerRequestHandler = updateUserAnswerRequestHandler;
        this.getByScheduleTestAndUserAnswersRequestHandler = getByScheduleTestAndUserAnswersRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> AddRange(AddUserAnswersRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started adding of user answers with schedule test id {ScheduleTestId} and user id {UserId}", request.ScheduleTestId, request.UserId);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.ScheduleTestId, out var scheduleTestId, "schedule test id")
                .TryParseGuid(request.UserId, out var userId, "user id");

            var questionBodies = new List<MediatorQuestionBody>();
            foreach (var questionBody in request.QuestionBodies)
            {
                argumentsConverter.TryParseGuid(questionBody.QuestionId, out var questionId, "question id");
                questionBodies.Add(new MediatorQuestionBody(questionId, questionBody.Body));
            }

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("User answers with schedule test id {ScheduleTestId} and user id {UserId} have not been added, because {Errors}", 
                    request.ScheduleTestId, request.UserId, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            var addUserAnswersRequest = new MediatorAddUserAnswersRequest(scheduleTestId, userId, request.AnsweredAt.ToDateTimeOffset(), questionBodies);
            var result = await addUserAnswersRequestHandler.HandleAsync(addUserAnswersRequest, context.CancellationToken);
            logger.LogInformation("Finished adding of user answers with schedule test id {ScheduleTestId} and user id {UserId}", request.ScheduleTestId, request.UserId);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answers have not been added");
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Update(UpdateUserAnswerRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of user answer with schedule test id {ScheduleTestId}, user id {UserId} and question id {QuestionId}", 
                request.ScheduleTestId, request.UserId, request.QuestionId);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.ScheduleTestId, out var scheduleTestId, "schedule test id")
                .TryParseGuid(request.UserId, out var userId, "user id")
                .TryParseGuid(request.QuestionId, out var questionId, "question id");

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("User answer with schedule test id {ScheduleTestId}, user id {UserId} and question id {QuestionId} has not been updated, because {Errors}", 
                    request.ScheduleTestId, request.UserId, request.QuestionId, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            var updateRequest = new MediatorUpdateUserAnswerRequest(scheduleTestId, userId, questionId, request.TeacherMark);
            var result = await updateUserAnswerRequestHandler.HandleAsync(updateRequest, context.CancellationToken);
            logger.LogInformation("Finished updating of user answer with schedule test id {ScheduleTestId}, user id {UserId} and question id {QuestionId}", request.ScheduleTestId, request.UserId, request.QuestionId);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answer has not been updated");
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetUserAnswersResponse> GetByScheduleTestAndUser(GetUserAnswersByScheduleTestAndUserRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of user answers by schedule test with id {ScheduleTestId} and user with id {UserId}", request.ScheduleTestId, request.UserId);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.ScheduleTestId, out var scheduleTestId, "schedule test id")
                .TryParseGuid(request.UserId, out var userId, "user id");

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("User answers with schedule test id {ScheduleTestId} and user id {UserId} have not been retrieved, because {Errors}", 
                    request.ScheduleTestId, request.UserId, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<GetUserAnswersResponse>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            var getByScheduleTestAndUserRequest = new MediatorGetByScheduleTestAndUserAnswersRequest(scheduleTestId, userId);
            var result = await getByScheduleTestAndUserAnswersRequestHandler.HandleAsync(getByScheduleTestAndUserRequest, context.CancellationToken);
            logger.LogInformation("User answers have been retrieved by schedule test with id {ScheduleTestId} and user with id {UserId}", request.ScheduleTestId, request.UserId);
            return new GetUserAnswersResponse
            {
                IsSuccess = true,
                UserAnswers = { result.Value.Select(ua => new UserAnswer
                {
                    ScheduleTestId = ua.ScheduleTestId.ToString(),
                    QuestionId = ua.QuestionId.ToString(),
                    UserId = ua.UserId.ToString(),
                    Body = ua.Body,
                    ProgramMark = ua.ProgramMark,
                    TeacherMark = ua.TeacherMark,
                    QuestionMaxMark = ua.QuestionMaxMark,
                    AnsweredAt = Timestamp.FromDateTimeOffset(ua.AnsweredAt),
                    ScheduleTest = ua.ScheduleTest is not null ? new ScheduleTest
                    {
                        Id = ua.ScheduleTestId.ToString(),
                        TestId = ua.ScheduleTest.TestId.ToString(),
                        StartAt = Timestamp.FromDateTimeOffset(ua.ScheduleTest.StartAt),
                        FinishAt = Timestamp.FromDateTimeOffset(ua.ScheduleTest.FinishAt),
                        Test = ua.ScheduleTest.Test is not null ? new Test
                        {
                            Id = ua.ScheduleTest.TestId.ToString(),
                            Name = ua.ScheduleTest.Test.Name,
                        } : null
                    } : null,
                    Question = ua.Question is not null ? new Question
                    {
                        Id = ua.QuestionId.ToString(),
                        Body = ua.Question.Body,
                        Complexity = ua.Question.Complexity,
                        TopicId = ua.Question.TopicId.ToString(),
                        DatabaseId = ua.Question.DatabaseId.ToString(),
                        CorrectAnswer = ua.Question.CorrectAnswer is not null && request.IncludeCorrectAnswers ? new CorrectAnswer
                        {
                            Id = ua.Question.CorrectAnswer.Id.ToString(),
                            Body = ua.Question.Body,
                            QuestionId = ua.Question.CorrectAnswer.QuestionId.ToString(),
                        } : null
                    } : null
                }) }
            };
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User answers have not been retrieved");
            return ResponseHelper.Fail<GetUserAnswersResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }
}