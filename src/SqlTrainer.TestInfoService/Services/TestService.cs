﻿using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Tests.Add;
using SqlTrainer.TestInfoService.Application.Tests.Delete;
using SqlTrainer.TestInfoService.Application.Tests.GetAll;
using SqlTrainer.TestInfoService.Application.Tests.GetById;
using SqlTrainer.TestInfoService.Application.Tests.Update;
using SqlTrainer.TestInfoService.Persistence.Helpers;

namespace SqlTrainer.TestInfoService.Services;

using MediatorAddTestRequest = Application.Tests.Add.AddTestRequest;
using MediatorDeleteTestRequest = Application.Tests.Delete.DeleteTestRequest;
using MediatorGetByIdTestRequest = Application.Tests.GetById.GetByIdTestRequest;
using MediatorGetAllTestsRequest = Application.Tests.GetAll.GetAllTestsRequest;
using MediatorUpdateTestRequest = Application.Tests.Update.UpdateTestRequest;

public sealed class TestService : TestInfoService.TestService.TestServiceBase
{
    private readonly IAddTestRequestHandler addTestRequestHandler;
    private readonly IDeleteTestRequestHandler deleteTestRequestHandler;
    private readonly IGetByIdTestRequestHandler getByIdTestRequestHandler;
    private readonly IGetAllTestsRequestHandler getAllTestsRequestHandler;
    private readonly IUpdateTestRequestHandler updateTestRequestHandler;
    private readonly ILogger<TestService> logger;

    public TestService(
        IAddTestRequestHandler addTestRequestHandler, 
        IDeleteTestRequestHandler deleteTestRequestHandler,
        IGetByIdTestRequestHandler getByIdTestRequestHandler, 
        IGetAllTestsRequestHandler getAllTestsRequestHandler, 
        IUpdateTestRequestHandler updateTestRequestHandler, 
        ILogger<TestService> logger)
    {
        this.addTestRequestHandler = addTestRequestHandler;
        this.deleteTestRequestHandler = deleteTestRequestHandler;
        this.getByIdTestRequestHandler = getByIdTestRequestHandler;
        this.getAllTestsRequestHandler = getAllTestsRequestHandler;
        this.updateTestRequestHandler = updateTestRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> Add(AddTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started adding of test with name {Name}", request.Name);
            var argumentConverter = new ArgumentsConverter();
            var questionMaxMarks = new List<(Guid, double)>();
            foreach (var questionMaxMark in request.Questions)
            {
                argumentConverter.TryParseGuid(questionMaxMark.QuestionId, out var questionId, "question id");
                questionMaxMarks.Add((questionId, questionMaxMark.MaxMark));
            }

            if (argumentConverter.HasErrors)
            {
                logger.LogError("Test with name {Name} has not been added, because {Errors}", request.Name, string.Join(", ", argumentConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var addTestRequest = new MediatorAddTestRequest(request.Name, request.CreatedAt.ToDateTime(), questionMaxMarks);
            var result = await addTestRequestHandler.HandleAsync(addTestRequest, context.CancellationToken);
            logger.LogInformation("Finished adding of test with name {Name}", request.Name);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been added. Name of test: {Name}", request.Name);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Delete(DeleteTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started deleting of test with id {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Test with id {Id} has not been deleted, because {Error}", request.Id, error);
                return ResponseHelper.Fail<Response>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            
            var deleteTestRequest = new MediatorDeleteTestRequest(id);
            var result = await deleteTestRequestHandler.HandleAsync(deleteTestRequest, context.CancellationToken);
            logger.LogInformation("Finished deleting of test with id {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been deleted. Id of test: {Id}", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Update(UpdateTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of test with id {Id}", request.Id);
            var argumentConverter = new ArgumentsConverter();
            argumentConverter.TryParseGuid(request.Id, out var id, "id");
            
            var questionMaxMarks = new List<(Guid, double)>();
            foreach (var questionMaxMark in request.Questions)
            {
                argumentConverter.TryParseGuid(questionMaxMark.QuestionId, out var questionId, "question id");
                questionMaxMarks.Add((questionId, questionMaxMark.MaxMark));
            }
            
            if (argumentConverter.HasErrors)
            {
                logger.LogError("Test with id {Id} has not been updated, because {Errors}", request.Id, string.Join(", ", argumentConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            
            var updateTestRequest = new MediatorUpdateTestRequest(id, request.Name, questionMaxMarks);
            var result = await updateTestRequestHandler.HandleAsync(updateTestRequest, context.CancellationToken);
            logger.LogInformation("Finished updating of test with id {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been updated. Id of test: {Id}", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetTestResponse> Get(GetByIdTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of test with id {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Test with id {Id} has not been retrieved, because {Error}", request.Id, error);
                return ResponseHelper.Fail<GetTestResponse>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }
            
            var getByIdTestRequest = new MediatorGetByIdTestRequest(id);
            var result = await getByIdTestRequestHandler.HandleAsync(getByIdTestRequest, context.CancellationToken);
            return result.ToResponse<GetTestResponse, Test, Domain.Models.Test>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, value) => r.Test = value,
                ToRpcTest);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Test has not been retrieved. Id of test: {Id}", request.Id);
            return ResponseHelper.Fail<GetTestResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetTestsResponse> GetAll(GetAllTestsRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of tests");
            var getAllTestsRequest = new MediatorGetAllTestsRequest(request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getAllTestsRequestHandler.HandleAsync(getAllTestsRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of tests");
            return result.ToEnumerableResponse<GetTestsResponse, Test, Domain.Models.Test>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, values) => r.Tests.AddRange(values),
                ToRpcTest);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Tests have not been retrieved");
            return ResponseHelper.Fail<GetTestsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    private static Test? ToRpcTest(Domain.Models.Test? test)
    {
        return test is not null ? new Test
        {
            Id = test.Id.ToString(),
            Name = test.Name,
            TestQuestions = { test.TestQuestions?.Select(tq => new TestQuestion
            {
                TestId = tq.TestId.ToString(),
                QuestionId = tq.QuestionId.ToString(),
                MaxMark = tq.MaxMark,
                Question = tq.Question is not null ? new Question
                {
                    Id = tq.Question.Id.ToString(),
                    Body = tq.Question.Body,
                    Complexity = tq.Question.Complexity,
                    DatabaseId = tq.Question.DatabaseId.ToString(),
                    TopicId = tq.Question.TopicId.ToString(),
                    CorrectAnswer = tq.Question.CorrectAnswer is not null ? new CorrectAnswer
                    {
                        Id = tq.Question.CorrectAnswer.Id.ToString(),
                        Body = tq.Question.CorrectAnswer.Body,
                        QuestionId = tq.Question.CorrectAnswer.QuestionId.ToString()
                    } : null,
                } : null
            }) }
        } : null;
    }
}