﻿using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Topics.Add;
using SqlTrainer.TestInfoService.Application.Topics.Delete;
using SqlTrainer.TestInfoService.Application.Topics.GetAll;
using SqlTrainer.TestInfoService.Application.Topics.GetById;
using SqlTrainer.TestInfoService.Application.Topics.Update;
using SqlTrainer.TestInfoService.Persistence.Helpers;

namespace SqlTrainer.TestInfoService.Services;

using MediatorAddTopicRequest = Application.Topics.Add.AddTopicRequest;
using MediatorDeleteTopicRequest = Application.Topics.Delete.DeleteTopicRequest;
using MediatorGetByIdTopicRequest = Application.Topics.GetById.GetByIdTopicRequest;
using MediatorGetAllTopicsRequest = Application.Topics.GetAll.GetAllTopicsRequest;
using MediatorUpdateTopicRequest = Application.Topics.Update.UpdateTopicRequest;

public sealed class TopicService : TestInfoService.TopicService.TopicServiceBase
{
    private readonly IAddTopicRequestHandler addTopicRequestHandler;
    private readonly IDeleteTopicRequestHandler deleteTopicRequestHandler;
    private readonly IGetByIdTopicRequestHandler getByIdTopicRequestHandler;
    private readonly IGetAllTopicsRequestHandler getAllTopicsRequestHandler;
    private readonly IUpdateTopicRequestHandler updateTopicRequestHandler;
    private readonly ILogger<TopicService> logger;

    public TopicService(
        IAddTopicRequestHandler addTopicRequestHandler, 
        IDeleteTopicRequestHandler deleteTopicRequestHandler, 
        IGetByIdTopicRequestHandler getByIdTopicRequestHandler, 
        IGetAllTopicsRequestHandler getAllTopicsRequestHandler, 
        IUpdateTopicRequestHandler updateTopicRequestHandler, 
        ILogger<TopicService> logger)
    {
        this.addTopicRequestHandler = addTopicRequestHandler;
        this.deleteTopicRequestHandler = deleteTopicRequestHandler;
        this.getByIdTopicRequestHandler = getByIdTopicRequestHandler;
        this.getAllTopicsRequestHandler = getAllTopicsRequestHandler;
        this.updateTopicRequestHandler = updateTopicRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> Add(AddTopicRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started adding of topic with name {Name}", request.Name);
            var addTopicRequest = new MediatorAddTopicRequest(request.Name);
            var result = await addTopicRequestHandler.HandleAsync(addTopicRequest, context.CancellationToken);
            logger.LogInformation("Finished adding of topic with name {Name}", request.Name);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic has not been added. Name of topic: {Name}", request.Name);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Delete(DeleteTopicRequest request, ServerCallContext context)
    {
        try
        {
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Topic with id {Id} has not been deleted, because {Error}", request.Id, "id");
                return ResponseHelper.Fail<Response>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var deleteTopicRequest = new MediatorDeleteTopicRequest(id);
            var result = await deleteTopicRequestHandler.HandleAsync(deleteTopicRequest, context.CancellationToken);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic with id {Id} has not been deleted", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Update(UpdateTopicRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of topic with id {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Topic with id {Id} has not been updated, because {Error}", request.Id, error);
                return ResponseHelper.Fail<Response>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var updateTopicRequest = new MediatorUpdateTopicRequest(id, request.Name);
            var result = await updateTopicRequestHandler.HandleAsync(updateTopicRequest, context.CancellationToken);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic with id {Id} has not been updated", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetTopicResponse> Get(GetByIdTopicRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of topic with id {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Topic with id {Id} has not been retrieved, because {Error}", request.Id, error);
                return ResponseHelper.Fail<GetTopicResponse>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var getByIdTopicRequest = new MediatorGetByIdTopicRequest(id);
            var result = await getByIdTopicRequestHandler.HandleAsync(getByIdTopicRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of topic with id {Id}", request.Id);
            return result.ToResponse<GetTopicResponse, Topic, Domain.Models.Topic>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, value) => r.Topic = value,
                ToTopicRpcDto);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topic with id {Id} has not been retrieved", request.Id);
            return ResponseHelper.Fail<GetTopicResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetTopicsResponse> GetAll(GetAllTopicsRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of topics");
            var getAllTopicsRequest = new MediatorGetAllTopicsRequest(request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getAllTopicsRequestHandler.HandleAsync(getAllTopicsRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of topics");
            return result.ToEnumerableResponse<GetTopicsResponse, Topic, Domain.Models.Topic>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, values) => r.Topics.AddRange(values),
                ToTopicRpcDto);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Topics have not been retrieved");
            return ResponseHelper.Fail<GetTopicsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    private static Topic? ToTopicRpcDto(Domain.Models.Topic? topic) => topic is null ? null : new Topic
    {
        Id = topic.Id.ToString(),
        Name = topic.Name
    };
}