﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Add;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Delete;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetById;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Update;
using SqlTrainer.TestInfoService.Persistence.Helpers;

namespace SqlTrainer.TestInfoService.Services;

using MediatorAddScheduleTestRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.Add.AddScheduleTestRequest;
using MediatorDeleteScheduleTestRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.Delete.DeleteScheduleTestRequest;
using MediatorUpdateScheduleTestRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.Update.UpdateScheduleTestRequest;
using MediatorGetByIdScheduleTestRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.GetById.GetByIdScheduleTestRequest;
using MediatorGetAllScheduleTestsRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll.GetAllScheduleTestsRequest;
using MediatorGetByUserScheduleTestsRequest = SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser.GetByUserScheduleTestsRequest;

public class ScheduleTestService : TestInfoService.ScheduleTestService.ScheduleTestServiceBase
{
    private readonly IAddScheduleTestRequestHandler addScheduleTestRequestHandler;
    private readonly IDeleteScheduleTestRequestHandler deleteScheduleTestRequestHandler;
    private readonly IUpdateScheduleTestRequestHandler updateScheduleTestRequestHandler;
    private readonly IGetByIdScheduleTestRequestHandler getByIdScheduleTestRequestHandler;
    private readonly IGetAllScheduleTestsRequestHandler getAllScheduleTestsRequestHandler;
    private readonly IGetByUserScheduleTestsRequestHandler getByUserScheduleTestsRequestHandler;
    private readonly ILogger<ScheduleTestService> logger;

    public ScheduleTestService(
        IAddScheduleTestRequestHandler addScheduleTestRequestHandler, 
        IDeleteScheduleTestRequestHandler deleteScheduleTestRequestHandler, 
        IUpdateScheduleTestRequestHandler updateScheduleTestRequestHandler, 
        IGetByIdScheduleTestRequestHandler getByIdScheduleTestRequestHandler, 
        IGetAllScheduleTestsRequestHandler getAllScheduleTestsRequestHandler, 
        IGetByUserScheduleTestsRequestHandler getByUserScheduleTestsRequestHandler, 
        ILogger<ScheduleTestService> logger)
    {
        this.addScheduleTestRequestHandler = addScheduleTestRequestHandler;
        this.deleteScheduleTestRequestHandler = deleteScheduleTestRequestHandler;
        this.updateScheduleTestRequestHandler = updateScheduleTestRequestHandler;
        this.getByIdScheduleTestRequestHandler = getByIdScheduleTestRequestHandler;
        this.getAllScheduleTestsRequestHandler = getAllScheduleTestsRequestHandler;
        this.getByUserScheduleTestsRequestHandler = getByUserScheduleTestsRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> Add(AddScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started adding of schedule test. Test id: {TestId}, from {StartAt} and to {FinishAt}", request.TestId, request.StartAt, request.FinishAt);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.TestId, out var testId, "test id");
            var userIds = new List<Guid>();
            foreach (var userId in request.UserIds)
            {
                argumentsConverter.TryParseGuid(userId, out var parsedUserId, "user id");
                userIds.Add(parsedUserId);
            }

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("Schedule test has not been added. Test id: {TestId}, from {StartAt} and to {FinishAt}, because {Errors}", 
                    request.TestId, request.StartAt, request.FinishAt, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var addScheduleTestRequest = new MediatorAddScheduleTestRequest(testId, request.StartAt.ToDateTimeOffset(), request.FinishAt.ToDateTimeOffset(), userIds);
            var result = await addScheduleTestRequestHandler.HandleAsync(addScheduleTestRequest, context.CancellationToken);
            logger.LogInformation("Finished adding of schedule test. Test id: {TestId}, from {StartAt} and to {FinishAt}", request.TestId, request.StartAt, request.FinishAt);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been added. Test id: {TestId}, from {StartAt} and to {FinishAt}", request.TestId, request.StartAt, request.FinishAt);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }
    
    public override async Task<Response> Delete(DeleteScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started deleting of schedule test. Id: {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var scheduleTestId, "schedule test id", out var error))
            {
                logger.LogError("Schedule test has not been deleted. Id: {Id}, because {Error}", request.Id, error);
                return ResponseHelper.Fail<Response>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var deleteScheduleTestRequest = new MediatorDeleteScheduleTestRequest(scheduleTestId);
            var result = await deleteScheduleTestRequestHandler.HandleAsync(deleteScheduleTestRequest, context.CancellationToken);
            logger.LogInformation("Finished deleting of schedule test. Id: {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been deleted. Id: {Id}", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<Response> Update(UpdateScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of schedule test. Id: {Id}", request.Id);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.Id, out var scheduleTestId, "schedule test id")
                .TryParseGuid(request.TestId, out var testId, "test id");
            var userIds = new List<Guid>();
            foreach (var userId in request.UserIds)
            {
                argumentsConverter.TryParseGuid(userId, out var parsedUserId, "user id");
                userIds.Add(parsedUserId);
            }

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("Schedule test has not been updated. Id: {Id}, because {Errors}", request.Id, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var updateScheduleTestRequest = new MediatorUpdateScheduleTestRequest(scheduleTestId, testId, request.StartAt.ToDateTimeOffset(), request.FinishAt.ToDateTimeOffset(), userIds);
            var result = await updateScheduleTestRequestHandler.HandleAsync(updateScheduleTestRequest, context.CancellationToken);
            logger.LogInformation("Finished updating of schedule test. Id: {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been updated. Id: {Id}", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetScheduleTestResponse> Get(GetByIdScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of schedule test. Id: {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var scheduleTestId, "schedule test id", out var error))
            {
                logger.LogError("Schedule test has not been retrieved. Id: {Id}, because {Error}", request.Id, error);
                return ResponseHelper.Fail<GetScheduleTestResponse>(
                    error, 
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var getByIdScheduleTestRequest = new MediatorGetByIdScheduleTestRequest(scheduleTestId);
            var result = await getByIdScheduleTestRequestHandler.HandleAsync(getByIdScheduleTestRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of schedule test. Id: {Id}", request.Id);
            return result.ToResponse<GetScheduleTestResponse, ScheduleTest, Domain.Models.ScheduleTest>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, value) => r.ScheduleTest = value,
                ToRpcDto);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule test has not been retrieved. Id: {Id}", request.Id);
            return ResponseHelper.Fail<GetScheduleTestResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetScheduleTestsResponse> GetAll(GetAllScheduleTestsRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of schedule tests");
            var getAllScheduleTestsRequest = new MediatorGetAllScheduleTestsRequest(request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getAllScheduleTestsRequestHandler.HandleAsync(getAllScheduleTestsRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of schedule tests");
            return result.ToEnumerableResponse<GetScheduleTestsResponse, ScheduleTest, Domain.Models.ScheduleTest>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, values) => r.ScheduleTests.AddRange(values),
                ToRpcDto);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule tests have not been retrieved");
            return ResponseHelper.Fail<GetScheduleTestsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetScheduleTestsResponse> GetByUser(GetByUserScheduleTestsRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of schedule tests with user id {UserId}", request.UserId);
            if (!ArgumentsConverter.TryParseGuid(request.UserId, out var userId, "user id", out var error))
            {
                logger.LogError("Schedule tests have not been retrieved with user id {UserId}, because {Error}", request.UserId, error);
                return ResponseHelper.Fail<GetScheduleTestsResponse>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var getByUserScheduleTestsRequest = new MediatorGetByUserScheduleTestsRequest(userId, request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getByUserScheduleTestsRequestHandler.HandleAsync(getByUserScheduleTestsRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of schedule tests with user id {UserId}", request.UserId);
            return result.ToEnumerableResponse<GetScheduleTestsResponse, ScheduleTest, Domain.Models.ScheduleTest>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors),
                (r, values) => r.ScheduleTests.AddRange(values),
                ToRpcDto);
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Schedule tests with user id {UserId} have not been retrieved", request.UserId);
            return ResponseHelper.Fail<GetScheduleTestsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    private static ScheduleTest? ToRpcDto(Domain.Models.ScheduleTest? scheduleTest) => scheduleTest is null ? null : new ScheduleTest
    {
        Id = scheduleTest.Id.ToString(),
        TestId = scheduleTest.TestId.ToString(),
        StartAt = Timestamp.FromDateTimeOffset(scheduleTest.StartAt),
        FinishAt = Timestamp.FromDateTimeOffset(scheduleTest.FinishAt),
        Test = scheduleTest.Test is not null ? new Test
        {
            Id = scheduleTest.Test!.Id.ToString(),
            Name = scheduleTest.Test.Name
        } : null
    };
}