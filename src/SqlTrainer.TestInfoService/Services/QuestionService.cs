﻿using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Questions.Add;
using SqlTrainer.TestInfoService.Application.Questions.Delete;
using SqlTrainer.TestInfoService.Application.Questions.GetAll;
using SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;
using SqlTrainer.TestInfoService.Application.Questions.GetByTest;
using SqlTrainer.TestInfoService.Application.Questions.Update;
using SqlTrainer.TestInfoService.Persistence.Helpers;

namespace SqlTrainer.TestInfoService.Services;

using MediatorAddQuestionRequest = Application.Questions.Add.AddQuestionRequest;
using MediatorDeleteQuestionRequest = Application.Questions.Delete.DeleteQuestionRequest;
using MediatorGetAllQuestionsRequest = Application.Questions.GetAll.GetAllQuestionsRequest;
using MediatorGetQuestionsByScheduleTestRequest = Application.Questions.GetByScheduleTest.GetQuestionsByScheduleTestRequest;
using MediatorGetQuestionsByTestRequest = Application.Questions.GetByTest.GetQuestionsByTestRequest;
using MediatorUpdateQuestionRequest = Application.Questions.Update.UpdateQuestionRequest;

public sealed class QuestionService : TestInfoService.QuestionService.QuestionServiceBase
{
    private readonly IAddQuestionRequestHandler addQuestionRequestHandler;
    private readonly IDeleteQuestionRequestHandler deleteQuestionRequestHandler;
    private readonly IGetAllQuestionsRequestHandler getAllQuestionsRequestHandler;
    private readonly IGetQuestionsByScheduleTestRequestHandler getQuestionsByScheduleTestRequestHandler;
    private readonly IGetQuestionsByTestRequestHandler getQuestionsByTestRequestHandler;
    private readonly IUpdateQuestionRequestHandler updateQuestionRequestHandler;
    private readonly ILogger<QuestionService> logger;

    public QuestionService(
        IAddQuestionRequestHandler addQuestionRequestHandler,
        IDeleteQuestionRequestHandler deleteQuestionRequestHandler,
        IGetAllQuestionsRequestHandler getAllQuestionsRequestHandler,
        IGetQuestionsByScheduleTestRequestHandler getQuestionsByScheduleTestRequestHandler,
        IGetQuestionsByTestRequestHandler getQuestionsByTestRequestHandler,
        IUpdateQuestionRequestHandler updateQuestionRequestHandler,
        ILogger<QuestionService> logger)
    {
        this.addQuestionRequestHandler = addQuestionRequestHandler;
        this.deleteQuestionRequestHandler = deleteQuestionRequestHandler;
        this.getAllQuestionsRequestHandler = getAllQuestionsRequestHandler;
        this.getQuestionsByScheduleTestRequestHandler = getQuestionsByScheduleTestRequestHandler;
        this.getQuestionsByTestRequestHandler = getQuestionsByTestRequestHandler;
        this.updateQuestionRequestHandler = updateQuestionRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> Add(AddQuestionRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started adding of question with body: {Body}", request.Body);
            var convertErrors = new ArgumentsConverter()
                .TryParseGuid(request.TopicId, out var topicId, "topic id")
                .TryParseGuid(request.DatabaseId, out var databaseId, "database id")
                .Errors.ToArray();

            if (convertErrors.Length != 0)
            {
                logger.LogError("Question with body {Body} has not been added, because {Errors}", request.Body, string.Join(", ", convertErrors));
                return ResponseHelper.Fail<Response>(
                    convertErrors,
                    (r, success) => r.IsSuccess = success, 
                    (r, errors) => r.Errors.Add(errors));
            }

            var addQuestionRequest = new MediatorAddQuestionRequest(request.Body, request.Complexity, topicId, databaseId, request.CorrectAnswerBody);
            var result = await addQuestionRequestHandler.HandleAsync(addQuestionRequest, context.CancellationToken);
            logger.LogInformation("Finished adding of question with body: {Body}", request.Body);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.Add(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question has not been added. Body of question: {Body}", request.Body);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success, 
                (r, errors) => r.Errors.Add(errors));
        }
    }

    public override async Task<Response> Delete(DeleteQuestionRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started deleting of question with id: {Id}", request.Id);
            if (!ArgumentsConverter.TryParseGuid(request.Id, out var id, "id", out var error))
            {
                logger.LogError("Question with id {Id} has not been deleted, because {Error}", request.Id, error);
                return ResponseHelper.Fail<Response>(
                    error,
                    (r, success) => r.IsSuccess = success, 
                    (r, errors) => r.Errors.Add(errors));
            }

            var deleteQuestionRequest = new MediatorDeleteQuestionRequest(id);
            var result = await deleteQuestionRequestHandler.HandleAsync(deleteQuestionRequest, context.CancellationToken);
            logger.LogInformation("Finished deleting of question with id: {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.Add(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question has not been deleted. Id of question: {Id}", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.Add(errors));
        }
    }

    public override async Task<Response> Update(UpdateQuestionRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of question with id: {Id}", request.Id);
            var convertErrors = new ArgumentsConverter()
                .TryParseGuid(request.Id, out var id, "id")
                .TryParseGuid(request.TopicId, out var topicId, "topic id")
                .TryParseGuid(request.DatabaseId, out var databaseId, "database id")
                .TryParseGuid(request.CorrectAnswerId, out var correctAnswerId, "correct answer id")
                .Errors.ToArray();

            if (convertErrors.Length != 0)
            {
                logger.LogError("Question with id {Id} has not been added, because {Errors}", request.Id, string.Join(", ", convertErrors));
                return ResponseHelper.Fail<Response>(
                    convertErrors,
                    (r, success) => r.IsSuccess = success, 
                    (r, errors) => r.Errors.Add(errors));
            }
            
            var updateQuestionRequest = new MediatorUpdateQuestionRequest(id, request.Body, request.Complexity, topicId, databaseId, correctAnswerId, request.CorrectAnswerBody);
            var result = await updateQuestionRequestHandler.HandleAsync(updateQuestionRequest, context.CancellationToken);
            logger.LogInformation("Finished updating of question with id: {Id}", request.Id);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.Add(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Question with id {Id} has not been updated", request.Id);
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.Add(errors));
        }
    }

    public override async Task<GetQuestionsResponse> GetAll(GetAllQuestionsRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of questions");
            var getAllQuestionsRequest = new MediatorGetAllQuestionsRequest(request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getAllQuestionsRequestHandler.HandleAsync(getAllQuestionsRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of questions");
            return new GetQuestionsResponse
            {
                IsSuccess = result.IsSuccess,
                Errors = { result.Errors },
                Questions = { result.Value.Select(q => ToQuestionRpcDto(q, request.IncludeCorrectAnswer)) }
            };
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions has not been retrieved");
            return ResponseHelper.Fail<GetQuestionsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetQuestionsResponse> GetByTest(GetQuestionsByTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of questions with test id {TestId}", request.TestId);
            if (!ArgumentsConverter.TryParseGuid(request.TestId, out var testId, "test id", out var error))
            {
                logger.LogError("Questions with test id {TestId} has not been retrieved, because {Error}", request.TestId, error);
                return ResponseHelper.Fail<GetQuestionsResponse>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var getQuestionsByTestRequest = new MediatorGetQuestionsByTestRequest(testId, request.SearchTerm, request.OrderBy, request.OrderByDirection.FromSqlString(), request.Page, request.PageSize);
            var result = await getQuestionsByTestRequestHandler.HandleAsync(getQuestionsByTestRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of questions with test id {TestId}", request.TestId);
            return new GetQuestionsResponse
            {
                IsSuccess = result.IsSuccess,
                Errors = { result.Errors },
                Questions = { result.Value.Select(q => ToQuestionRpcDto(q, request.IncludeCorrectAnswer)) }
            };
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions with test id {TestId} has not been retrieved", request.TestId);
            return ResponseHelper.Fail<GetQuestionsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }

    public override async Task<GetQuestionsResponse> GetByScheduleTest(GetQuestionsByScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started retrieving of questions with schedule test id {ScheduleTestId}", request.ScheduleTestId);
            if (!ArgumentsConverter.TryParseGuid(request.ScheduleTestId, out var scheduleTestId, "schedule test id", out var error))
            {
                logger.LogError("Questions with schedule test id {ScheduleTestId} has not been retrieved, because {Error}", request.ScheduleTestId, error);
                return ResponseHelper.Fail<GetQuestionsResponse>(
                    error,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var getQuestionsByScheduleTestRequest = new MediatorGetQuestionsByScheduleTestRequest(scheduleTestId);
            var result = await getQuestionsByScheduleTestRequestHandler.HandleAsync(getQuestionsByScheduleTestRequest, context.CancellationToken);
            logger.LogInformation("Finished retrieving of questions with schedule test id {ScheduleTestId}", request.ScheduleTestId);
            return new GetQuestionsResponse
            {
                IsSuccess = result.IsSuccess,
                Errors = { result.Errors },
                Questions = { result.Value.Select(q => ToQuestionRpcDto(q, request.IncludeCorrectAnswer)) }
            };
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "Questions with schedule test id {ScheduleTestId} has not been retrieved", request.ScheduleTestId);
            return ResponseHelper.Fail<GetQuestionsResponse>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }
    
    private static Question ToQuestionRpcDto(Domain.Models.Question question, bool includeCorrectAnswer = false)
    {
        var addCorrectAnswer = includeCorrectAnswer && question.CorrectAnswer is not null;
        return new Question
        {
            Id = question.Id.ToString(),
            Body = question.Body,
            Complexity = question.Complexity,
            TopicId = question.TopicId.ToString(),
            DatabaseId = question.DatabaseId.ToString(),
            CorrectAnswer = addCorrectAnswer ? new()
            {
                Id = question.CorrectAnswer!.Id.ToString(),
                Body = question.CorrectAnswer!.Body,
                QuestionId = question.CorrectAnswer!.QuestionId.ToString()
            } : null
        };
    }
}