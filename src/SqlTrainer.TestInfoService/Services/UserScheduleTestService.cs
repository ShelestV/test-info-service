﻿using Grpc.Core;
using GrpcHelper;
using GrpcHelper.Extensions;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;

namespace SqlTrainer.TestInfoService.Services;

using MediatorUpdateUserScheduleTestRequest = SqlTrainer.TestInfoService.Application.UserScheduleTests.Update.UpdateUserScheduleTestRequest;

public sealed class UserScheduleTestService : TestInfoService.UserScheduleTestService.UserScheduleTestServiceBase
{
    private readonly IUpdateUserScheduleTestRequestHandler updateUserScheduleTestRequestHandler;
    private readonly ILogger<UserScheduleTestService> logger;

    public UserScheduleTestService(
        IUpdateUserScheduleTestRequestHandler updateUserScheduleTestRequestHandler,
        ILogger<UserScheduleTestService> logger)
    {
        this.updateUserScheduleTestRequestHandler = updateUserScheduleTestRequestHandler;
        this.logger = logger;
    }

    public override async Task<Response> Update(UpdateUserScheduleTestRequest request, ServerCallContext context)
    {
        try
        {
            logger.LogInformation("Started updating of user schedule test with schedule test id {ScheduleTestId} and user id {UserId}", request.ScheduleTestId, request.UserId);
            var argumentsConverter = new ArgumentsConverter()
                .TryParseGuid(request.ScheduleTestId, out var scheduleTestId, "schedule test id")
                .TryParseGuid(request.UserId, out var userId, "user id");

            if (argumentsConverter.HasErrors)
            {
                logger.LogError("User schedule test with schedule test id {ScheduleTestId} and user id {UserId} has not been updated, because {Errors}",
                    request.ScheduleTestId, request.UserId, string.Join(", ", argumentsConverter.Errors));
                return ResponseHelper.Fail<Response>(
                    argumentsConverter.Errors,
                    (r, success) => r.IsSuccess = success,
                    (r, errors) => r.Errors.AddRange(errors));
            }

            var updateRequest = new MediatorUpdateUserScheduleTestRequest(scheduleTestId, userId, request.FinishedAt?.ToDateTimeOffset(), request.CheckedByTeacher);
            var result = await updateUserScheduleTestRequestHandler.HandleAsync(updateRequest, context.CancellationToken);
            logger.LogInformation("Finished updating of user schedule test with schedule test id {ScheduleTestId} and user id {UserId}", request.ScheduleTestId, request.UserId);
            return result.ToResponse<Response>(
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "User schedule test has not been updated");
            return ResponseHelper.Fail<Response>(
                ex,
                (r, success) => r.IsSuccess = success,
                (r, errors) => r.Errors.AddRange(errors));
        }
    }
}