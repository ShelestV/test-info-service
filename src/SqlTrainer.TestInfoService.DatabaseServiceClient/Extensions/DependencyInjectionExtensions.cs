using GrpcHelper.DependencyInjection.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.DatabaseServiceClient.Services;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Extensions;

public static class DependencyInjectionExtensions
{
    public static IServiceCollection AddDatabaseServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<IScriptService, ScriptService>();

        var databaseServiceUrl = configuration["DATABASE_INFO_SERVICE_URL"]!;
        services.AddNotSecureGrpcClient<DatabaseInfoService.ScriptService.ScriptServiceClient>(databaseServiceUrl);
        return services;
    }
}