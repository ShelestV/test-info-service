using System.ComponentModel;
using Results;
using SqlTrainer.DatabaseInfoService;
using SqlTrainer.TestInfoService.Application.Services;

namespace SqlTrainer.TestInfoService.DatabaseServiceClient.Services;

public sealed class ScriptService : IScriptService
{
    private readonly DatabaseInfoService.ScriptService.ScriptServiceClient scriptServiceClient;
    
    public ScriptService(
        DatabaseInfoService.ScriptService.ScriptServiceClient scriptServiceClient)
    {
        this.scriptServiceClient = scriptServiceClient;
    }
    
    public async Task<Result<DatabaseHelper.ScriptResults.ScriptResult>> ExecuteScriptAsync(string script, Guid databaseId, CancellationToken cancellationToken = default)
    {
        var result = Result<DatabaseHelper.ScriptResults.ScriptResult>.Create();
        var request = new ExecuteRequest
        {
            DatabaseId = databaseId.ToString(),
            Script = script
        };
        var response = await scriptServiceClient.ExecuteAsync(request, cancellationToken: cancellationToken);
        if (!response.IsSuccess)
        {
            result.Fail(response.Errors);
            return result;
        }
        
        result.Complete(FromRpcDto(response.Result));
        return result;
    }

    public async Task<Result<IReadOnlyCollection<DatabaseHelper.ScriptResults.ScriptResult>>> ExecuteScriptsAsync(IEnumerable<string> scripts, Guid databaseId, CancellationToken cancellationToken = default)
    {
        var result = Result<IReadOnlyCollection<DatabaseHelper.ScriptResults.ScriptResult>>.Create();
        var request = new ExecuteBatchRequest
        {
            DatabaseId = databaseId.ToString(),
            Scripts = { scripts }
        };
        var response = await scriptServiceClient.ExecuteBatchAsync(request, cancellationToken: cancellationToken);
        if (!response.IsSuccess)
        {
            result.Fail(response.Errors);
            return result;
        }
        
        result.Complete(response.Results.Select(FromRpcDto).ToList());
        return result;
    }
    
    private static DatabaseHelper.ScriptResults.ScriptResult FromRpcDto(ScriptResult script)
    {
        var scriptResult = new DatabaseHelper.ScriptResults.ScriptResult { Script = script.Script };
        foreach (var obj in script.Objects)
        {
            if (obj is null)
                continue;
            scriptResult.Add(FromRpcDto(obj));
        }
        return scriptResult;
    }

    private static DatabaseHelper.ScriptResults.ScriptResultObject FromRpcDto(ScriptResultObject scriptResultObject)
    {
        var scriptObject = new DatabaseHelper.ScriptResults.ScriptResultObject();
        foreach (var objValue in scriptResultObject.Values)
        {
            if (objValue is null)
                continue;
            scriptObject.Add(FromRpcDto(objValue));
        }
        return scriptObject;
    }

    private static DatabaseHelper.ScriptResults.ScriptResultObjectValue FromRpcDto(ScriptResultObjectValue scriptResultObjectValue)
    {
        var converter = TypeDescriptor.GetConverter(scriptResultObjectValue.CsharpType);
        return new DatabaseHelper.ScriptResults.ScriptResultObjectValue(
            scriptResultObjectValue.Name, 
            converter.ConvertFrom(scriptResultObjectValue.Value)!);
    }
}