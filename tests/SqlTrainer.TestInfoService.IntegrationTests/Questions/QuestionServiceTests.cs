﻿using Moq;
using Grpc.Core;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Questions;

[Collection(DatabaseDefinition.Name)]
public class QuestionServiceTests
{
    private readonly SqlTrainer.TestInfoService.Services.QuestionService sut;

    private QuestionServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new QuestionServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateQuestionService();
    }

    public class AddTests : QuestionServiceTests
    {
        private readonly Domain.Models.Question question;
        
        public AddTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            question = QuestionServiceInitializationHelper.AddQuestion;
        }

        [Theory]
        [InlineData(QuestionServiceInitializationHelper.TopicIdString, QuestionServiceInitializationHelper.DatabaseIdString, 0)]
        [InlineData(QuestionServiceInitializationHelper.TopicIdString, "invalid", 1)]
        [InlineData("invalid", QuestionServiceInitializationHelper.DatabaseIdString, 1)]
        [InlineData("invalid", "invalid", 2)]
        public async Task Add_ReturnResponse(string topicId, string databaseId, int errorsCount)
        {
            // Arrange
            var addQuestionRequest = new AddQuestionRequest
            {
                Body = question.Body,
                Complexity = question.Complexity,
                TopicId = topicId,
                CorrectAnswerBody = question.CorrectAnswer!.Body,
                DatabaseId = databaseId
            };
            
            // Act
            var response = await sut.Add(addQuestionRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class DeleteTests : QuestionServiceTests
    {
        public DeleteTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(QuestionServiceInitializationHelper.DeleteQuestionIdString)]
        [InlineData("invalid")]
        public async Task Delete_ReturnResponse(string id)
        {
            // Arrange
            var deleteQuestionRequest = new DeleteQuestionRequest { Id = id };
            
            // Act
            var response = await sut.Delete(deleteQuestionRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class UpdateTests : QuestionServiceTests
    {
        private const string QuestionId = QuestionServiceInitializationHelper.UpdateQuestionIdString;
        private const string TopicId = QuestionServiceInitializationHelper.TopicIdString;
        private const string DatabaseId = QuestionServiceInitializationHelper.DatabaseIdString;
        private const string CorrectAnswerId = QuestionServiceInitializationHelper.UpdateCorrectAnswerIdString;
        
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(QuestionId, TopicId, DatabaseId, CorrectAnswerId, 0)]
        [InlineData(QuestionId, TopicId, DatabaseId, "invalid", 1)]
        [InlineData(QuestionId, TopicId, "invalid", CorrectAnswerId, 1)]
        [InlineData(QuestionId, "invalid", DatabaseId, CorrectAnswerId, 1)]
        [InlineData("invalid", TopicId, DatabaseId, CorrectAnswerId, 1)]
        [InlineData("invalid", "invalid", DatabaseId, CorrectAnswerId, 2)]
        [InlineData("invalid", TopicId, "invalid", CorrectAnswerId, 2)]
        [InlineData("invalid", TopicId, DatabaseId, "invalid", 2)]
        [InlineData(QuestionId, "invalid", "invalid", CorrectAnswerId, 2)]
        [InlineData(QuestionId, "invalid", DatabaseId, "invalid", 2)]
        [InlineData(QuestionId, TopicId, "invalid", "invalid", 2)]
        [InlineData("invalid", "invalid", "invalid", CorrectAnswerId, 3)]
        [InlineData("invalid", "invalid", DatabaseId, "invalid", 3)]
        [InlineData("invalid", TopicId, "invalid", "invalid", 3)]
        [InlineData(QuestionId, "invalid", "invalid", "invalid", 3)]
        [InlineData("invalid", "invalid", "invalid", "invalid", 4)]
        public async Task Update_ReturnResponse(
            string questionId, string topicId, string databaseId, string correctAnswerId, int errorsCount)
        {
            // Arrange
            var updateQuestionRequest = new UpdateQuestionRequest
            {
                Id = questionId,
                Body = "Update Question Test Question Updated",
                Complexity = 5,
                TopicId = topicId,
                CorrectAnswerBody = "Correct Answer Updated Test Body",
                CorrectAnswerId = correctAnswerId,
                DatabaseId = databaseId
            };
            
            // Act
            var response = await sut.Update(updateQuestionRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class GetAllTests : QuestionServiceTests
    {
        public GetAllTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async Task GetAll_ReturnResponse(bool includeCorrectAnswers)
        {
            // Arrange
            var getAllQuestionsRequest = new GetAllQuestionsRequest
            {
                SearchTerm = "_question_",
                IncludeCorrectAnswer = includeCorrectAnswers,
                OrderBy = "body",
                OrderByDirection = "asc",
                Page = 4,
                PageSize = 100
            };
            
            // Act
            var response = await sut.GetAll(getAllQuestionsRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetQuestionsResponse>();
            response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
            response.Errors.Should().BeEmpty();
            response.Questions.Should().NotBeEmpty();
            response.Questions.Count.Should().Be(100);
            if (includeCorrectAnswers)
                response.Questions.First().CorrectAnswer.Should().NotBeNull();
            else
                response.Questions.First().CorrectAnswer.Should().BeNull();
            var questionNames = response.Questions.Select(q => q.Body).ToArray();
            var expectedQuestionNames = QuestionServiceInitializationHelper.Questions
                .Where(q => q.Body.Contains("_question_"))
                .OrderBy(q => q.Body).ThenBy(q => q.Id)
                .Skip(300).Take(100)
                .Select(q => q.Body).ToArray();
            questionNames.Should().BeEquivalentTo(expectedQuestionNames);
        }
    }
    
    public class GetByTestTests : QuestionServiceTests
    {
        public GetByTestTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(QuestionServiceInitializationHelper.TestIdString, true)]
        [InlineData(QuestionServiceInitializationHelper.TestIdString, false)]
        [InlineData("invalid", false)]
        public async Task GetByTest_ReturnResponse(string testId, bool includeCorrectAnswers)
        {
            // Arrange
            var getByTestQuestionsRequest = new GetQuestionsByTestRequest
            {
                TestId = testId,
                SearchTerm = "_question_",
                IncludeCorrectAnswer = includeCorrectAnswers,
                OrderBy = "body",
                OrderByDirection = "asc",
                Page = 3,
                PageSize = 50
            };
            
            // Act
            var response = await sut.GetByTest(getByTestQuestionsRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetQuestionsResponse>();
            if (Guid.TryParse(testId, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.Questions.Should().HaveCount(50);
                if (includeCorrectAnswers)
                    response.Questions.First().CorrectAnswer.Should().NotBeNull();
                else
                    response.Questions.First().CorrectAnswer.Should().BeNull();
                var questionNames = response.Questions.Select(q => q.Body).ToArray();
                questionNames.Should().BeInAscendingOrder();
                var testQuestionIds = QuestionServiceInitializationHelper.Test1.TestQuestions!.Select(tq => tq.QuestionId).ToArray();
                var expectedQuestionNames = QuestionServiceInitializationHelper.Questions
                    .Where(q => testQuestionIds.Contains(q.Id) && q.Body.Contains("_question_"))
                    .OrderBy(q => q.Body).ThenBy(q => q.Id)
                    .Skip(100).Take(50).Select(q => q.Body).ToArray();
                questionNames.Should().BeEquivalentTo(expectedQuestionNames);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }
    
    public class GetByScheduleTestTests : QuestionServiceTests
    {
        public GetByScheduleTestTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }
        
        [Theory]
        [InlineData(QuestionServiceInitializationHelper.ScheduleTestIdString, true)]
        [InlineData(QuestionServiceInitializationHelper.ScheduleTestIdString, false)]
        [InlineData("invalid", false)]
        public async Task GetByScheduleTest_ReturnResponse(string scheduleTestId, bool includeCorrectAnswers)
        {
            // Arrange
            var getByScheduleTestQuestionsRequest = new GetQuestionsByScheduleTestRequest
            {
                ScheduleTestId = scheduleTestId,
                IncludeCorrectAnswer = includeCorrectAnswers
            };
            
            // Act
            var response = await sut.GetByScheduleTest(getByScheduleTestQuestionsRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetQuestionsResponse>();
            if (Guid.TryParse(scheduleTestId, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.Questions.Should().HaveCount(167);
                if (includeCorrectAnswers)
                    response.Questions.First().CorrectAnswer.Should().NotBeNull();
                else
                    response.Questions.First().CorrectAnswer.Should().BeNull();
                var questionNames = response.Questions.Select(q => q.Body).ToArray();
                var testQuestionIds = QuestionServiceInitializationHelper.Test1.TestQuestions!.Select(tq => tq.QuestionId).ToArray();
                var expectedQuestionNames = QuestionServiceInitializationHelper.Questions
                    .Where(q => testQuestionIds.Contains(q.Id))
                    .Select(q => q.Body);
                questionNames.Should().BeEquivalentTo(expectedQuestionNames);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }
}