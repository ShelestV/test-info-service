﻿using Moq;
using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using Persistence.Configurations;
using Results;
using SqlTrainer.TestInfoService.Application.Questions.Add;
using SqlTrainer.TestInfoService.Application.Questions.Delete;
using SqlTrainer.TestInfoService.Application.Questions.GetAll;
using SqlTrainer.TestInfoService.Application.Questions.GetByScheduleTest;
using SqlTrainer.TestInfoService.Application.Questions.GetByTest;
using SqlTrainer.TestInfoService.Application.Questions.Update;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Questions;

public sealed class QuestionServiceInitializationHelper : InitializationHelper
{
    public const string DatabaseIdString = "5D32099F-3E82-4D0C-9928-6F2E29DDA4ED";
    public const string TopicIdString = "72E9BA43-4DCF-4FB6-BE67-BD4A5E15B8E9";
    
    public const string UpdateQuestionIdString = "A8B76FB7-7F3C-449E-BDA9-483B14062148";
    public const string DeleteQuestionIdString = "432C48FC-302C-48EB-96BA-C5D418B6EDB9";
    public const string GetQuestionIdString = "AEEA775F-47DD-42E2-81C6-24D7CF09035A";
    public const string CheckAnswerQuestionIdString = "B261AC13-0A72-4660-9BBD-F2EF410FA262";
    
    public const string UpdateCorrectAnswerIdString = "B753E020-9FDD-47AC-8D8E-E5F5A51A1E3A";

    public const string TestIdString = "37C2627E-D77F-46B2-B9EE-0228B9BF1664";
    public const string ScheduleTestIdString = "A7EC238F-9C96-4940-97C0-167F587483A0";
    
    private static readonly Guid DatabaseId = Guid.Parse(DatabaseIdString);
    private static readonly Guid TopicId = Guid.Parse(TopicIdString);
    private static readonly Guid UpdateQuestionId = Guid.Parse(UpdateQuestionIdString);
    private static readonly Guid DeleteQuestionId = Guid.Parse(DeleteQuestionIdString);
    private static readonly Guid GetQuestionId = Guid.Parse(GetQuestionIdString);
    private static readonly Guid CheckAnswerQuestionId = Guid.Parse(CheckAnswerQuestionIdString);
    
    private static readonly Guid UpdateCorrectAnswerId = Guid.Parse(UpdateCorrectAnswerIdString);

    private static readonly Guid TestId = Guid.Parse(TestIdString);
    private static readonly Guid ScheduleTestId = Guid.Parse(ScheduleTestIdString);
    
    public static Domain.Models.Topic Topic { get; }
    public static Domain.Models.Question AddQuestion { get; }
    public static Domain.Models.Question UpdateQuestion { get; }
    public static Domain.Models.Question DeleteQuestion { get; }
    public static Domain.Models.Question GetQuestion { get; }
    public static Domain.Models.Question CheckAnswerQuestion { get; }
    public static IReadOnlyCollection<Domain.Models.Question> Questions { get; }

    public static Domain.Models.Test Test1 { get; }
    private static Domain.Models.Test Test2 { get; }

    public static Domain.Models.ScheduleTest ScheduleTest1 { get; }
    private static Domain.Models.ScheduleTest ScheduleTest2 { get; }
    
    public QuestionServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }

    static QuestionServiceInitializationHelper()
    {
        Topic = new(TopicId) { Name = $"Question Test Topic - {Guid.NewGuid()}" };
        
        AddQuestion = CreateQuestionWithCorrectAnswer("Question Add Test");
        UpdateQuestion = CreateQuestionWithCorrectAnswer("Question Update Test", UpdateQuestionId, UpdateCorrectAnswerId);
        DeleteQuestion = CreateQuestionWithCorrectAnswer("Question Delete Test", DeleteQuestionId);
        GetQuestion = CreateQuestionWithCorrectAnswer("Question Get Test", GetQuestionId);
        CheckAnswerQuestion = CreateQuestionWithCorrectAnswer("Question Check Answer Test", CheckAnswerQuestionId);

        Questions = Enumerable.Range(1, 500)
            .Select(i => CreateQuestionWithCorrectAnswer($"Question _question_ Get All Test {i.ToString().PadLeft(3, '0')}"))
            .ToArray();
        
        Test1 = new Domain.Models.Test(TestId) { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };
        Test2 = new Domain.Models.Test { Name = $"GetByScheduleTest Question Test Test - {Guid.NewGuid()}", CreatedAt = DateTime.Now };

        var startAt = DateTime.Now.AddDays(1);
        var finishAt = DateTime.Now.AddDays(1).AddHours(2);
        ScheduleTest1 = new Domain.Models.ScheduleTest(ScheduleTestId) { TestId = Test1.Id, StartAt = startAt, FinishAt = finishAt };
        ScheduleTest1.WithUserScheduleTests(Array.Empty<Domain.Models.UserScheduleTest>());
        ScheduleTest2 = new Domain.Models.ScheduleTest { TestId = Test2.Id, StartAt = startAt, FinishAt = finishAt };
        ScheduleTest2.WithUserScheduleTests(Array.Empty<Domain.Models.UserScheduleTest>());
    }

    public Services.QuestionService CreateQuestionService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);

        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        
        var addQuestionRequestValidator = new AddQuestionRequestValidator(topicRepository);
        var addQuestionLogger = Mock.Of<ILogger<AddQuestionRequestHandler>>();
        var addQuestionRequestHandler = new AddQuestionRequestHandler(questionRepository, correctAnswerRepository, addQuestionLogger, addQuestionRequestValidator, unitOfWork);
        
        var databaseService = new Mock<IScriptService>();
        var result = Result<ScriptResult>.Create();
        var resultBatch = Result<IReadOnlyCollection<ScriptResult>>.Create();
        result.Complete(new ScriptResult { Script = string.Empty });
        resultBatch.Complete(new [] { new ScriptResult { Script = string.Empty } });
        databaseService
            .Setup(ds => ds.ExecuteScriptAsync(It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(result);
        databaseService
            .Setup(ds => ds.ExecuteScriptsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(resultBatch);
        
        var updateQuestionRequestValidator = new UpdateQuestionRequestValidator(topicRepository);
        var updateQuestionLogger = Mock.Of<ILogger<UpdateQuestionRequestHandler>>();
        var updateQuestionRequestHandler = new UpdateQuestionRequestHandler(questionRepository, correctAnswerRepository, updateQuestionLogger, updateQuestionRequestValidator, unitOfWork);

        var deleteQuestionLogger = Mock.Of<ILogger<DeleteQuestionRequestHandler>>();
        var deleteQuestionRequestHandler = new DeleteQuestionRequestHandler(questionRepository, deleteQuestionLogger, unitOfWork);

        var getByTestQuestionsLogger = Mock.Of<ILogger<GetQuestionsByTestRequestHandler>>();
        var getByTestQuestionsRequestHandler = new GetQuestionsByTestRequestHandler(questionRepository, getByTestQuestionsLogger);

        var getByScheduleTestQuestionsLogger = Mock.Of<ILogger<GetQuestionsByScheduleTestRequestHandler>>();
        var getByScheduleTestQuestionsRequestHandler = new GetQuestionsByScheduleTestRequestHandler(questionRepository, getByScheduleTestQuestionsLogger);
        
        var getAllQuestionsLogger = Mock.Of<ILogger<GetAllQuestionsRequestHandler>>();
        var getAllQuestionsRequestHandler = new GetAllQuestionsRequestHandler(questionRepository, getAllQuestionsLogger);

        var questionServiceLogger = Mock.Of<ILogger<Services.QuestionService>>();
        var service = new Services.QuestionService(
            addQuestionRequestHandler,
            deleteQuestionRequestHandler,
            getAllQuestionsRequestHandler,
            getByScheduleTestQuestionsRequestHandler,
            getByTestQuestionsRequestHandler,
            updateQuestionRequestHandler,
            questionServiceLogger);

        return service;
    }

    public static async Task InitializeAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new QuestionServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        
        await topicRepository.AddAsync(Topic);
        
        await questionRepository.AddAsync(UpdateQuestion);
        await correctAnswerRepository.AddAsync(UpdateQuestion.CorrectAnswer!);
        
        await questionRepository.AddAsync(DeleteQuestion);
        await correctAnswerRepository.AddAsync(DeleteQuestion.CorrectAnswer!);
        
        await questionRepository.AddAsync(GetQuestion);
        await correctAnswerRepository.AddAsync(GetQuestion.CorrectAnswer!);

        await questionRepository.AddAsync(CheckAnswerQuestion);
        await correctAnswerRepository.AddAsync(CheckAnswerQuestion.CorrectAnswer!);
        
        await testRepository.AddAsync(Test1);
        await testRepository.AddAsync(Test2);
        await scheduleTestRepository.AddAsync(ScheduleTest1);
        await scheduleTestRepository.AddAsync(ScheduleTest2);

        var count = 0;
        var random = new Random();
        var testQuestions = new List<Domain.Models.TestQuestion>();
        foreach (var question in Questions)
        {
            await questionRepository.AddAsync(question);
            await correctAnswerRepository.AddAsync(question.CorrectAnswer!);
            
            count++;
            var countMod = count % 3;
            if (countMod == 0)
                continue;
            
            var test = countMod == 1 ? Test1 : Test2;
            var testQuestion = new Domain.Models.TestQuestion(test.Id, question.Id) { MaxMark = random.Next(1, 5) };
            testQuestions.Add(testQuestion);
            await testQuestionRepository.AddAsync(testQuestion);
        }
        Test1.WithTestQuestions(testQuestions.Where(tq => tq.TestId == Test1.Id).ToArray());
        Test2.WithTestQuestions(testQuestions.Where(tq => tq.TestId == Test2.Id).ToArray());

        unitOfWork.SaveChanges();
    }

    public static async Task CleanUpAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new QuestionServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);

        await testRepository.DeleteAsync(Test1.Id);
        await testRepository.DeleteAsync(Test2.Id);
        await scheduleTestRepository.DeleteAsync(ScheduleTest1.Id);
        await scheduleTestRepository.DeleteAsync(ScheduleTest2.Id);
        
        foreach (var question in Questions)
            await questionRepository.DeleteAsync(question.Id);
        await questionRepository.DeleteAsync(AddQuestion.Id);
        await questionRepository.DeleteAsync(UpdateQuestion.Id);
        await questionRepository.DeleteAsync(DeleteQuestion.Id);
        await questionRepository.DeleteAsync(GetQuestion.Id);
        await questionRepository.DeleteAsync(CheckAnswerQuestion.Id);
        
        await topicRepository.DeleteAsync(Topic.Id);
        unitOfWork.SaveChanges();
    }
    
    private static Domain.Models.Question CreateQuestionWithCorrectAnswer(string text, Guid questionId = default, Guid correctAnswerId = default)
    {
        var random = new Random();
        var question = new Domain.Models.Question(questionId)
        {
            Body = text + " Body - " + Guid.NewGuid(),
            TopicId = TopicId,
            Complexity = random.Next(1, 5),
            DatabaseId = DatabaseId
        };
        question.WithCorrectAnswer(new(correctAnswerId)
        {
            Body = text + " Correct Answer Body - " + Guid.NewGuid(),
            QuestionId = question.Id
        });
        
        return question;
    }
}