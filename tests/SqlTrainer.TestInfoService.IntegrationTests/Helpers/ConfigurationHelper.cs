﻿namespace SqlTrainer.TestInfoService.IntegrationTests.Helpers;

public sealed class ConfigurationHelper : PostgresServiceIntegrationTests.ConfigurationHelper
{
    protected override string ServerConfigKey => "POSTGRES_SERVER";
    protected override string PortConfigKey => "POSTGRES_PORT";
    protected override string UserConfigKey => "POSTGRES_USER";
    protected override string PasswordConfigKey => "POSTGRES_PASSWORD";
    protected override string DatabaseConfigKey => "POSTGRES_TESTS_DB";
}