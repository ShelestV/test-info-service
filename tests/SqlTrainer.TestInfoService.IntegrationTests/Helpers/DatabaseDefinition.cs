﻿namespace SqlTrainer.TestInfoService.IntegrationTests.Helpers;

[CollectionDefinition(Name)]
public class DatabaseDefinition : ICollectionFixture<DatabaseHelper>
{
    public const string Name = "TestInfoDb";
}