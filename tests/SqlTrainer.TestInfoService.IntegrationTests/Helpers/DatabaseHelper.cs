﻿using PostgresServiceIntegrationTests;
using SqlTrainer.TestInfoService.IntegrationTests.Questions;
using SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;
using SqlTrainer.TestInfoService.IntegrationTests.Tests;
using SqlTrainer.TestInfoService.IntegrationTests.Topics;
using SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;
using SqlTrainer.TestInfoService.IntegrationTests.UserScheduleTests;
using SqlTrainer.TestInfoService.Persistence.DbUp;

namespace SqlTrainer.TestInfoService.IntegrationTests.Helpers;

public sealed class DatabaseHelper : DatabaseInitializer<ConfigurationHelper>, IAsyncLifetime
{
    public DatabaseHelper() => new TestInfoDbUpService(DatabaseConfiguration).Migrate();
    
    public async Task InitializeAsync()
    {
        await TopicServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
        await QuestionServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
        await TestServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
        await ScheduleTestServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
        await UserScheduleTestServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
        await UserAnswerServiceInitializationHelper.InitializeAsync(DatabaseConfiguration);
    }

    public async Task DisposeAsync()
    {
        await TopicServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
        await QuestionServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
        await TestServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
        await ScheduleTestServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
        await UserScheduleTestServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
        await UserAnswerServiceInitializationHelper.CleanUpAsync(DatabaseConfiguration);
    }
}