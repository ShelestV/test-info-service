﻿using Persistence.Contexts;
using Persistence.Configurations;
using PostgresServiceIntegrationTests;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.Persistence.Repositories;

namespace SqlTrainer.TestInfoService.IntegrationTests.Helpers;

public class InitializationHelper : Initializer
{
    public InitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public static ITopicRepository CreateTopicRepository(DatabaseContext context) => new TopicRepository(context);
    public static IQuestionRepository CreateQuestionRepository(DatabaseContext context) => new QuestionRepository(context);
    public static ICorrectAnswerRepository CreateCorrectAnswerRepository(DatabaseContext context) => new CorrectAnswerRepository(context);
    public static ITestRepository CreateTestRepository(DatabaseContext context) => new TestRepository(context);
    public static IScheduleTestRepository CreateScheduleTestRepository(DatabaseContext context) => new ScheduleTestRepository(context);
    public static ITestQuestionRepository CreateTestQuestionRepository(DatabaseContext context) => new TestQuestionRepository(context);
    public static IUserScheduleTestRepository CreateUserScheduleTestRepository(DatabaseContext context) => new UserScheduleTestRepository(context);
    public static IUserAnswerRepository CreateUserAnswerRepository(DatabaseContext context) => new UserAnswerRepository(context);
}