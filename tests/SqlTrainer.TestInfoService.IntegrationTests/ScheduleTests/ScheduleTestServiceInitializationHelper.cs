﻿using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Configurations;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Add;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Delete;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetAll;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetById;
using SqlTrainer.TestInfoService.Application.ScheduleTests.GetByUser;
using SqlTrainer.TestInfoService.Application.ScheduleTests.Update;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

public sealed class ScheduleTestServiceInitializationHelper : InitializationHelper
{
    private const string TopicIdString = "01848604-083B-4BB3-B91B-23B1FC93F6AD";
    private const string QuestionIdString = "26D500A0-9AA8-475B-A70D-C790C217D9BB";
    private const string CorrectAnswerIdString = "BF4E2FB0-D4BE-4DEF-88C6-527DE94A0C49";
    public const string TestIdString = "D560FBA4-D1E4-4F03-A207-0F7C66508068";
    public const string GetUserIdString = "2A6D7FF3-098E-4D66-AB85-E7D7D6F1A4A4";
    public const string UserIdString = "A0192F3E-8954-403C-A027-DFEAA9A0FC4F";
    
    private const string AddScheduleTestIdString = "F21BC900-5F54-440A-A6BA-6FF8A7D6A9EA";
    public const string UpdateScheduleTestIdString = "E9C61E76-7814-4790-BC21-25525F2957CC";
    public const string DeleteScheduleTestIdString = "120AF1AF-ABB6-4F9E-87E8-04B7EC69ACAF";
    public const string GetScheduleTestIdString = "48DD9317-FEC7-46ED-88E9-A5FEE5896239";
    
    private static readonly Guid TopicId = Guid.Parse(TopicIdString);
    private static readonly Guid QuestionId = Guid.Parse(QuestionIdString);
    private static readonly Guid CorrectAnswerId = Guid.Parse(CorrectAnswerIdString);
    private static readonly Guid TestId = Guid.Parse(TestIdString);
    private static readonly Guid GetUserId = Guid.Parse(GetUserIdString);
    private static readonly Guid UserId = Guid.Parse(UserIdString);
    
    private static readonly Guid AddScheduleTestId = Guid.Parse(AddScheduleTestIdString);
    private static readonly Guid UpdateScheduleTestId = Guid.Parse(UpdateScheduleTestIdString);
    private static readonly Guid DeleteScheduleTestId = Guid.Parse(DeleteScheduleTestIdString);
    private static readonly Guid GetScheduleTestId = Guid.Parse(GetScheduleTestIdString);
    
    private static Domain.Models.Topic Topic { get; }
    private static Domain.Models.Question Question { get; }
    private static Domain.Models.CorrectAnswer CorrectAnswer { get; }
    private static Domain.Models.Test Test { get; }
    private static Domain.Models.TestQuestion TestQuestion { get; }
    
    private static Domain.Models.ScheduleTest AddScheduleTest { get; }
    public static Domain.Models.ScheduleTest UpdateScheduleTest { get; }
    private static Domain.Models.ScheduleTest DeleteScheduleTest { get; }
    public static Domain.Models.ScheduleTest GetScheduleTest { get; }
    public static IReadOnlyCollection<Domain.Models.ScheduleTest> ScheduleTests { get; }
    
    static ScheduleTestServiceInitializationHelper()
    {
        var random = new Random();
        Topic = new Domain.Models.Topic(TopicId) { Name = "Schedule Test Test Topic Name - " + Guid.NewGuid() };
        Question = new Domain.Models.Question(QuestionId)
        {
            Body = "Schedule Test Test Question Body - " + Guid.NewGuid(),
            TopicId = TopicId,
            Complexity = random.Next(1, 5),
            DatabaseId = Guid.NewGuid()
        };
        CorrectAnswer = new Domain.Models.CorrectAnswer(CorrectAnswerId)
        {
            Body = "Schedule Test Test Correct Answer Body - " + Guid.NewGuid(),
            QuestionId = QuestionId
        };
        Test = new Domain.Models.Test(TestId)
        {
            Name = "Schedule Test Test Name - " + Guid.NewGuid(),
            CreatedAt = DateTimeOffset.UtcNow
        };
        TestQuestion = new Domain.Models.TestQuestion(TestId, QuestionId) { MaxMark = 3.0 };
        
        AddScheduleTest = CreateWithUserScheduleTests(AddScheduleTestId);
        UpdateScheduleTest = CreateWithUserScheduleTests(UpdateScheduleTestId);
        DeleteScheduleTest = CreateWithUserScheduleTests(DeleteScheduleTestId);
        GetScheduleTest = CreateWithUserScheduleTests(GetScheduleTestId);
        var users = new[] { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), GetUserId, Guid.NewGuid() };
        ScheduleTests = Enumerable.Range(1, 500)
            .Select(i => CreateWithUserScheduleTests(userId: users[i % users.Length]))
            .ToArray();
    }
    
    public ScheduleTestServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public Services.ScheduleTestService CreateScheduleTestService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);

        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        
        var addScheduleTestRequestValidator = new AddScheduleTestRequestValidator(testRepository);
        var addScheduleTestLogger = Mock.Of<ILogger<AddScheduleTestRequestHandler>>();
        var addScheduleTestRequestHandler = new AddScheduleTestRequestHandler(scheduleTestRepository, addScheduleTestLogger, addScheduleTestRequestValidator, unitOfWork);
        
        var updateScheduleTestRequestValidator = new UpdateScheduleTestRequestValidator(testRepository);
        var updateScheduleTestLogger = Mock.Of<ILogger<UpdateScheduleTestRequestHandler>>();
        var updateScheduleTestRequestHandler = new UpdateScheduleTestRequestHandler(scheduleTestRepository, updateScheduleTestLogger, updateScheduleTestRequestValidator, unitOfWork);

        var deleteScheduleTestLogger = Mock.Of<ILogger<DeleteScheduleTestRequestHandler>>();
        var deleteScheduleTestRequestHandler = new DeleteScheduleTestRequestHandler(scheduleTestRepository, deleteScheduleTestLogger, unitOfWork);

        var getByIdScheduleTestLogger = Mock.Of<ILogger<GetByIdScheduleTestRequestHandler>>();
        var getByIdScheduleTestRequestHandler = new GetByIdScheduleTestRequestHandler(scheduleTestRepository, getByIdScheduleTestLogger);

        var getAllScheduleTestsLogger = Mock.Of<ILogger<GetAllScheduleTestsRequestHandler>>();
        var getAllScheduleTestsRequestHandler = new GetAllScheduleTestsRequestHandler(scheduleTestRepository, getAllScheduleTestsLogger);

        var getByUserScheduleTestsLogger = Mock.Of<ILogger<GetByUserScheduleTestsRequestHandler>>();
        var getByUserScheduleTestsRequestHandler = new GetByUserScheduleTestsRequestHandler(scheduleTestRepository, getByUserScheduleTestsLogger);
        
        var scheduleTestServiceLogger = Mock.Of<ILogger<Services.ScheduleTestService>>();
        var service = new Services.ScheduleTestService(
            addScheduleTestRequestHandler,
            deleteScheduleTestRequestHandler,
            updateScheduleTestRequestHandler,
            getByIdScheduleTestRequestHandler,
            getAllScheduleTestsRequestHandler,
            getByUserScheduleTestsRequestHandler,
            scheduleTestServiceLogger);

        return service;
    }

    public static async Task InitializeAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new ScheduleTestServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testRepository = CreateTestRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);

        await topicRepository.AddAsync(Topic);
        await questionRepository.AddAsync(Question);
        await correctAnswerRepository.AddAsync(CorrectAnswer);
        await testRepository.AddAsync(Test);
        await testQuestionRepository.AddAsync(TestQuestion);
        await scheduleTestRepository.AddAsync(AddScheduleTest);
        await scheduleTestRepository.AddAsync(UpdateScheduleTest);
        await scheduleTestRepository.AddAsync(DeleteScheduleTest);
        await scheduleTestRepository.AddAsync(GetScheduleTest);
        foreach (var scheduleTest in ScheduleTests)
            await scheduleTestRepository.AddAsync(scheduleTest);
        
        unitOfWork.SaveChanges();
    }

    public static async Task CleanUpAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new ScheduleTestServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        
        await scheduleTestRepository.DeleteAsync(AddScheduleTestId);
        await scheduleTestRepository.DeleteAsync(UpdateScheduleTestId);
        await scheduleTestRepository.DeleteAsync(DeleteScheduleTestId);
        await scheduleTestRepository.DeleteAsync(GetScheduleTestId);
        foreach (var scheduleTest in ScheduleTests)
            await scheduleTestRepository.DeleteAsync(scheduleTest.Id);
        await testRepository.DeleteAsync(TestId);
        await questionRepository.DeleteAsync(QuestionId);
        await topicRepository.DeleteAsync(TopicId);
        
        unitOfWork.SaveChanges();
    }
    
    private static Domain.Models.ScheduleTest CreateWithUserScheduleTests(Guid scheduleTestId = default, Guid? userId = null)
    {
        var random = new Random();
        var addDays = random.Next(1, 10);
        var addHours = random.Next(1, 24);
        var addMinutes = random.Next(1, 60);
        var scheduleTest = new Domain.Models.ScheduleTest(scheduleTestId)
        {
            TestId = TestId,
            StartAt = DateTimeOffset.UtcNow.AddDays(addDays).AddHours(addHours).AddMinutes(addMinutes),
            FinishAt = DateTimeOffset.UtcNow.AddDays(addDays).AddHours(addHours + 1).AddMinutes(addMinutes)
        };
        var userScheduleTest = new Domain.Models.UserScheduleTest(scheduleTest.Id, userId ?? UserId);
        return scheduleTest.WithUserScheduleTests(new[] { userScheduleTest });
    }
}