﻿using Moq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.ScheduleTests;

[Collection(DatabaseDefinition.Name)]
public class ScheduleTestServiceTests
{
    private readonly SqlTrainer.TestInfoService.Services.ScheduleTestService sut;
    
    private readonly IScheduleTestRepository scheduleTestRepository;

    private ScheduleTestServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new ScheduleTestServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateScheduleTestService();

        var context = initializer.CreateContext();
        scheduleTestRepository = InitializationHelper.CreateScheduleTestRepository(context);
    }
    
    public class AddTests : ScheduleTestServiceTests
    {
        public AddTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }
        
        [Theory]
        [InlineData(ScheduleTestServiceInitializationHelper.TestIdString, "31A25F70-1788-41A2-B258-62B9AD5ED49E", "4560689F-6DA2-48C1-A8CA-1896FA8C05FA", 0)]
        [InlineData(ScheduleTestServiceInitializationHelper.TestIdString, "31A25F70-1788-41A2-B258-62B9AD5ED49E", "invalid", 1)]
        [InlineData(ScheduleTestServiceInitializationHelper.TestIdString, "invalid", "4560689F-6DA2-48C1-A8CA-1896FA8C05FA", 1)]
        [InlineData("invalid", "31A25F70-1788-41A2-B258-62B9AD5ED49E", "4560689F-6DA2-48C1-A8CA-1896FA8C05FA", 1)]
        [InlineData("invalid", "invalid", "4560689F-6DA2-48C1-A8CA-1896FA8C05FA", 2)]
        [InlineData("invalid", "31A25F70-1788-41A2-B258-62B9AD5ED49E", "invalid", 2)]
        [InlineData(ScheduleTestServiceInitializationHelper.TestIdString, "invalid", "invalid", 2)]
        [InlineData("invalid", "invalid", "invalid", 3)]
        public async Task Add_ReturnResponse(string testId, string user1Id, string user2Id, int errorsCount)
        {
            // Arrange
            var scheduleTestRequest = new AddScheduleTestRequest
            {
                TestId = testId,
                StartAt = Timestamp.FromDateTime(DateTime.UtcNow.AddDays(1)),
                FinishAt = Timestamp.FromDateTime(DateTime.UtcNow.AddDays(1).AddHours(2)),
                UserIds = { new[] { user1Id, user2Id } }
            };
            
            // Act
            var response = await sut.Add(scheduleTestRequest, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class DeleteTests : ScheduleTestServiceTests
    {
        public DeleteTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(ScheduleTestServiceInitializationHelper.DeleteScheduleTestIdString)]
        [InlineData("invalid")]
        public async Task Delete_ReturnResponse(string id)
        {
            // Arrange
            var deleteScheduleTestRequest = new DeleteScheduleTestRequest { Id = id };
            
            // Act
            var response = await sut.Delete(deleteScheduleTestRequest, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class UpdateTests : ScheduleTestServiceTests
    {
        private const string ScheduleTestId = ScheduleTestServiceInitializationHelper.UpdateScheduleTestIdString;
        private const string TestId = ScheduleTestServiceInitializationHelper.TestIdString;
        private const string User1Id = "F8F4ECC5-2E15-4090-AEC8-F2C36EFCA126";
        private const string User2Id = "681D4A77-7716-4203-BDFF-6C3FFA0462B3";

        private readonly Domain.Models.ScheduleTest scheduleTest;
        
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            scheduleTest = ScheduleTestServiceInitializationHelper.UpdateScheduleTest;
        }

        [Theory]
        [InlineData(ScheduleTestId, TestId, User1Id, User2Id, 0)]
        [InlineData("invalid", TestId, User1Id, User2Id, 1)]
        [InlineData(ScheduleTestId, "invalid", User1Id, User2Id, 1)]
        [InlineData(ScheduleTestId, TestId, "invalid", User2Id, 1)]
        [InlineData(ScheduleTestId, TestId, User1Id, "invalid", 1)]
        [InlineData("invalid", "invalid", User1Id, User2Id, 2)]
        [InlineData("invalid", TestId, "invalid", User2Id, 2)]
        [InlineData("invalid", TestId, User1Id, "invalid", 2)]
        [InlineData(ScheduleTestId, "invalid", "invalid", User2Id, 2)]
        [InlineData(ScheduleTestId, "invalid", User1Id, "invalid", 2)]
        [InlineData(ScheduleTestId, TestId, "invalid", "invalid", 2)]
        [InlineData("invalid", "invalid", "invalid", User2Id, 3)]
        [InlineData("invalid", "invalid", User1Id, "invalid", 3)]
        [InlineData("invalid", TestId, "invalid", "invalid", 3)]
        [InlineData(ScheduleTestId, "invalid", "invalid", "invalid", 3)]
        [InlineData("invalid", "invalid", "invalid", "invalid", 4)]
        public async Task Update_ReturnResponse(string id, string testId, string user1Id, string user2Id, int errorsCount)
        {
            // Arrange
            var updateScheduleTestRequest = new UpdateScheduleTestRequest
            {
                Id = id,
                TestId = testId,
                StartAt = Timestamp.FromDateTimeOffset(DateTimeOffset.UtcNow.AddDays(2)),
                FinishAt = Timestamp.FromDateTimeOffset(DateTimeOffset.UtcNow.AddDays(2).AddHours(2)),
                UserIds = { new[] { user1Id, user2Id } }
            };
            
            // Act
            var response = await sut.Update(updateScheduleTestRequest, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                var updatedScheduleTest = await scheduleTestRepository.GetByIdAsync(scheduleTest.Id);
                updatedScheduleTest.Should().NotBeNull();
                updatedScheduleTest!.TestId.Should().Be(scheduleTest.TestId);
                updatedScheduleTest.StartAt.ToString("MM/dd/yyyy hh:mm").Should()
                    .Be(updateScheduleTestRequest.StartAt.ToDateTime().ToUniversalTime().ToString("MM/dd/yyyy hh:mm"));
                updatedScheduleTest.StartAt.ToString("MM/dd/yyyy hh:mm").Should()
                    .NotBe(scheduleTest.StartAt.ToString("MM/dd/yyyy hh:mm"));
                updatedScheduleTest.FinishAt.ToString("MM/dd/yyyy hh:mm").Should()
                    .Be(updateScheduleTestRequest.FinishAt.ToDateTime().ToUniversalTime().ToString("MM/dd/yyyy hh:mm"));
                updatedScheduleTest.FinishAt.ToString("MM/dd/yyyy hh:mm").Should()
                    .NotBe(scheduleTest.FinishAt.ToString("MM/dd/yyyy hh:mm"));
                updatedScheduleTest.UserScheduleTests.Should().NotBeEmpty();
                updatedScheduleTest.UserScheduleTests!.Count.Should().Be(2);
                updatedScheduleTest.UserScheduleTests.Should().NotContain(scheduleTest.UserScheduleTests!.First());
                var userIds = updatedScheduleTest.UserScheduleTests.Select(ust => ust.UserId.ToString().ToUpper()).ToArray();
                userIds.Should().Contain(updateScheduleTestRequest.UserIds.First().ToUpper());
                userIds.Should().Contain(updateScheduleTestRequest.UserIds.Last().ToUpper());
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class GetTests : ScheduleTestServiceTests
    {
        private readonly Domain.Models.ScheduleTest scheduleTest;
        
        public GetTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            scheduleTest = ScheduleTestServiceInitializationHelper.GetScheduleTest;
        }

        [Theory]
        [InlineData(ScheduleTestServiceInitializationHelper.GetScheduleTestIdString)]
        [InlineData("invalid")]
        public async Task Get_ReturnGetScheduleTestResponse(string id)
        {
            // Arrange
            var getScheduleTestRequest = new GetByIdScheduleTestRequest { Id = id };
            
            // Act
            var response = await sut.Get(getScheduleTestRequest, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<GetScheduleTestResponse>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.ScheduleTest.Should().NotBeNull();
                response.ScheduleTest.Id.Should().Be(scheduleTest.Id.ToString());
                response.ScheduleTest.TestId.Should().Be(scheduleTest.TestId.ToString());
                response.ScheduleTest.StartAt.ToDateTimeOffset().ToString("MM/dd/yyyy hh:mm").Should()
                    .Be(scheduleTest.StartAt.ToString("MM/dd/yyyy hh:mm"));
                response.ScheduleTest.FinishAt.ToDateTimeOffset().ToString("MM/dd/yyyy hh:mm").Should()
                    .Be(scheduleTest.FinishAt.ToString("MM/dd/yyyy hh:mm"));
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class GetAllTests : ScheduleTestServiceTests
    {
        public GetAllTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Fact]
        public async Task GetAll_ReturnGetScheduleTestsResponse()
        {
            // Arrange
            var getAllScheduleTestRequest = new GetAllScheduleTestsRequest
            {
                OrderBy = "startAt",
                OrderByDirection = "desc",
                Page = 5,
                PageSize = 25
            };

            // Act
            var response = await sut.GetAll(getAllScheduleTestRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetScheduleTestsResponse>();
            response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
            response.Errors.Should().BeEmpty();
            response.ScheduleTests.Should().HaveCount(25);
            response.ScheduleTests.Select(st => st.StartAt).Should().BeInDescendingOrder();
        }
    }

    public class GetByUser : ScheduleTestServiceTests
    {
        private readonly Domain.Models.ScheduleTest[] scheduleTests;
        
        public GetByUser(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            scheduleTests = ScheduleTestServiceInitializationHelper.ScheduleTests.ToArray();
        }

        [Theory]
        [InlineData(ScheduleTestServiceInitializationHelper.GetUserIdString)]
        [InlineData("invalid")]
        public async Task GetByUser_GetScheduleTestsResponse(string userId)
        {
            // Arrange
            var getByUserScheduleTestRequest = new GetByUserScheduleTestsRequest
            {
                UserId = userId,
                OrderBy = "startAt",
                OrderByDirection = "desc",
                Page = 3,
                PageSize = 20
            };
            
            // Act
            var response = await sut.GetByUser(getByUserScheduleTestRequest, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<GetScheduleTestsResponse>();
            if (Guid.TryParse(userId, out var userIdGuid))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.ScheduleTests.Should().HaveCount(20);
                response.ScheduleTests.Select(st => st.StartAt).Should().BeInDescendingOrder();
                var expectedScheduleTests = scheduleTests
                    .Where(st => st.UserScheduleTests!.Any(ust => ust.UserId == userIdGuid))
                    .OrderByDescending(st => st.StartAt).ThenByDescending(st => st.Id)
                    .Skip(40).Take(20).Select(st => st.Id.ToString().ToUpper()).ToArray();
                var scheduleTestIds = response.ScheduleTests.Select(st => st.Id.ToUpper());
                foreach (var id in scheduleTestIds)
                    expectedScheduleTests.Should().Contain(id);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }
}