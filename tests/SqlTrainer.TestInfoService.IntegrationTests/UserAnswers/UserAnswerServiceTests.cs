﻿using Moq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;

[Collection(DatabaseDefinition.Name)]
public class UserAnswerServiceTests
{
    private readonly Services.UserAnswerService sut;
    private readonly IUserAnswerRepository userAnswerRepository;
    
    private UserAnswerServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new UserAnswerServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateUserAnswerService();
        var context = initializer.CreateContext();
        userAnswerRepository = InitializationHelper.CreateUserAnswerRepository(context);
    }

    public class AddRangeTests : UserAnswerServiceTests
    {
        private const string ScheduleTestId = UserAnswerServiceInitializationHelper.AddUpdateScheduleTestIdString;
        private const string UserId = UserAnswerServiceInitializationHelper.AddUserIdString;
        private const string QuestionId1 = UserAnswerServiceInitializationHelper.AddQuestionIdString1;
        private const string QuestionId2 = UserAnswerServiceInitializationHelper.AddQuestionIdString2;
        private const string QuestionId3 = UserAnswerServiceInitializationHelper.AddQuestionIdString3;
        private const string QuestionId4 = UserAnswerServiceInitializationHelper.AddQuestionIdString4;
        private const string QuestionId5 = UserAnswerServiceInitializationHelper.AddQuestionIdString5;
        
        private readonly string[] userAnswerBodies;
        
        public AddRangeTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            userAnswerBodies = UserAnswerServiceInitializationHelper.UserAnswerBodies;
        }
        
        [Theory]
        [InlineData(ScheduleTestId, UserId, QuestionId1, QuestionId2, QuestionId3, QuestionId4, QuestionId5, 0)]
        [InlineData("invalid", UserId, QuestionId1, QuestionId2, QuestionId3, QuestionId4, QuestionId5, 1)]
        [InlineData(ScheduleTestId, "invalid", QuestionId1, QuestionId2, QuestionId3, QuestionId4, QuestionId5, 1)]
        [InlineData(ScheduleTestId, UserId, "invalid", QuestionId2, QuestionId3, QuestionId4, QuestionId5, 1)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, "invalid", QuestionId3, QuestionId4, QuestionId5, 1)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, QuestionId2, "invalid", QuestionId4, QuestionId5, 1)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, QuestionId2, QuestionId3, "invalid", QuestionId5, 1)]
        [InlineData("invalid", UserId, QuestionId1, QuestionId2, QuestionId3, QuestionId4, "invalid", 2)]
        [InlineData(ScheduleTestId, "invalid", "invalid", QuestionId2, QuestionId3, QuestionId4, QuestionId5, 2)]
        [InlineData(ScheduleTestId, "invalid", QuestionId1, "invalid", QuestionId3, QuestionId4, QuestionId5, 2)]
        [InlineData(ScheduleTestId, UserId, "invalid", QuestionId2, QuestionId3, "invalid", QuestionId5, 2)]
        [InlineData(ScheduleTestId, UserId, "invalid", QuestionId2, QuestionId3, QuestionId4, "invalid", 2)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, "invalid", "invalid", QuestionId4, QuestionId5, 2)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, "invalid", QuestionId3, "invalid", QuestionId5, 2)]
        [InlineData("invalid", UserId, "invalid", "invalid", QuestionId3, QuestionId4, QuestionId5, 3)]
        [InlineData("invalid", UserId, "invalid", QuestionId2, "invalid", QuestionId4, QuestionId5, 3)]
        [InlineData("invalid", UserId, "invalid", QuestionId2, QuestionId3, "invalid", QuestionId5, 3)]
        [InlineData("invalid", UserId, QuestionId1, QuestionId2, QuestionId3, "invalid", "invalid", 3)]
        [InlineData(ScheduleTestId, "invalid", "invalid", "invalid", QuestionId3, QuestionId4, QuestionId5, 3)]
        [InlineData(ScheduleTestId, "invalid", "invalid", QuestionId2, "invalid", QuestionId4, QuestionId5, 3)]
        [InlineData(ScheduleTestId, "invalid", "invalid", QuestionId2, QuestionId3, "invalid", QuestionId5, 3)]
        [InlineData(ScheduleTestId, "invalid", "invalid", QuestionId2, QuestionId3, QuestionId4, "invalid", 3)]
        [InlineData(ScheduleTestId, UserId, "invalid", "invalid", "invalid", QuestionId4, QuestionId5, 3)]
        [InlineData(ScheduleTestId, UserId, "invalid", "invalid", QuestionId3, "invalid", QuestionId5, 3)]
        [InlineData(ScheduleTestId, UserId, "invalid", "invalid", QuestionId3, QuestionId4, "invalid", 3)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, "invalid", QuestionId3, "invalid", "invalid", 3)]
        [InlineData(ScheduleTestId, UserId, QuestionId1, QuestionId2, "invalid", "invalid", "invalid", 3)]
        [InlineData("invalid", "invalid", "invalid", "invalid", QuestionId3, QuestionId4, QuestionId5, 4)]
        [InlineData(ScheduleTestId, "invalid", "invalid", "invalid", "invalid", QuestionId4, QuestionId5, 4)]
        [InlineData(ScheduleTestId, UserId, "invalid", "invalid", "invalid", "invalid", QuestionId5, 4)]
        [InlineData("invalid", "invalid", "invalid", "invalid", "invalid", QuestionId4, QuestionId5, 5)]
        [InlineData(ScheduleTestId, "invalid", "invalid", "invalid", "invalid", "invalid", QuestionId5, 5)]
        [InlineData("invalid", "invalid", "invalid", "invalid", "invalid", "invalid", QuestionId5, 6)]
        [InlineData("invalid", "invalid", "invalid", "invalid", "invalid", "invalid", "invalid", 7)]
        public async Task AddRange_ReturnResponse(string scheduleTestId, string userId, 
            string questionId1, string questionId2, string questionId3, string questionId4, string questionId5, 
            int errorsCount)
        {
            // Arrange
            var request = new AddUserAnswersRequest
            {
                ScheduleTestId = scheduleTestId,
                UserId = userId,
                AnsweredAt = Timestamp.FromDateTimeOffset(DateTimeOffset.UtcNow),
                QuestionBodies =
                {
                    new QuestionBody { QuestionId = questionId1, Body = userAnswerBodies[0] },
                    new QuestionBody { QuestionId = questionId2, Body = userAnswerBodies[1] },
                    new QuestionBody { QuestionId = questionId3, Body = userAnswerBodies[2] },
                    new QuestionBody { QuestionId = questionId4, Body = userAnswerBodies[3] },
                    new QuestionBody { QuestionId = questionId5, Body = userAnswerBodies[4] }
                }
            };
            
            // Act
            var response = await sut.AddRange(request, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                var userAnswers = await userAnswerRepository.GetByScheduleTestAndUserAsync(Guid.Parse(scheduleTestId), Guid.Parse(userId));
                userAnswers.Should().HaveCount(5);
                var userAnswersArray = userAnswers.ToArray();
                var questions = new[]
                {
                    Guid.Parse(questionId1),
                    Guid.Parse(questionId2),
                    Guid.Parse(questionId3),
                    Guid.Parse(questionId4),
                    Guid.Parse(questionId5)
                };
                for (var i = 0; i < 5; i++)
                {
                    userAnswersArray[i].ScheduleTestId.Should().Be(Guid.Parse(scheduleTestId));
                    userAnswersArray[i].UserId.Should().Be(userId);
                    userAnswersArray[i].AnsweredAt.ToString("MM/dd/yyyy hh:mm").Should()
                        .Be(request.AnsweredAt.ToDateTimeOffset().ToString("MM/dd/yyyy hh:mm"));
                    userAnswersArray[i].QuestionId.Should().Be(questions[i]);
                    userAnswersArray[i].Body.Should().Be(request.QuestionBodies[i].Body);
                    userAnswersArray[i].ProgramMark.Should().Be(2.0);
                }
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class UpdateTests : UserAnswerServiceTests
    {
        private const string ScheduleTestId = UserAnswerServiceInitializationHelper.AddUpdateScheduleTestIdString;
        private const string UserId = UserAnswerServiceInitializationHelper.UpdateUserIdString;
        private const string QuestionId = UserAnswerServiceInitializationHelper.UpdateQuestionIdString;
        
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(ScheduleTestId, UserId, QuestionId, 0)]
        [InlineData(ScheduleTestId, UserId, "invalid", 1)]
        [InlineData(ScheduleTestId, "invalid", QuestionId, 1)]
        [InlineData("invalid", UserId, QuestionId, 1)]
        [InlineData("invalid", "invalid", QuestionId, 2)]
        [InlineData(ScheduleTestId, "invalid", "invalid", 2)]
        [InlineData("invalid", UserId, "invalid", 2)]
        [InlineData("invalid", "invalid", "invalid", 3)]
        public async Task Update_ReturnResponse(string scheduleTestId, string userId, string questionId, int errorsCount)
        {
            // Arrange
            var request = new UpdateUserAnswerRequest
            {
                ScheduleTestId = scheduleTestId,
                UserId = userId,
                QuestionId = questionId,
                TeacherMark = 3.0
            };
            
            // Act
            var response = await sut.Update(request, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                var answers = await userAnswerRepository.GetByScheduleTestAndUserAsync(Guid.Parse(scheduleTestId), Guid.Parse(userId));
                answers.Should().NotBeNull();
                answers.Should().HaveCount(1);
                answers.First().TeacherMark.Should().Be(request.TeacherMark);
                answers.First().ProgramMark.Should().Be(1.0);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }

    public class GetByScheduleTestAndUserTests : UserAnswerServiceTests
    {
        private const string ScheduleTestId = UserAnswerServiceInitializationHelper.GetScheduleTestIdString;
        private const string UserId = UserAnswerServiceInitializationHelper.GetUserIdString;
        
        public GetByScheduleTestAndUserTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(ScheduleTestId, UserId, 0)]
        [InlineData(ScheduleTestId, "invalid", 1)]
        [InlineData("invalid", UserId, 1)]
        [InlineData("invalid", "invalid", 2)]
        public async Task GetByScheduleTestAndUser_ReturnResponse(string scheduleTestId, string userId, int errorsCount)
        {
            // Arrange
            var request = new GetUserAnswersByScheduleTestAndUserRequest
            {
                ScheduleTestId = scheduleTestId,
                UserId = userId
            };

            // Act
            var response = await sut.GetByScheduleTestAndUser(request, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetUserAnswersResponse>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.UserAnswers.Should().NotBeEmpty();
                var userAnswersFromDb = UserAnswerServiceInitializationHelper.UserAnswers
                    .Where(ua => ua.ScheduleTestId == Guid.Parse(scheduleTestId) && ua.UserId == Guid.Parse(userId))
                    .ToArray();
                response.UserAnswers.Should().HaveCount(userAnswersFromDb.Length);
                foreach (var userAnswer in response.UserAnswers)
                {
                    var userAnswerFromDb =
                        userAnswersFromDb.First(ua => ua.QuestionId == Guid.Parse(userAnswer.QuestionId));
                    userAnswerFromDb.Should().NotBeNull();
                    Guid.Parse(userAnswer.ScheduleTestId).Should().Be(userAnswerFromDb.ScheduleTestId);
                    Guid.Parse(userAnswer.UserId).Should().Be(userAnswerFromDb.UserId);
                    Guid.Parse(userAnswer.QuestionId).Should().Be(userAnswerFromDb.QuestionId);
                    userAnswer.Body.Should().Be(userAnswerFromDb.Body);
                    userAnswer.AnsweredAt.ToDateTimeOffset().ToString("MM/dd/yyyy hh:mm").Should()
                        .Be(userAnswerFromDb.AnsweredAt.ToString("MM/dd/yyyy hh:mm"));
                    userAnswer.ProgramMark.Should().Be(userAnswerFromDb.ProgramMark);
                    userAnswer.TeacherMark.Should().Be(0.0);
                }
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
                response.UserAnswers.Should().BeNullOrEmpty();
            }
        }
    }
}