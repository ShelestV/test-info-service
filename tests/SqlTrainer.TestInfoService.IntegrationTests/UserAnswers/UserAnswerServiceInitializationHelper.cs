﻿using DatabaseHelper.ScriptResults;
using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Configurations;
using Results;
using SqlTrainer.TestInfoService.Application.Services;
using SqlTrainer.TestInfoService.Application.UserAnswers.AddRange;
using SqlTrainer.TestInfoService.Application.UserAnswers.GetByScheduleTestAndUser;
using SqlTrainer.TestInfoService.Application.UserAnswers.Update;
using SqlTrainer.TestInfoService.Domain.Models;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserAnswers;

public sealed class UserAnswerServiceInitializationHelper : InitializationHelper
{
    public static readonly Guid[] DatabaseIds = { Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid() };

    public static readonly string[] CorrectAnswerBodies =
    {
        $"Add Answers Test Correct Answer 1 - {DatabaseIds[0]}",
        $"Add Answers Test Correct Answer 2 - {DatabaseIds[1]}",
        $"Add Answers Test Correct Answer 3 - {DatabaseIds[2]}",
        $"Add Answers Test Correct Answer 4 - {DatabaseIds[3]}",
        $"Add Answers Test Correct Answer 5 - {DatabaseIds[4]}",
    };
    
    public static readonly string[] UserAnswerBodies =
    {
        $"Add Answers Test User Answer 1 - {DatabaseIds[0]}",
        $"Add Answers Test User Answer 2 - {DatabaseIds[1]}",
        $"Add Answers Test User Answer 3 - {DatabaseIds[2]}",
        $"Add Answers Test User Answer 4 - {DatabaseIds[3]}",
        $"Add Answers Test User Answer 5 - {DatabaseIds[4]}",
    };

    public const string GetScheduleTestIdString = "A82B3CBE-884B-498F-9B85-C7E9ED2C0B75";
    public const string GetUserIdString = "599BD100-567C-4050-B14D-6F9359071301";
    public const string AddUpdateScheduleTestIdString = "59CC4E27-B370-447E-9A69-111C6FA5B1B4";
    public const string AddUserIdString = "4C1DA2E7-AF04-47F8-9B8D-4E9C63AC7E17";
    public const string UpdateUserIdString = "FD2B8428-0CCE-4E42-82D8-1492E0DE431F";
    public const string UpdateQuestionIdString = "355F965B-040D-408C-BDF3-61DB7207B301";
    public const string AddQuestionIdString1 = "92213AA6-E29D-459C-B246-F25D176DEA54";
    public const string AddQuestionIdString2 = "227D238E-C85D-45AA-B81C-A73C6F1C18B6";
    public const string AddQuestionIdString3 = "FB0A8CEC-6925-4D0C-B7DC-1FD77D79A74A";
    public const string AddQuestionIdString4 = "74B0A8DC-BF28-4B04-9377-266C73228970";
    public const string AddQuestionIdString5 = "A509365D-630B-474B-A50C-9AEA90247E32";
    
    private static readonly Guid GetScheduleTestId = Guid.Parse(GetScheduleTestIdString);
    private static readonly Guid GetUserId = Guid.Parse(GetUserIdString);
    private static readonly Guid AddUpdateScheduleTestId = Guid.Parse(AddUpdateScheduleTestIdString);
    private static readonly Guid AddUserId = Guid.Parse(AddUserIdString);
    private static readonly Guid UpdateUserId = Guid.Parse(UpdateUserIdString);
    private static readonly Guid UpdateQuestionId = Guid.Parse(UpdateQuestionIdString);
    private static readonly Guid AddQuestionId1 = Guid.Parse(AddQuestionIdString1);
    private static readonly Guid AddQuestionId2 = Guid.Parse(AddQuestionIdString2);
    private static readonly Guid AddQuestionId3 = Guid.Parse(AddQuestionIdString3);
    private static readonly Guid AddQuestionId4 = Guid.Parse(AddQuestionIdString4);
    private static readonly Guid AddQuestionId5 = Guid.Parse(AddQuestionIdString5);
    
    private static Domain.Models.Topic Topic { get; }
    private static Domain.Models.Test Test { get; }
    private static Domain.Models.Test[] Tests { get; }
    private static IReadOnlyCollection<Domain.Models.Question> AddQuestions { get; }
    private static Domain.Models.Question UpdateQuestion { get; }
    private static ICollection<Domain.Models.Question> Questions { get; }
    private static ICollection<Domain.Models.TestQuestion> AddUpdateTestQuestions { get; }
    private static ICollection<Domain.Models.TestQuestion> TestQuestions { get; }
    private static Domain.Models.ScheduleTest ScheduleTest { get; }
    private static ICollection<Domain.Models.ScheduleTest> ScheduleTests { get; }
    private static Guid[] UserIds { get; }
    private static Domain.Models.UserAnswer UpdateUserAnswer { get; }
    public static ICollection<Domain.Models.UserAnswer> UserAnswers { get; }

    static UserAnswerServiceInitializationHelper()
    {
        var random = new Random();
        
        Topic = new Domain.Models.Topic { Name = $"User Answer Test Topic - {Guid.NewGuid()}" };
        Test = new() { Name = $"User Answer Test Test - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow };
        Tests = new Domain.Models.Test[]
        {
            new() { Name = $"User Answer Test Test 1 - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow },
            new() { Name = $"User Answer Test Test 2 - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow },
            new() { Name = $"User Answer Test Test 3 - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow },
            new() { Name = $"User Answer Test Test 4 - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow },
            new() { Name = $"User Answer Test Test 5 - {Guid.NewGuid()}", CreatedAt = DateTimeOffset.UtcNow },
        };

        AddQuestions = new Domain.Models.Question[]
        {
            new(AddQuestionId1) { Body = $"Add User Answer Test Question - {Guid.NewGuid()}", TopicId = Topic.Id, Complexity = random.Next(1, 5), DatabaseId = DatabaseIds[0] },
            new(AddQuestionId2) { Body = $"Add User Answer Test Question - {Guid.NewGuid()}", TopicId = Topic.Id, Complexity = random.Next(1, 5), DatabaseId = DatabaseIds[1] },
            new(AddQuestionId3) { Body = $"Add User Answer Test Question - {Guid.NewGuid()}", TopicId = Topic.Id, Complexity = random.Next(1, 5), DatabaseId = DatabaseIds[2] },
            new(AddQuestionId4) { Body = $"Add User Answer Test Question - {Guid.NewGuid()}", TopicId = Topic.Id, Complexity = random.Next(1, 5), DatabaseId = DatabaseIds[3] },
            new(AddQuestionId5) { Body = $"Add User Answer Test Question - {Guid.NewGuid()}", TopicId = Topic.Id, Complexity = random.Next(1, 5), DatabaseId = DatabaseIds[4] },
        };
        UpdateQuestion = new Domain.Models.Question(UpdateQuestionId)
        {
            Body = $"Update User Answer Test Question - {Guid.NewGuid()}",
            Complexity = random.Next(1, 5),
            TopicId = Topic.Id,
            DatabaseId = DatabaseIds[0]
        };

        AddUpdateTestQuestions = new List<Domain.Models.TestQuestion>();
        AddQuestions = AddQuestions.Select((q, i) => q.WithCorrectAnswer(new()
        {
            Body = CorrectAnswerBodies[i],
            QuestionId = q.Id
        })).ToList();
        UpdateQuestion.WithCorrectAnswer(new()
        {
            Body = $"Update User Answer Test Correct Answer - {Guid.NewGuid()}",
            QuestionId = UpdateQuestion.Id
        });
        foreach (var question in AddQuestions.Concat(new[] { UpdateQuestion }))
            AddUpdateTestQuestions.Add(new(Test.Id, question.Id) { MaxMark = 3.0 });
        
        Questions = new List<Domain.Models.Question>();
        TestQuestions = new List<Domain.Models.TestQuestion>();
        for (var i = 1; i <= 25; i++)
        {
            var question = new Domain.Models.Question
            {
                Body = $"User Answer Test Question {i} - {Guid.NewGuid()}",
                Complexity = random.Next(1, 5), TopicId = Topic.Id, DatabaseId = DatabaseIds[i % 5]
            };
            Questions.Add(question);

            var correctAnswer = new Domain.Models.CorrectAnswer
            {
                Body = $"User Answer Test Correct Answer {i} - {Guid.NewGuid()}",
                QuestionId = question.Id
            };
            question.WithCorrectAnswer(correctAnswer);

            var testQuestion = new Domain.Models.TestQuestion(Tests[i % 5].Id, question.Id) { MaxMark = 3.0 };
            TestQuestions.Add(testQuestion);
        }

        ScheduleTest = new Domain.Models.ScheduleTest(AddUpdateScheduleTestId)
        {
            TestId = Test.Id, 
            StartAt = DateTimeOffset.UtcNow.AddHours(-1),
            FinishAt = DateTimeOffset.UtcNow.AddHours(1)
        };
        ScheduleTest.WithUserScheduleTests(new[]
        {
            new UserScheduleTest(ScheduleTest.Id, AddUserId), 
            new UserScheduleTest(ScheduleTest.Id, UpdateUserId)
        });
        
        ScheduleTests = new List<Domain.Models.ScheduleTest>();
        UserIds = Enumerable.Range(0, 20).Select(i => i == 10 ? GetUserId : Guid.NewGuid()).ToArray();
        var userIndex = 0;
        foreach (var test in Tests)
        {
            var scheduleTest = new Domain.Models.ScheduleTest(userIndex == 8 ? GetScheduleTestId : default)
            {
                TestId = test.Id, 
                StartAt = DateTimeOffset.UtcNow.AddHours(-1),
                FinishAt = DateTimeOffset.UtcNow.AddHours(1)
            };
            scheduleTest.WithUserScheduleTests(new[]
            {
                new UserScheduleTest(scheduleTest.Id, UserIds[userIndex++]),
                new UserScheduleTest(scheduleTest.Id, UserIds[userIndex++]),
                new UserScheduleTest(scheduleTest.Id, UserIds[userIndex++]),
                new UserScheduleTest(scheduleTest.Id, UserIds[userIndex++])
            });
            ScheduleTests.Add(scheduleTest);
        }

        UpdateUserAnswer = new Domain.Models.UserAnswer(ScheduleTest.Id, UpdateQuestion.Id, UpdateUserId)
        {
            Body = $"Update User Answer Test User Answer - {Guid.NewGuid()}",
            AnsweredAt = DateTimeOffset.UtcNow.AddHours(1.5),
            ProgramMark = 1.0
        };
        
        UserAnswers = new List<Domain.Models.UserAnswer>();
        foreach (var scheduleTest in ScheduleTests)
        {
            foreach (var userScheduleTest in scheduleTest.UserScheduleTests!)
            {
                foreach (var testQuestion in TestQuestions.Where(tq => tq.TestId == scheduleTest.TestId))
                {
                    var userAnswer = new Domain.Models.UserAnswer(scheduleTest.Id, testQuestion.QuestionId, userScheduleTest.UserId)
                    {
                        Body = $"User Answer Test User Answer - {Guid.NewGuid()}",
                        AnsweredAt = DateTimeOffset.UtcNow,
                        ProgramMark = 0.0
                    };
                    UserAnswers.Add(userAnswer);
                }
            }
        }
    }
    
    public UserAnswerServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public Services.UserAnswerService CreateUserAnswerService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);
        
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        var userAnswerRepository = CreateUserAnswerRepository(context);
        
        var addUserAnswersRequestValidator = new AddUserAnswersRequestValidator(userAnswerRepository, scheduleTestRepository, questionRepository);
        var addUserAnswersLogger = Mock.Of<ILogger<AddUserAnswersRequestHandler>>();
        // ToDo: mock with more realistic results
        var databaseService = new Mock<IScriptService>();
        for (var i = 0; i < 5; i++)
        {
            var expectedScript = new ScriptResult { Script = CorrectAnswerBodies[i] };
            var actualScript = new ScriptResult { Script = UserAnswerBodies[i] };
            var result = Result<IReadOnlyCollection<ScriptResult>>.Create();
            result.Complete(new[] { actualScript, expectedScript });
            var mockIndex = i;
            databaseService
                .Setup(ds => ds.ExecuteScriptsAsync(new List<string> { UserAnswerBodies[mockIndex], CorrectAnswerBodies[mockIndex] }, DatabaseIds[mockIndex], It.IsAny<CancellationToken>()))
                .ReturnsAsync(result);
        }

        var scriptFormattingService = new Mock<IScriptFormattingService>();
        scriptFormattingService
            .Setup(s => s.Clear(It.IsAny<string>()))
            .Returns<string>(str => str);
        var scriptResultsCompareService = new Mock<IScriptResultsCompareService>();
        scriptResultsCompareService
            .Setup(s => s.Compare(It.IsAny<ScriptResult>(), It.IsAny<ScriptResult>(), It.IsAny<double>()))
            .Returns(2.0);
        
        var addUserAnswersRequestHandler = new AddUserAnswersRequestHandler(
            userAnswerRepository,
            questionRepository,
            correctAnswerRepository,
            testQuestionRepository,
            databaseService.Object,
            scriptFormattingService.Object,
            scriptResultsCompareService.Object,
            addUserAnswersLogger,
            addUserAnswersRequestValidator,
            unitOfWork);
        
        var updateUserAnswerRequestValidator = new UpdateUserAnswerRequestValidator(testQuestionRepository, scheduleTestRepository, questionRepository);
        var updateUserAnswerLogger = Mock.Of<ILogger<UpdateUserAnswerRequestHandler>>();
        var updateUserAnswerRequestHandler = new UpdateUserAnswerRequestHandler(userAnswerRepository, updateUserAnswerLogger, updateUserAnswerRequestValidator, unitOfWork);
        
        var getUserAnswersLogger = Mock.Of<ILogger<GetByScheduleTestAndUserAnswersRequestHandler>>();
        var getUserAnswersRequestHandler = new GetByScheduleTestAndUserAnswersRequestHandler(userAnswerRepository, getUserAnswersLogger);
        
        var userAnswerServiceLogger = Mock.Of<ILogger<Services.UserAnswerService>>();
        var service = new Services.UserAnswerService(
            addUserAnswersRequestHandler,
            updateUserAnswerRequestHandler,
            getUserAnswersRequestHandler,
            userAnswerServiceLogger);

        return service;
    }

    public static async Task InitializeAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new UserAnswerServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var testRepository = CreateTestRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        var userAnswerRepository = CreateUserAnswerRepository(context);

        await topicRepository.AddAsync(Topic);
        foreach (var test in Tests.Concat(new[] { Test }))
            await testRepository.AddAsync(test);
        var allQuestions = Questions.Concat(AddQuestions).Concat(new[] { UpdateQuestion }).ToArray();
        foreach (var question in allQuestions)
            await questionRepository.AddAsync(question);
        foreach (var correctAnswer in allQuestions.Select(q => q.CorrectAnswer!))
            await correctAnswerRepository.AddAsync(correctAnswer);
        foreach (var testQuestion in TestQuestions.Concat(AddUpdateTestQuestions))
            await testQuestionRepository.AddAsync(testQuestion);
        foreach (var scheduleTest in ScheduleTests.Concat(new [] { ScheduleTest }))
            await scheduleTestRepository.AddAsync(scheduleTest);
        foreach (var userAnswer in UserAnswers.Concat(new [] { UpdateUserAnswer }))
            await userAnswerRepository.AddAsync(userAnswer);
        
        unitOfWork.SaveChanges();
    }
    
    public static async Task CleanUpAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new UserAnswerServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var testRepository = CreateTestRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        
        foreach (var scheduleTest in ScheduleTests.Concat(new [] { ScheduleTest }))
            await scheduleTestRepository.DeleteAsync(scheduleTest.Id);
        foreach (var test in Tests.Concat(new[] { Test }))
            await testRepository.DeleteAsync(test.Id);
        var allQuestions = Questions.Concat(AddQuestions).Concat(new[] { UpdateQuestion }).ToArray();
        foreach (var question in allQuestions)
            await questionRepository.DeleteAsync(question.Id);
        await topicRepository.DeleteAsync(Topic.Id);
        
        unitOfWork.SaveChanges();
    }
}