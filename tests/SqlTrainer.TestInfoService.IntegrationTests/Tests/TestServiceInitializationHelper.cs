﻿using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Configurations;
using SqlTrainer.TestInfoService.Application.Tests.Add;
using SqlTrainer.TestInfoService.Application.Tests.Delete;
using SqlTrainer.TestInfoService.Application.Tests.GetAll;
using SqlTrainer.TestInfoService.Application.Tests.GetById;
using SqlTrainer.TestInfoService.Application.Tests.Update;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

public sealed class TestServiceInitializationHelper : InitializationHelper
{
    public const string TopicIdString = "E4C52026-076D-415E-93A0-49E125D4D8B3";
    public const string QuestionIdString = "CF3FAFC4-4534-4CCB-BEF8-4B585DB65E42";

    public const string UpdateTestIdString = "2BFECCFF-B6C3-49FA-99A5-8C4D517E9EDB";
    public const string DeleteTestIdString = "330195B1-C794-47EF-8238-5DDF6802E5C4";
    public const string GetTestIdString = "66482A1A-772F-49B5-90C3-869726FC6FF3";

    private static readonly Guid TopicId = Guid.Parse(TopicIdString);
    private static readonly Guid QuestionId = Guid.Parse(QuestionIdString);
    
    private static readonly Guid UpdateTestId = Guid.Parse(UpdateTestIdString);
    private static readonly Guid DeleteTestId = Guid.Parse(DeleteTestIdString);
    private static readonly Guid GetTestId = Guid.Parse(GetTestIdString);

    private static Domain.Models.Topic Topic { get; }
    private static Domain.Models.Question Question { get; }
    private static Domain.Models.CorrectAnswer CorrectAnswer { get; }
    
    public static Domain.Models.Test AddTest { get; }
    public static Domain.Models.Test UpdateTest { get; }
    public static Domain.Models.Test DeleteTest { get; }
    public static Domain.Models.Test GetTest { get; }
    public static IReadOnlyCollection<Domain.Models.Test> Tests { get; }
    
    static TestServiceInitializationHelper()
    {
        Topic = new Domain.Models.Topic(TopicId) { Name = "Test Topic Name - " + Guid.NewGuid() };
        Question = new Domain.Models.Question(QuestionId)
        {
            Body = "Test Question Body - " + Guid.NewGuid(),
            TopicId = TopicId,
            Complexity = new Random().Next(1, 5),
            DatabaseId = Guid.NewGuid()
        };
        CorrectAnswer = new Domain.Models.CorrectAnswer
        {
            Body = "Test Correct Answer Body - " + Guid.NewGuid(),
            QuestionId = QuestionId
        };
        
        AddTest = CreateTestWithTestQuestion("Add");
        UpdateTest = CreateTestWithTestQuestion("Update", UpdateTestId);
        DeleteTest = CreateTestWithTestQuestion("Delete", DeleteTestId);
        GetTest = CreateTestWithTestQuestion("Get", GetTestId);
        Tests = Enumerable.Range(1, 500)
            .Select(i => CreateTestWithTestQuestion($"Test _test_ {i.ToString().PadLeft(3, '0')}"))
            .ToArray();
    }
    
    public TestServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public Services.TestService CreateTestService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);

        var testRepository = CreateTestRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        
        var addTestRequestValidator = new AddTestRequestValidator(questionRepository);
        var addTestLogger = Mock.Of<ILogger<AddTestRequestHandler>>();
        var addTestRequestHandler = new AddTestRequestHandler(testRepository, testQuestionRepository, addTestRequestValidator, unitOfWork, addTestLogger);
        
        var updateTestRequestValidator = new UpdateTestRequestValidator(testRepository, questionRepository);
        var updateTestLogger = Mock.Of<ILogger<UpdateTestRequestHandler>>();
        var updateTestRequestHandler = new UpdateTestRequestHandler(testRepository, testQuestionRepository, updateTestRequestValidator, unitOfWork, updateTestLogger);

        var deleteTestLogger = Mock.Of<ILogger<DeleteTestRequestHandler>>();
        var deleteTestRequestHandler = new DeleteTestRequestHandler(testRepository, unitOfWork, deleteTestLogger);

        var getByIdTestLogger = Mock.Of<ILogger<GetByIdTestRequestHandler>>();
        var getByIdTestRequestHandler = new GetByIdTestRequestHandler(testRepository, getByIdTestLogger);

        var getAllTestsLogger = Mock.Of<ILogger<GetAllTestsRequestHandler>>();
        var getAllTestsRequestHandler = new GetAllTestsRequestHandler(testRepository, getAllTestsLogger);

        var testServiceLogger = Mock.Of<ILogger<Services.TestService>>();
        var service = new Services.TestService(
            addTestRequestHandler,
            deleteTestRequestHandler,
            getByIdTestRequestHandler,
            getAllTestsRequestHandler,
            updateTestRequestHandler,
            testServiceLogger);

        return service;
    }
    
    public static async Task InitializeAsync(IDatabaseConfiguration configuration)
    {
        var helper = new TestServiceInitializationHelper(configuration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testRepository = CreateTestRepository(context);
        var testQuestionRepository = CreateTestQuestionRepository(context);
        
        await topicRepository.AddAsync(Topic);
        await questionRepository.AddAsync(Question);
        await correctAnswerRepository.AddAsync(CorrectAnswer);
        await testRepository.AddAsync(UpdateTest);
        await testRepository.AddAsync(DeleteTest);
        await testRepository.AddAsync(GetTest);
        foreach (var test in Tests)
            await testRepository.AddAsync(test);

        foreach (var testQuestion in Tests.Concat(new[] { UpdateTest, DeleteTest, GetTest }).SelectMany(t => t.TestQuestions!))
            await testQuestionRepository.AddAsync(testQuestion);
        
        unitOfWork.SaveChanges();
    }
    
    public static async Task CleanUpAsync(IDatabaseConfiguration configuration)
    {
        var helper = new TestServiceInitializationHelper(configuration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var testRepository = CreateTestRepository(context);
        
        await testRepository.DeleteAsync(UpdateTestId);
        await testRepository.DeleteAsync(DeleteTestId);
        await testRepository.DeleteAsync(GetTestId);
        foreach (var test in Tests)
            await testRepository.DeleteAsync(test.Id);
        
        await questionRepository.DeleteAsync(Question.Id);
        await topicRepository.DeleteAsync(Topic.Id);
        
        unitOfWork.SaveChanges();
    }

    private static Domain.Models.Test CreateTestWithTestQuestion(string text, Guid testId = default)
    {
        var test = new Domain.Models.Test(testId)
        {
            Name = text + " Test Name - " + Guid.NewGuid(),
            CreatedAt = DateTimeOffset.UtcNow
        };
        var random = new Random();
        var testQuestion = new Domain.Models.TestQuestion(test.Id, QuestionId)
        {
            MaxMark = random.Next(1, 5)
        };

        return test.WithTestQuestions(new [] { testQuestion });
    }
}