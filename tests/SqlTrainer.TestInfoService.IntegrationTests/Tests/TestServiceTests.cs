﻿using Moq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Tests;

[Collection(DatabaseDefinition.Name)]
public class TestServiceTests
{
    private readonly SqlTrainer.TestInfoService.Services.TestService sut;

    private TestServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new TestServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateTestService();
    }
    
    public class AddTests : TestServiceTests
    {
        private readonly Domain.Models.Test test;
        
        public AddTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            test = TestServiceInitializationHelper.AddTest;
        }
        
        [Theory]
        [InlineData(TestServiceInitializationHelper.QuestionIdString)]
        [InlineData("invalid")]
        public async Task Add_ReturnResponse(string questionId)
        {
            // Arrange
            var addTestRequest = new AddTestRequest
            {
                Name = test.Name,
                CreatedAt = Timestamp.FromDateTimeOffset(test.CreatedAt),
                Questions =
                {
                    new QuestionMaxMark
                    {
                        QuestionId = questionId,
                        MaxMark = 3.0
                    }
                }
            };
            
            // Act
            var response = await sut.Add(addTestRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(questionId, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class DeleteTests : TestServiceTests
    {
        public DeleteTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }
        
        [Theory]
        [InlineData(TestServiceInitializationHelper.DeleteTestIdString)]
        [InlineData("invalid")]
        public async Task Delete_ReturnResponse(string id)
        {
            // Arrange
            var deleteTestRequest = new DeleteTestRequest { Id = id };

            // Act
            var response = await sut.Delete(deleteTestRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class UpdateTests : TestServiceTests
    {
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }
        
        [Theory]
        [InlineData(TestServiceInitializationHelper.UpdateTestIdString, TestServiceInitializationHelper.QuestionIdString, 0)]
        [InlineData(TestServiceInitializationHelper.UpdateTestIdString, "invalid", 1)]
        [InlineData("invalid", TestServiceInitializationHelper.QuestionIdString, 1)]
        [InlineData("invalid", "invalid", 2)]
        public async Task Update_ReturnResponse(string testId, string questionId, int errorsCount)
        {
            // Arrange
            var updateTestRequest = new UpdateTestRequest
            {
                Id = testId,
                Name = $"Updated topic {testId}",
                Questions = { new QuestionMaxMark { QuestionId = questionId, MaxMark = 5.0 } }
            };

            // Act
            var response = await sut.Update(updateTestRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeEmpty();
                response.Errors.Count.Should().Be(errorsCount);
            }
        }
    }

    public class GetByIdTests : TestServiceTests
    {
        private readonly Domain.Models.Test test;
        
        public GetByIdTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            test = TestServiceInitializationHelper.GetTest;
        }
        
        [Theory]
        [InlineData(TestServiceInitializationHelper.GetTestIdString)]
        [InlineData("invalid")]
        public async Task GetById_ReturnTopicResponse(string id)
        {
            // Arrange
            var getByIdTopicRequest = new GetByIdTestRequest { Id = id };

            // Act
            var response = await sut.Get(getByIdTopicRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetTestResponse>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.Test.Should().NotBeNull();
                response.Test.Id.Should().Be(test.Id.ToString());
                response.Test.Name.Should().Be(test.Name);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeEmpty();
                response.Errors.Count.Should().Be(1);
                response.Test.Should().BeNull();
            }
        }
    }

    public class GetAllTests : TestServiceTests
    {
        public GetAllTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Fact]
        public async Task GetAll_Success_ReturnTopicsResponse()
        {
            // Arrange
            var getAllTestsRequest = new GetAllTestsRequest
            {
                SearchTerm = "_test_",
                OrderBy = "name",
                OrderByDirection = "desc",
                Page = 2,
                PageSize = 50
            };

            // Act
            var response = await sut.GetAll(getAllTestsRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetTestsResponse>();
            response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
            response.Errors.Should().BeEmpty();
            response.Tests.Should().NotBeNull();
            response.Tests.Count.Should().Be(50);
            var resultNames = response.Tests.Select(t => t.Name).ToArray();
            var expectedNames = TestServiceInitializationHelper.Tests.Where(t => t.Name.Contains("_test_"))
                .OrderByDescending(t => t.Name).Skip(50).Take(50).Select(t => t.Name).ToArray();
            resultNames.Should().BeEquivalentTo(expectedNames);
        }
    }
}