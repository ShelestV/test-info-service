﻿using Moq;
using Microsoft.Extensions.Logging;
using Persistence.Configurations;
using SqlTrainer.TestInfoService.Application.Topics.Add;
using SqlTrainer.TestInfoService.Application.Topics.Delete;
using SqlTrainer.TestInfoService.Application.Topics.GetAll;
using SqlTrainer.TestInfoService.Application.Topics.GetById;
using SqlTrainer.TestInfoService.Application.Topics.Update;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

public sealed class TopicServiceInitializationHelper : InitializationHelper
{
    public const string DeleteTopicIdString = "E24EE3FE-7660-4E52-8392-D87E9332893F";
    public const string UpdateTopicIdString = "D1C775E7-6EB9-4C48-9B86-17D64A7FFAA9";
    public const string GetTopicIdString = "923C75D3-3296-4671-84D0-A78F3E5AE012";
    
    private static readonly Guid DeleteTopicId = Guid.Parse(DeleteTopicIdString);
    private static readonly Guid UpdateTopicId = Guid.Parse(UpdateTopicIdString);
    private static readonly Guid GetTopicId = Guid.Parse(GetTopicIdString);
    
    public static Domain.Models.Topic AddTopic { get; }
    public static Domain.Models.Topic UpdateTopic { get; }
    public static Domain.Models.Topic DeleteTopic { get; }
    public static Domain.Models.Topic GetTopic { get; }
    public static IReadOnlyCollection<Domain.Models.Topic> Topics { get; }

    static TopicServiceInitializationHelper()
    {
        AddTopic = CreateTopic("Add Topic");
        UpdateTopic = CreateTopic("Update Topic", UpdateTopicId);
        DeleteTopic = CreateTopic("Delete Topic", DeleteTopicId);
        GetTopic = CreateTopic("Get Topic", GetTopicId);
        Topics = Enumerable.Range(1, 500)
            .Select(i => CreateTopic($"Topic _topic_ Get All {i.ToString().PadLeft(3, '0')}"))
            .ToArray();
    }
    
    public TopicServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public Services.TopicService CreateTopicService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);

        var topicRepository = CreateTopicRepository(context);
        
        var addTopicRequestValidator = new AddTopicRequestValidator();
        var addTopicLogger = Mock.Of<ILogger<AddTopicRequestHandler>>();
        var addTopicRequestHandler = new AddTopicRequestHandler(addTopicRequestValidator, topicRepository, unitOfWork, addTopicLogger);
        
        var updateTopicRequestValidator = new UpdateTopicRequestValidator();
        var updateTopicLogger = Mock.Of<ILogger<UpdateTopicRequestHandler>>();
        var updateTopicRequestHandler = new UpdateTopicRequestHandler(updateTopicRequestValidator, topicRepository, unitOfWork, updateTopicLogger);

        var deleteTopicLogger = Mock.Of<ILogger<DeleteTopicRequestHandler>>();
        var deleteTopicRequestHandler = new DeleteTopicRequestHandler(topicRepository, unitOfWork, deleteTopicLogger);

        var getByIdTopicLogger = Mock.Of<ILogger<GetByIdTopicRequestHandler>>();
        var getByIdTopicRequestHandler = new GetByIdTopicRequestHandler(topicRepository, getByIdTopicLogger);

        var getAllTopicsLogger = Mock.Of<ILogger<GetAllTopicsRequestHandler>>();
        var getAllTopicsRequestHandler = new GetAllTopicsRequestHandler(topicRepository, getAllTopicsLogger);

        var topicServiceLogger = Mock.Of<ILogger<Services.TopicService>>();
        var service = new Services.TopicService(
            addTopicRequestHandler,
            deleteTopicRequestHandler,
            getByIdTopicRequestHandler,
            getAllTopicsRequestHandler,
            updateTopicRequestHandler,
            topicServiceLogger);

        return service;
    }

    public static async Task InitializeAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new TopicServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);

        await topicRepository.AddAsync(UpdateTopic);
        await topicRepository.AddAsync(DeleteTopic);
        await topicRepository.AddAsync(GetTopic);
        foreach (var topic in Topics)
            await topicRepository.AddAsync(topic);
        
        unitOfWork.SaveChanges();
    }

    public static async Task CleanUpAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new TopicServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);

        await topicRepository.DeleteAsync(AddTopic.Id);
        await topicRepository.DeleteAsync(UpdateTopicId);
        await topicRepository.DeleteAsync(DeleteTopicId);
        await topicRepository.DeleteAsync(GetTopicId);
        foreach (var topic in Topics)
            await topicRepository.DeleteAsync(topic.Id);
        
        unitOfWork.SaveChanges();
    }
    
    private static Domain.Models.Topic CreateTopic(string text, Guid topicId = default)
    {
        return new(topicId)
        {
            Name = text + " Name - " + Guid.NewGuid(),
        };
    }
}