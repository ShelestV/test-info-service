﻿using Moq;
using Grpc.Core;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.Topics;

[Collection(DatabaseDefinition.Name)]
public class TopicServiceTests
{
    private readonly SqlTrainer.TestInfoService.Services.TopicService sut;

    private TopicServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new TopicServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateTopicService();
    }
    
    public class AddTests : TopicServiceTests
    {
        private readonly Domain.Models.Topic topic;
        
        public AddTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            topic = TopicServiceInitializationHelper.AddTopic;
        }
        
        [Fact]
        public async Task Add_ReturnResponse()
        {
            // Arrange
            var addTopicRequest = new AddTopicRequest { Name = topic.Name };
            
            // Act
            var response = await sut.Add(addTopicRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
            response.Errors.Should().BeEmpty();
        }
    }

    public class DeleteTests : TopicServiceTests
    {
        public DeleteTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Theory]
        [InlineData(TopicServiceInitializationHelper.DeleteTopicIdString)]
        [InlineData("invalid")]
        public async Task Delete_ReturnResponse(string id)
        {
            // Arrange
            var deleteTopicRequest = new DeleteTopicRequest { Id = id };

            // Act
            var response = await sut.Delete(deleteTopicRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(1);
            }
        }
    }

    public class UpdateTests : TopicServiceTests
    {
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }
        
        [Theory]
        [InlineData(TopicServiceInitializationHelper.UpdateTopicIdString)]
        [InlineData("invalid")]
        public async Task Update_ReturnResponse(string id)
        {
            // Arrange
            var updateTopicRequest = new UpdateTopicRequest
            {
                Id = id,
                Name = $"Updated topic {id}"
            };

            // Act
            var response = await sut.Update(updateTopicRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<Response>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeEmpty();
                response.Errors.Count.Should().Be(1);
            }
        }
    }

    public class GetByIdTests : TopicServiceTests
    {
        private readonly Domain.Models.Topic topic;
        public GetByIdTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            topic = TopicServiceInitializationHelper.GetTopic;
        }
        
        [Theory]
        [InlineData(TopicServiceInitializationHelper.GetTopicIdString)]
        [InlineData("invalid")]
        public async Task GetById_ReturnTopicResponse(string id)
        {
            // Arrange
            var getByIdTopicRequest = new GetByIdTopicRequest { Id = id };

            // Act
            var response = await sut.Get(getByIdTopicRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetTopicResponse>();
            if (Guid.TryParse(id, out _))
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                response.Topic.Should().NotBeNull();
                response.Topic.Id.ToUpper().Should().Be(id.ToUpper());
                response.Topic.Name.Should().Be(topic.Name);
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().NotBeEmpty();
                response.Errors.Count.Should().Be(1);
                response.Topic.Should().BeNull();
            }
        }
    }

    public class GetAllTests : TopicServiceTests
    {
        public GetAllTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
        }

        [Fact]
        public async Task GetAll_Success_ReturnTopicsResponse()
        {
            // Arrange
            var getAllTopicsRequest = new GetAllTopicsRequest
            {
                SearchTerm = "Topic _topic_ Get All",
                OrderBy = "name",
                OrderByDirection = "desc",
                Page = 2,
                PageSize = 50
            };

            // Act
            var response = await sut.GetAll(getAllTopicsRequest, Mock.Of<ServerCallContext>());

            // Assert
            response.Should().BeOfType<GetTopicsResponse>();
            response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
            response.Errors.Should().BeEmpty();
            response.Topics.Should().NotBeNull();
            response.Topics.Count.Should().Be(50);
            var resultNames = response.Topics.Select(t => t.Name).ToArray();
            var expectedNames = TopicServiceInitializationHelper.Topics.Where(t => t.Name.Contains("Topic _topic_ Get All"))
                .OrderByDescending(t => t.Name).ThenByDescending(t => t.Id).Skip(50).Take(50).Select(t => t.Name).ToArray();
            resultNames.Should().BeInDescendingOrder();
            foreach (var name in expectedNames)
                resultNames.Should().Contain(name);
        }
    }
}