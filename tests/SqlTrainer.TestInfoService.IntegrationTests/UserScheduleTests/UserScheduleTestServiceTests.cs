﻿using Moq;
using Grpc.Core;
using Google.Protobuf.WellKnownTypes;
using SqlTrainer.Responses;
using SqlTrainer.TestInfoService.Application.Repositories;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserScheduleTests;

[Collection(DatabaseDefinition.Name)]
public class UserScheduleTestServiceTests
{
    private readonly Services.UserScheduleTestService sut;
    private readonly IScheduleTestRepository scheduleTestRepository;

    private UserScheduleTestServiceTests(Helpers.DatabaseHelper databaseHelper)
    {
        var initializer = new UserScheduleTestServiceInitializationHelper(databaseHelper.DatabaseConfiguration);
        sut = initializer.CreateUserScheduleTestService();
        var context = initializer.CreateContext();
        scheduleTestRepository = InitializationHelper.CreateScheduleTestRepository(context);
    }

    public class UpdateTests : UserScheduleTestServiceTests
    {
        private const string ScheduleTestIdString = UserScheduleTestServiceInitializationHelper.ScheduleTestIdString;
        private const string UserUpdateFinishedAtIdString = UserScheduleTestServiceInitializationHelper.UserUpdateFinishedAtIdString;
        private const string UserUpdateCheckedByTeacherIdString = UserScheduleTestServiceInitializationHelper.UserUpdateCheckedByTeacherIdString;
        
        private readonly Domain.Models.ScheduleTest scheduleTest;
        
        public UpdateTests(Helpers.DatabaseHelper databaseHelper) : base(databaseHelper)
        {
            scheduleTest = UserScheduleTestServiceInitializationHelper.ScheduleTest;
        }

        [Theory]
        [InlineData(ScheduleTestIdString, UserUpdateFinishedAtIdString, false, 0)]
        [InlineData(ScheduleTestIdString, UserUpdateCheckedByTeacherIdString, true, 0)]
        [InlineData(ScheduleTestIdString, "invalid", false, 1)]
        [InlineData("invalid", UserUpdateFinishedAtIdString, false, 1)]
        [InlineData("invalid", "invalid", false, 2)]
        public async Task Update_ReturnResponse(string scheduleTestId, string userId, bool checkedByTeacher, int errorsCount)
        {
            // Arrange
            var userScheduleTest = Guid.TryParse(userId, out var userIdGuid)
                ? scheduleTest.UserScheduleTests!.FirstOrDefault(ust => ust.UserId == userIdGuid)
                : null;
            var finishedAt = userScheduleTest is null ? DateTimeOffset.UtcNow 
                : userScheduleTest.FinishedAt ?? DateTimeOffset.UtcNow.AddSeconds(-30);
            var request = new UpdateUserScheduleTestRequest
            {
                ScheduleTestId = scheduleTestId,
                UserId = userId,
                FinishedAt = Timestamp.FromDateTimeOffset(finishedAt),
                CheckedByTeacher = checkedByTeacher ? checkedByTeacher : null
            };
            
            // Act
            var response = await sut.Update(request, Mock.Of<ServerCallContext>());
            
            // Assert
            response.Should().BeOfType<Response>();
            if (errorsCount == 0)
            {
                response.IsSuccess.Should().BeTrue(string.Join(", ", response.Errors));
                response.Errors.Should().BeEmpty();
                var assertScheduleTest = await scheduleTestRepository.GetByIdAsync(scheduleTest.Id);
                assertScheduleTest.Should().NotBeNull();
                assertScheduleTest!.UserScheduleTests.Should().NotBeEmpty();
                var assertUserScheduleTest = assertScheduleTest.UserScheduleTests!.First(ust => ust.UserId == userIdGuid);
                assertUserScheduleTest.Should().NotBeNull();
                assertUserScheduleTest.FinishedAt!.Value.ToString("MM/dd/yyyy hh:mm").Should().Be(finishedAt.ToString("MM/dd/yyyy hh:mm"));
                if (checkedByTeacher)
                    assertUserScheduleTest.CheckedByTeacher.Should().BeTrue();
                else
                    assertUserScheduleTest.CheckedByTeacher.Should().BeNull();
            }
            else
            {
                response.IsSuccess.Should().BeFalse();
                response.Errors.Should().HaveCount(errorsCount);
            }
        }
    }
}