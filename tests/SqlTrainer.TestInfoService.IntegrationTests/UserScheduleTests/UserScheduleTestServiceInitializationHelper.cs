﻿using Microsoft.Extensions.Logging;
using Moq;
using Persistence.Configurations;
using SqlTrainer.TestInfoService.Application.UserScheduleTests.Update;
using SqlTrainer.TestInfoService.IntegrationTests.Helpers;

namespace SqlTrainer.TestInfoService.IntegrationTests.UserScheduleTests;

public sealed class UserScheduleTestServiceInitializationHelper : InitializationHelper
{
    private const string TopicIdString = "CAA8379E-604E-4D0B-951B-3D0763CD3E6D";
    private const string QuestionIdString = "AA74D6C0-EA0F-498A-B640-AD5B2EEBAA13";
    private const string CorrectAnswerIdString = "6CD48385-BA5A-4D56-A995-CAD2104E26AD";
    private const string TestIdString = "75E16060-D647-464E-B193-D08BF34F1A07";
    public const string ScheduleTestIdString = "6DD0EE45-5667-4685-97C6-E85E4921FDF1";
    public const string UserUpdateFinishedAtIdString = "2EDB216B-3434-4DA1-B61F-98E87DF7A851";
    public const string UserUpdateCheckedByTeacherIdString = "A1C95D45-B3AE-4F1C-A589-40116F14A831";
    
    private static readonly Guid TopicId = Guid.Parse(TopicIdString);
    private static readonly Guid QuestionId = Guid.Parse(QuestionIdString);
    private static readonly Guid CorrectAnswerId = Guid.Parse(CorrectAnswerIdString);
    private static readonly Guid TestId = Guid.Parse(TestIdString);
    private static readonly Guid UserUpdateFinishedAtId = Guid.Parse(UserUpdateFinishedAtIdString);
    private static readonly Guid UserUpdateCheckedByTeacherId = Guid.Parse(UserUpdateCheckedByTeacherIdString);
    private static readonly Guid ScheduleTestId = Guid.Parse(ScheduleTestIdString);
    
    private static Domain.Models.Topic Topic { get; }
    private static Domain.Models.Question Question { get; }
    private static Domain.Models.CorrectAnswer CorrectAnswer { get; }
    private static Domain.Models.Test Test { get; }
    public static Domain.Models.ScheduleTest ScheduleTest { get; }
    private static Domain.Models.UserScheduleTest UpdateFinishedAtUserScheduleTest { get; }
    private static Domain.Models.UserScheduleTest UpdateCheckedByTeacherUserScheduleTest { get; }
    
    static UserScheduleTestServiceInitializationHelper()
    {
        var random = new Random();
        Topic = new Domain.Models.Topic(TopicId) { Name = "User Schedule Test Test Topic Name - " + Guid.NewGuid() };
        Question = new Domain.Models.Question(QuestionId)
        {
            Body = "User Schedule Test Test Question Body - " + Guid.NewGuid(),
            TopicId = TopicId,
            Complexity = random.Next(1, 5),
            DatabaseId = Guid.NewGuid()
        };
        CorrectAnswer = new Domain.Models.CorrectAnswer(CorrectAnswerId)
        {
            Body = "User Schedule Test Test Correct Answer Body - " + Guid.NewGuid(),
            QuestionId = QuestionId
        };
        Test = new Domain.Models.Test(TestId)
        {
            Name = "User Schedule Test Test Name - " + Guid.NewGuid(),
            CreatedAt = DateTimeOffset.UtcNow
        };
        ScheduleTest = new Domain.Models.ScheduleTest(ScheduleTestId)
        {
            TestId = TestId,
            StartAt = DateTimeOffset.UtcNow.AddDays(-1),
            FinishAt = DateTimeOffset.UtcNow.AddDays(1),
        };
        UpdateFinishedAtUserScheduleTest = new Domain.Models.UserScheduleTest(ScheduleTestId, UserUpdateFinishedAtId);
        UpdateCheckedByTeacherUserScheduleTest = new Domain.Models.UserScheduleTest(ScheduleTestId, UserUpdateCheckedByTeacherId)
        {
            FinishedAt = DateTimeOffset.UtcNow.AddHours(-1)
        };
        ScheduleTest.WithUserScheduleTests(new[] { UpdateFinishedAtUserScheduleTest, UpdateCheckedByTeacherUserScheduleTest });
    }
    
    public UserScheduleTestServiceInitializationHelper(IDatabaseConfiguration configuration) : base(configuration)
    {
    }
    
    public Services.UserScheduleTestService CreateUserScheduleTestService()
    {
        var context = CreateContext();
        var unitOfWork = CreateUnitOfWork(context);
        
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        var userScheduleTestRepository = CreateUserScheduleTestRepository(context);
        
        var updateUserScheduleTestRequestValidator = new UpdateUserScheduleTestRequestValidator(scheduleTestRepository);
        var updateUserScheduleTestLogger = Mock.Of<ILogger<UpdateUserScheduleTestRequestHandler>>();
        var updateUserScheduleTestRequestHandler = new UpdateUserScheduleTestRequestHandler(userScheduleTestRepository, updateUserScheduleTestLogger, updateUserScheduleTestRequestValidator, unitOfWork);

        var userScheduleTestServiceLogger = Mock.Of<ILogger<Services.UserScheduleTestService>>();
        var service = new Services.UserScheduleTestService(
            updateUserScheduleTestRequestHandler, 
            userScheduleTestServiceLogger);

        return service;
    }

    public static async Task InitializeAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new UserScheduleTestServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var correctAnswerRepository = CreateCorrectAnswerRepository(context);
        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        var userScheduleTestRepository = CreateUserScheduleTestRepository(context);

        await topicRepository.AddAsync(Topic);
        await questionRepository.AddAsync(Question);
        await correctAnswerRepository.AddAsync(CorrectAnswer);
        await testRepository.AddAsync(Test);
        await scheduleTestRepository.AddAsync(ScheduleTest);
        await userScheduleTestRepository.UpdateAsync(UpdateCheckedByTeacherUserScheduleTest);
        
        unitOfWork.SaveChanges();
    }

    public static async Task CleanUpAsync(IDatabaseConfiguration databaseConfiguration)
    {
        var helper = new UserScheduleTestServiceInitializationHelper(databaseConfiguration);
        var context = helper.CreateContext();
        var unitOfWork = helper.CreateUnitOfWork(context);
        var topicRepository = CreateTopicRepository(context);
        var questionRepository = CreateQuestionRepository(context);
        var testRepository = CreateTestRepository(context);
        var scheduleTestRepository = CreateScheduleTestRepository(context);
        
        await scheduleTestRepository.DeleteAsync(ScheduleTestId);
        await testRepository.DeleteAsync(TestId);
        await questionRepository.DeleteAsync(QuestionId);
        await topicRepository.DeleteAsync(TopicId);
        
        unitOfWork.SaveChanges();
    }
}